<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'VioletGene',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Violet</b>Gene',

    'logo_mini' => '<b>V</b>Ge',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'purple-light',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'user/home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    'landing_page' => '',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MAIN NAVIGATION',
        // [
        //     'text' => 'Blog',
        //     'url'  => 'admin/blog',
        //     'can'  => 'manage-blog',
        // ],
        [
            'text'        => 'Dashboard',
            'url'         => '/user/home',
            'icon'        => 'home',
            // 'label'       => 4,
            // 'label_color' => 'success',
        ],
        [
            'text'        => 'Explore genetic data',
            'url'         => '/user/explorer',
            'icon'        => 'search',
        ],
        [
            'text'        => 'Genetics education',
            'url'         => 'user/education/0',
            'icon'        => 'graduation-cap',
            // 'icon_color' => 'green',
        ],
        'ADMIN',
        [
          'text' => 'Start testing',
          'icon' => 'flask',
          // 'url'  => '/test/start',
          'submenu' => [
              [
                  'text' => 'My tests',
                  'icon' => 'play',
                  'url'  => '/test/my_tests/0',
              ],
              [
                  'text' => 'Create a new test',
                  'icon' => 'file',
                  'url'  => '/test/new/standard',
              ],
              [
                  'text' => 'Order test',
                  'icon' => 'credit-card',
                  'url'  => '/order/start',
              ],
              [
                  'text' => 'My orders',
                  'icon' => 'folder-open',
                  'url'  => '/order/view',
              ],
          ],
        ],
        [
            'text' => 'Design experiments',
            'icon' => 'wrench',
            'submenu' => [
              [
                  'text' => 'Gene targets',
                  'icon' => 'wrench',
                  'url'  => '/test/design_target/0',
              ],
              [
                  'text' => 'Gene Panels',
                  'icon' => 'wrench',
                  'url'  => '/test/design_panel/0',
              ],
              [
                  'text' => 'DNA extractions',
                  'icon' => 'wrench',
                  'url'  => '/test/design_extraction/0',
              ],
            ],
        ],
        [
            'text' => 'Edit educational material',
            'icon' => 'file',
            // 'url'  => '/test/design',
            'submenu' => [
                [
                    'text' => 'Flash cards',
                    'icon' => 'file',
                    'url'  => '/topic/EditFlashWindow',
                ],
                [
                    'text' => 'Exercises',
                    'icon' => 'file',
                    'url'  => '/topic/NewExamWindow',
                ],
                [
                    'text' => 'Genetics Topics',
                    'icon' => 'file',
                    'url'  => '/topic/NewTopicWindow',
                ],
            ],
        ],
        'PROFILE',
        [
          'text' => 'Profile settings',
          'icon' => 'user',
          'submenu' => [
              [
                  'text' => 'Profile settings',
                  'url'  => '/user/settings/basic',
                  'icon' => 'cog',
              ],
              [
                  'text' => 'Student settings',
                  'url'  => '/user/settings/student',
                  'icon' => 'user',
              ],
              [
                  'text' => 'Admin settings',
                  'url'  => '/user/settings/admin',
                  'icon' => 'users',
              ],
              [
                  'text' => 'Change Password',
                  'url'  => '/user/password',
                  'icon' => 'lock',
              ],
          ],
        ],
        'INFORMATION',
        [
            'text' => 'About the platform',
            'url'  => '/about',
            'icon' => 'info-circle',
        ],
        [
            'text' => 'Donate for education',
            'icon' => 'money',
            'url'  => 'https://www.justgiving.com',
        ],
        [
            'text' => 'Terms and Conditions',
            'url'  => '/terms',
            'icon' => 'legal',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
    ],
];
