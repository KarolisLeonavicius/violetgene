<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Get in touch</h4>
    </div>
    <div class="modal-body">
      {!! Form::open(['url' => url('send_mail'), 'class' => 'form-signin'] ) !!}
      @include('includes.errors')
      <h3>Details</h3>
      <label>Name</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <input name="name" type="text" class="form-control" placeholder="" required="">
      </div>

      <label>Email address</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
        <input name="email" type="email" class="form-control" placeholder="">
      </div>

      <label>Phone number</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
        <input name="phone" type="phone" class="form-control" placeholder="Format: +44 123 1234567">
      </div>

      <div class="form-group">
        <label>Your role</label>
        <select name="school_role" class="form-control">
          <option>Biology teacher</option>
          <option>Head teacher</option>
          <option>Laboratory assistant</option>
          <option>Student</option>
          <option>Other</option>
        </select>
      </div>

      <div class="form-group">
        <label>Message</label>
        <textarea name="message" class="form-control" rows="4"></textarea>
      </div>
      <button id="submit_button" class="btn btn-lg btn-primary btn-block register-btn" type="submit">Send</button>
      {!! Form::close() !!}
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
