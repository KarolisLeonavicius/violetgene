@extends('layouts.master')

@section('title', 'Test')

@section('navigation')
  @include('layouts.navbar_mini')
@endsection

@section('head')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
@stop

@section('content')
  <div class="content-section-a">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <h2 class="section-heading">Output</h2>
                  <div id="power">Test</div>
              </div>
              <button class="btn-lg btn btn-success" onclick="callAjax()">Start</button>
              <div class="col-lg-12 col-sm-6">
                  <h2 class="section-heading">Input</h2>
                  <div id="hidden">Test</div>
              </div>
          </div>
      </div>
      <!-- /.container -->
  </div>
@endsection


@section('footer')
  @include('layouts.footer')
@stop

@section('js')

    <script>
        //var socket = io('http://violetgene.org:3000');
        var socket = io('http://website.dev:3000');
        var current_value = 0;
        socket.on("test-channel:App\\Events\\DataEvent", function(message){
            // increase the power everytime we load test route
            // console.log(message);
            $('#power').text(current_value + parseInt(message.data.power));
            current_value = parseInt($('#power').text());
        });

        var interval;
        function callAjax() {
          $.ajax({
                  type: 'GET',
                  url: 'http://website.dev/fire',
                  //url: 'http://violetgene.org/fire',
                  data: $(this).serialize(),
                  dataType: 'jsonp',
                  success: function (data) {
                          $('#hidden').text(data.Success);// first set the value
                          // console.log(data.Success);
                          interval = setTimeout(callAjax, 1000);
                  },
                  error:function(){
                      // console.log("Error");
                  }
                });
        }
    </script>
@stop
