
@extends('layouts.master')

@section('title', 'VioletGene')

@section('content')
  <div class="content-section-a">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <a href="/">Homepage</a>
            <h1>Terms and conditions</h1>
            <div class="row">
                <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
                  <h3 class="section-heading">TERMS OF SERVICE</h3>
                  <p class="lead">
                  Last Updated: 2017/06/20<br/><br/>

                  These terms of service ("Terms") apply to your access and use of violetgene.org (the "Service"). Please read them carefully.
                  </p>
                  <h3 class="section-heading">Accepting these Terms</h3>
                  <p class="lead">
                  If you access or use the Service, it means you agree to be bound by all of the terms below. So, before you use the Service, please read all of the terms. If you don't agree to all of the terms below, please do not use the Service. Also, if a term does not make sense to you, please let us know by e-mailing info@violetgene.org.<br/><br/>
                  </p>
                  <h3 class="section-heading">Changes to these Terms</h3>
                  <p class="lead">
                  We reserve the right to modify these Terms at any time. For instance, we may need to change these Terms if we come out with a new feature or for some other reason.<br/><br/>

                  Whenever we make changes to these Terms, the changes are effective 7 days after we post such revised Terms (indicated by revising the date at the top of these Terms) or upon your acceptance if we provide a mechanism for your immediate acceptance of the revised Terms (such as a click-through confirmation or acceptance button). It is your responsibility to check VioletGene for changes to these Terms.<br/><br/>

                  If you continue to use the Service after the revised Terms go into effect, then you have accepted the changes to these Terms.<br/><br/>
                  </p>
                  <h3 class="section-heading">Creating Accounts</h3>
                  <p class="lead">
                  When you create an account or use another service to log in to the Service, you agree to maintain the security of your password and accept all risks of unauthorized access to any data or other information you provide to the Service.<br/><br/>

                  If you discover or suspect any Service security breaches, please let us know as soon as possible.<br/><br/>
                  </p>
                  <h3 class="section-heading">Your Content & Conduct</h3>
                  <p class="lead">

                  Our Service allows you and other users to post, link and otherwise make available content. You are responsible for the content that you make available to the Service, including its legality, reliability, and appropriateness.<br/><br/>

                  When you post, link or otherwise make available content to the Service, you grant us the right and license to use, reproduce, modify, publicly perform, publicly display and distribute your content on or through the Service. We may format your content for display throughout the Service, but we will not edit or revise the substance of your content itself.<br/><br/>

                  Aside from our limited right to your content, you retain all of your rights to the content you post, link and otherwise make available on or through the Service.<br/><br/>

                  You can remove the content that you posted by deleting it. Once you delete your content, it will not appear on the Service, but copies of your deleted content may remain in our system or backups for some period of time. We will retain web server access logs for a maximum of 6 months and then delete them.<br/><br/>

                  You may not post, link and otherwise make available on or through the Service any of the following:<br/>
                  Content that is libelous, defamatory, bigoted, fraudulent or deceptive;<br/>
                  Content that is illegal or unlawful, that would otherwise create liability;<br/>
                  Content that may infringe or violate any patent, trademark, trade secret, copyright, right of privacy, right of publicity or other intellectual or other right of any party;<br/>
                  Mass or repeated promotions, political campaigning or commercial messages directed at users who do not follow you (SPAM);<br/>
                  Private information of any third party (e.g., addresses, phone numbers, email addresses, Social Security numbers and credit card numbers); and<br/>
                  Viruses, corrupted data or other harmful, disruptive or destructive files or code.<br/><br/>

                  Also, you agree that you will not do any of the following in connection with the Service or other users:<br/>
                  Use the Service in any manner that could interfere with, disrupt, negatively affect or inhibit other users from fully enjoying the Service or that could damage, disable, overburden or impair the functioning of the Service;<br/>
                  Impersonate or post on behalf of any person or entity or otherwise misrepresent your affiliation with a person or entity;<br/>
                  Collect any personal information about other users, or intimidate, threaten, stalk or otherwise harass other users of the Service;<br/>
                  Create an account or post any content if you are not over 13 years of age years of age; and<br/>
                  Circumvent or attempt to circumvent any filtering, security measures, rate limits or other features designed to protect the Service, users of the Service, or third parties.<br/>

                  We put a lot of effort into creating the Service including, the logo and all designs, text, graphics, pictures, information and other content (excluding your content). This property is owned by us or our licensors and it is protected by UK and international copyright laws. We grant you the right to use it.<br/><br/>

                  However, unless we expressly state otherwise, your rights do not include: (i) publicly performing or publicly displaying the Service; (ii) modifying or otherwise making any derivative uses of the Service or any portion thereof; (iii) using any data mining, robots or similar data gathering or extraction methods; (iv) downloading (other than page caching) of any portion of the Service or any information contained therein; (v) reverse engineering or accessing the Service in order to build a competitive product or service; or (vi) using the Service other than for its intended purposes. If you do any of this stuff, we may terminate your use of the Service.<br/><br/>
                  </p>

                  <h3 class="section-heading">Hyperlinks and Third Party Content</h3>
                  <p class="lead">
                  You may create a hyperlink to the Service. But, you may not use, frame or utilize framing techniques to enclose any of our trademarks, logos or other proprietary information without our express written consent.<br/><br/>

                  Karolis Leonavicius makes no claim or representation regarding, and accepts no responsibility for third party websites accessible by hyperlink from the Service or websites linking to the Service. When you leave the Service, you should be aware that these Terms and our policies no longer govern.<br/><br/>

                  If there is any content on the Service from you and others, we don't review, verify or authenticate it, and it may include inaccuracies or false information. We make no representations, warranties, or guarantees relating to the quality, suitability, truth, accuracy or completeness of any content contained in the Service. You acknowledge sole responsibility for and assume all risk arising from your use of or reliance on any content.<br/><br/>
                  </p>

                  <h3 class="section-heading">Unavoidable Legal Stuff</h3>
                  <p class="lead">
                  THE SERVICE AND ANY OTHER SERVICE AND CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE ARE PROVIDED TO YOU ON AN AS IS OR AS AVAILABLE BASIS WITHOUT ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND. WE DISCLAIM ANY AND ALL WARRANTIES AND REPRESENTATIONS (EXPRESS OR IMPLIED, ORAL OR WRITTEN) WITH RESPECT TO THE SERVICE AND CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE WHETHER ALLEGED TO ARISE BY OPERATION OF LAW, BY REASON OF CUSTOM OR USAGE IN THE TRADE, BY COURSE OF DEALING OR OTHERWISE.<br/><br/>

                  IN NO EVENT WILL Karolis Leonavicius BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY SPECIAL, INDIRECT, INCIDENTAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES OF ANY KIND ARISING OUT OF OR IN CONNECTION WITH THE SERVICE OR ANY OTHER SERVICE AND/OR CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE, REGARDLESS OF THE FORM OF ACTION, WHETHER IN CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR ARE AWARE OF THE POSSIBILITY OF SUCH DAMAGES. OUR TOTAL LIABILITY FOR ALL CAUSES OF ACTION AND UNDER ALL THEORIES OF LIABILITY WILL BE LIMITED TO THE AMOUNT YOU PAID TO Karolis Leonavicius. THIS SECTION WILL BE GIVEN FULL EFFECT EVEN IF ANY REMEDY SPECIFIED IN THIS AGREEMENT IS DEEMED TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.<br/><br/>

                  You agree to defend, indemnify and hold us harmless from and against any and all costs, damages, liabilities, and expenses (including attorneys' fees, costs, penalties, interest and disbursements) we incur in relation to, arising from, or for the purpose of avoiding, any claim or demand from a third party relating to your use of the Service or the use of the Service by any person using your account, including any claim that your use of the Service violates any applicable law or regulation, or the rights of any third party, and/or your violation of these Terms.<br/><br/>
                  </p>

                  <h3 class="section-heading">Copyright Complaints</h3>
                  <p class="lead">
                  We take intellectual property rights seriously. In accordance with the Digital Millennium Copyright Act ("DMCA") and other applicable law, we have adopted a policy of terminating, in appropriate circumstances and, at our sole discretion, access to the service for users who are deemed to be repeat infringers.<br/><br/>
                  </p>

                  <h3 class="section-heading">Governing Law</h3>
                  <p class="lead">
                  The validity of these Terms and the rights, obligations, and relations of the parties under these Terms will be construed and determined under and in accordance with the laws of the United Kingdom, without regard to conflicts of law principles.<br/><br/>
                  </p>

                  <h3 class="section-heading">Jurisdiction</h3>
                  <p class="lead">
                  You expressly agree that exclusive jurisdiction for any dispute with the Service or relating to your use of it, resides in the courts of the United Kingdom and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of the United Kingdom located in connection with any such dispute including any claim involving Service. You further agree that you and Service will not commence against the other a class action, class arbitration or other representative action or proceeding.<br/><br/>
                  </p>

                  <h3 class="section-heading">Termination</h3>
                  <p class="lead">
                  If you breach any of these Terms, we have the right to suspend or disable your access to or use of the Service.<br/><br/>
                  </p>

                  <h3 class="section-heading">Entire Agreement</h3>
                  <p class="lead">
                  These Terms constitute the entire agreement between you and Karolis Leonavicius regarding the use of the Service, superseding any prior agreements between you and Karolis Leonavicius relating to your use of the Service.<br/><br/>
                  </p>

                  <h3 class="section-heading">Feedback</h3>
                  <p class="lead">
                  Please let us know what you think of the Service, these Terms and, in general, VioletGene. When you provide us with any feedback, comments or suggestions about the Service, these Terms and, in general, VioletGene, you irrevocably assign to us all of your right, title and interest in and to your feedback, comments and suggestions.<br/><br/>
                  </p>
          </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <h1> Privacy policy</h1>

              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                  <img class="img-responsive" src="img/Edu.png" alt="">
              </div> -->
              <p class="lead">
                This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>

              <h3 class="section-heading">What personal information do we collect from the people that visit our blog, website or app?</h3>

              <p class="lead">When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, credit card information or other details to help you with your experience.</p>

              <h3 class="section-heading">When do we collect information?</h3>

              <p class="lead">We collect information from you when you register on our site, place an order, fill out a form, Open a Support Ticket or enter information on our site.</p>


              <h3 class="section-heading">How do we use your information?</h3>

              <p class="lead">We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
              <ul>
                    <li> To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
                    <li> To improve our website in order to better serve you.</li>
                    <li> To allow us to better service you in responding to your customer service requests.</li>
                    <li> To administer a contest, promotion, survey or other site feature.</li>
                    <li> To quickly process your transactions.</li>
                    <li> To ask for ratings and reviews of services or products</li>
                    <li> To follow up with them after correspondence (live chat, email or phone inquiries)</li>
                  </ul>
              </p>

              <h3 class="section-heading">How do we protect your information?</h3>
              <p class="lead">
              We do not use vulnerability scanning and/or scanning to PCI standards.
              An external PCI compliant payment gateway handles all CC transactions.
              We use regular Malware Scanning.
              </p>
              <p class="lead">
              Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.
              </p>
              <p class="lead">
              We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.
              </p>
              <p class="lead">
              All transactions are processed through a gateway provider and are not stored or processed on our servers.
              </p>
              <h3 class="section-heading">Do we use 'cookies'?</h3>
              <p class="lead">
              Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.
              </p>
              <p class="lead">
              We use cookies to:
                <ul>
                    <li> Help remember and process the items in the shopping cart.</li>
                    <li> Understand and save user's preferences for future visits.</li>
                </ul>
              </p>
              <p class="lead">
              You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
              </p>
              <p class="lead">
              If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.
              </p>

              <h3 class="section-heading">Third-party disclosure</h3>
              <p class="lead">
              We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.<br/>

              However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
              </p>

              <h3 class="section-heading">Third-party links</h3>

              <p class="lead">
              Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
              </p>

              <h3 class="section-heading">Google</h3>
              <p class="lead">
                Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en <br/><br/>

                We use Google AdSense Advertising on our website.<br/>

                Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.<br/><br/>

                We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.<br/><br/>

                <strong>Opting out:</strong><br/>
                Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.<br/>
                </p>

                <h3 class="section-heading">California Online Privacy Protection Act</h3>
                <p class="lead">
                  CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. <br/><br/>
                  - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf<br/><br/>

                  According to CalOPPA, we agree to the following:
                  <ul>
                    <li>Users can visit our site anonymously.</li>
                    <li>Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.</li>
                    <li>Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</li>
                  </ul>

                  <strong>You will be notified of any Privacy Policy changes:</strong><br/>
                  <ul>
                        <li> On our Privacy Policy Page </li>
                  </ul>
                  Can change your personal information:<ul>
                        <li> By calling us </li>
                        <li> By logging in to your account </li>
                        </ul>
                </p>

                <h3 class="section-heading">How does our site handle Do Not Track signals?</h3>
                <p class="lead">
                  We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.<br/>
                  It's also important to note that we do not allow third-party behavioral tracking.<br/><br/>

                  <strong>COPPA (Children Online Privacy Protection Act)</strong><br/>

                  When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.

                  We do not specifically market to children under the age of 13 years old.<br/><br/>

                  <strong>Fair Information Practices</strong><br/>

                  The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.

                  In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:<br/>
                  We will notify you via email within 7 business days.<br/><br/>

                  We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
                </p>
                <h3 class="section-heading">CAN SPAM Act</h3>
                <p class="lead">
                  The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.

                  We collect your email address in order to:<ul>
                        <li> Process orders and to send information and updates pertaining to orders.</li>
                        <li> Send you additional information related to your product and/or service</li>
                      </ul>
                  To be in accordance with CANSPAM, we agree to the following:<ul>
                        <li> Not use false or misleading subjects or email addresses.</li>
                        <li> Identify the message as an advertisement in some reasonable way.</li>
                        <li> Include the physical address of our business or site headquarters.</li>
                        <li> Monitor third-party email marketing services for compliance, if one is used.</li>
                        <li> Honor opt-out/unsubscribe requests quickly.</li>
                        <li> Allow users to unsubscribe by using the link at the bottom of each email.</li>
                      </ul>
                  If at any time you would like to unsubscribe from receiving future emails, you can email us by following the instructions at the bottom of each email and we will promptly remove you from ALL correspondence.
                </p>

              <h3 class="section-heading">Contacting Us</h3>
              <p class="lead">
                If there are any questions regarding this privacy policy, you may contact us using the information below.
              </p>

              <address>
                <strong>violetcell.org</strong><br/>
                13 Denmark street<br/>
                Oxford, OX4 1QS<br/>
                UK<br/>
                info@violetgene.org<br/>
              </address>

              <p class="lead">
                @if(isset($status))
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i>Message sent</h4>
                  Your message has been sent.
                </div>
                @endif
              </p>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-4">
            <button class="btn btn-lg btn-primary btn-block register-btn" type="" id="in_touch">Get in touch</button>
          </div>
        </div>

        <!-- <div class="row">
            <div class="col-lg-12 col-sm-6">
              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <h3 class="section-heading">More thanks</strong></h3>
              <p class="lead">
                We are very thankful for the pleasure of working alongside some of the worlds best
                educational organisations.
              </p>
              <br/>
            </div>
          </div> -->
    </div>
    <!-- /.container -->
  </div>


  <div class="modal fade" id="questions" tabindex="-1" role="dialog" aria-labelledby="Answers">

  </div>

@endsection

@section('js')
<script>
  var in_touch = document.getElementById("in_touch");
  var AnswerWindow = document.getElementById("questions");

  AnswerWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    in_touch.addEventListener('click', DisplayWin, false);
  }

  $(document).ready(function () {
      $('#questions').modal({show: false}) //Off by default
  });

  function DisplayWin(){
    //Get a topic for the topic window
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', Forms);
    requestX.open("get", "get_mail");
    requestX.send();
  }

  function Forms(data){
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      AnswerWindow.innerHTML = "";
      AnswerWindow.innerHTML = resp.html;
      $('#questions').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }
</script>
@endsection
