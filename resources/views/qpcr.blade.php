@extends('layouts.master')

@section('title', 'VioletGene')

@section('navigation')
  @include('layouts.navbar_mini')
@endsection


@section('content')
  <div class="content-section-a">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
                  <h2 class="section-heading">How genetic information is acquired</h2>
                  <p class="lead">
                    There are four fundamental steps to obtaining genetic data based on amplification:
                    <ol class="lead">
                    <li> Identifying the gene of interest</li>
                    <li> Isolating DNA from your sample</li>
                    <li> Perfoming the PCR reaction</li>
                    <li> Analyzing data</li>
                    </ol>
                  </p>
                  <p class="lead">
                    One of the most accurate and commonly used ways of detecting
                    known genes is using quantitative polymerase chain reaction - <strong>qPCR</strong> to
                    multiply a known gene and test the hypothesis for its presence in your sample. When
                    the amplification reaction is run, a qPCR instrument monitors its progress and
                    analyses, whether a gene of interest is being multiplied. This amplification
                    based approach is the cornerstone of many research and clinical diagnostics methods.
                  </p>
              </div>
          </div>

          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
                  <h3 class="section-heading">Step 1: <strong>identifying the gene of interest</strong></h3>
                  <p class="lead">
                    Biological species are different because of their DNA and this makes it possible to
                    formulate hypotheses using known gene sequences. By trying to amplify specific gene regions,
                    you can test for the presence or absence of specific species. Large amounts of genomic information
                    have been collected by sequencing and are contained in public databases. Searching these databases
                    it is possible to identify sequences (~ 150 base pairs), which are specific to your species of interest.
                  <br/>
                  <br/>
                    For experiments published here, sequences will have already been chosen for you, but if you are
                    designing new experiments, please consider the following sources of information:
                    <ul class="lead">
                      <li>Already published research articles</li>
                      <li>BLAST search tools, <a href="https://www.ncbi.nlm.nih.gov/tools/primer-blast/">link</a></li>
                      <li>Primer bank for human genes, <a href="https://pga.mgh.harvard.edu/primerbank/">link</a></li>
                    </ul>
                  </p>
              </div>
          </div>

          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
                  <h3 class="section-heading">Step 2: <strong>isolating DNA from your sample</strong></h3>
                  <p class="lead">
                    This is the most demanding step of the technique, which is designed to extract
                    genetic material from cells in the sample. DNA has to be purified from other cellular components,
                    some of which can be detrimental to the gene amplification reaction. This step is often customized,
                    depending on the type of the sample that DNA has to be extracted from, because some cells and tissues
                    are more difficult to break apart than others. For example DNA is easier to extract from processed foods,
                    where many cells have alaready been broken, but it is much more difficult to extract DNA from bacterial and
                    plant tissues, which have thick walls surrounding the cells.
                  </p>
                  <br/>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-sm-6">
                  <img class="img-responsive" src="img/purification.png" alt="">
              </div>
              <div class="col-lg-6 col-sm-6">
              <p class="lead">
                While there are several ways to extract DNA from tissues, we prefer to use techniques
                based on solid support, due to their simplicity. After mechanically breaking the cells
                to release their contents into the solution, fundamentally there are three steps for DNA
                isolation:
                <ol class="lead">
                  <li>Bind DNA to the solid support by passing the solution with lysed cell contents</li>
                  <li>Wash the solid support with bound DNA to remove other cellular components</li>
                  <li>Unbind DNA from the solid support to recover it in purified form</li>
                </ol>
              </p>
              </div>
            </div>


    <div class="row">
        <div class="col-lg-12 col-sm-6">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                <img class="img-responsive" src="img/Edu.png" alt="">
            </div> -->
            <h3 class="section-heading">Step 3: <strong>perfoming gene amplification</strong></h3>
            <p class="lead">
              Using genetic material extracted in the previous step and primers against the gene of interest,
              it is possible to test for presence or absence of that gene by performing polymerase chain reaction - (PCR).
              Fundamentally, the technique is equivalent to the original method developed in 1980s, however with the
              addition of fluorescence based readout, PCR technique becomes quantitative and does not require
              gel electrophoresis for result analysis. During the reaction, a specially designed dye only binds double
              stranded DNA, which makes it possible to monitor the amplification progress during thermocycling steps.
            </p>
            <br/>
        </div>
      </div>

      <div class="row">
          <div class="col-lg-6 col-sm-6">
              <img class="img-responsive" src="img/PCR.png" alt="">
          </div>
          <!-- <hr class="section-heading-spacer">
          <div class="clearfix"></div> -->
          <div class="col-lg-6 col-sm-6">
            <p class="lead">
              Thermocycling generally consists of 32-40 rounds of three temperature steps:
              <ol class="lead">
                <li>DNA melting to separate two complementary strands</li>
                <li>Annealing, to bind DNA primers to the gene of interest</li>
                <li>Extension, which synthesizes DNA strands at primer location</li>
              </ol>
            </p>
        </div>
      </div>

      <div class="row">
        <br/>
          <div class="col-lg-12 col-sm-6">
              <p class="lead">
                As the reaction progresses and if your gene of interest is amplified, fluroescence of the sample will
                increase. This is detected by the instrument and the data can then be used to make a conclusion about the presence
                or absence of your gene of interest.
              </p>
              <br/>
          </div>
        </div>


        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                    <img class="img-responsive" src="img/Edu.png" alt="">
                </div> -->
                <h3 class="section-heading">Step 4: <strong>analyzing data</strong></h3>
                <p class="lead">
                  In order to estimate the amount of gene of interest present in your sample
                  fluorescence readings can be used to find the step at which DNA amplification
                  occurs the fastest. Depending on the amount of original material present, this
                  step can occur in the early or late thermocycling steps. Keeping in mind, that
                  the amount of DNA is doubled in every step, this step number can then be used to
                  estimate the amount of original material present in the sample.

                </p>
                <br/>
            </div>

            <div class="col-lg-12 col-sm-6">
                <img class="img-responsive" src="img/Example.jpg" alt="">
            </div>
          </div>

          <div class="row">
              <div class="col-lg-12 col-sm-6">
                <br/>
                  <p class="lead">
                    After the instruments measures sample fluorescence, the data is uploaded to
                    the servers to perform downstream data analysis and make conclusions based on the readings.
                  </p>
                  <br/>
              </div>
            </div>

      <!-- /.container -->
  </div>
@endsection

@section('footer')
  @include('layouts.footer')
@endsection
