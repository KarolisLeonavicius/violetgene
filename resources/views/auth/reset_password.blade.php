@extends('layouts.master')

@section('title', 'VioletGene')

@section('navigation')
  @include('layouts.navbar_mini')
@endsection


@section('content')

@section('head')
    <link rel="stylesheet" href="/css/reset.css">
@stop

@section('content')
  <div class="content-section-a">
    {!! Form::open(['url' => url('/password/email'), 'class' => 'form-signin' ] ) !!}

    @include('includes.status')

    <h2 class="form-signin-heading">Password Reset</h2>
    <label for="inputEmail" class="sr-only">Email address</label>
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email address', 'required', 'autofocus', 'id' => 'inputEmail' ]) !!}

    <br />
    <button class="btn btn-lg btn-primary btn-block" type="submit">Send me a reset link</button>

    {!! Form::close() !!}
  </div>
@endsection

@section('footer')
  @include('layouts.footer')
@endsection
