@extends('layouts.master')

@section('title', 'VioletGene')

@section('navigation')
  @include('layouts.navbar_mini')
@endsection


@section('content')

  <div class="content-section-a">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
                  <!-- <h2 class="section-heading">Overview of the practical experiment</h2>
                  <p class="lead">All living things are defined by their DNA, and looking at
                    genetic information makes it possible to identify and characterise the
                    biology around us. Therefore, it is unsurprising, that genetic testing methodology
                    has become the basis of research, diagnostics and recently, a consumer service.
                  </p> -->
              </div>
          <div class="col-lg-10 col-sm-6">
              <img class="img-responsive" src="img/Mobile_qPCR.png" alt="">
          </div>
      </div>
      <!-- /.container -->
  </div>
@endsection

@section('footer')
  @include('layouts.footer')
@endsection
