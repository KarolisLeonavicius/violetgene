<br/>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading"><h3>{{$test->title}}</h3></div>
    <div class="panel-body">

      <div class="row">
        <div class="col-lg-6 col-sm-6">
          <br/>
          <h4>Description</h4>
            <p class="lead">
            {!!$test->comments!!}
            </p>
            <button onclick="quick_view('test', {{$test->id}})" class="btn btn-block btn-success">View</button>
            <br/>
        </div>
        <div class="col-lg-6 col-sm-6">
              <!-- <img class="img-responsive" src="img/Tests.png" alt=""> -->
              <table class="table">
                <caption>Gene panel: 6 reactions</caption>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Replicates</th>
                    <th>Gene</th>
                    <th>Error rate</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Chicken</td>
                    <td>2</td>
                    <td>CytB</td>
                    <td>25%</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Pork</td>
                    <td>2</td>
                    <td>CytB</td>
                    <td>25%</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Beef</td>
                    <td>2</td>
                    <td>CytB</td>
                    <td>25%</td>
                  </tr>
                </tbody>
              </table>
        </div>
      </div>


    </div>
  </div>
</div>
