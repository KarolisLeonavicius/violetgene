<div class="row margin-bottom-10">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <a href="/social/redirect/facebook" class="btn btn-lg waves-effect waves-light btn-block facebook"><div style="color:white"> Facebook </div></a>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <a href="/social/redirect/google" class="btn btn-lg waves-effect waves-light btn-block google"><div style="color:white"> Google+ </div></a>
    </div>
</div>
