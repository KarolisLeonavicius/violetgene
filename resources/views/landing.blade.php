@extends('layouts.master')

@section('title', 'VioletGene')

@section('navigation')
  @include('layouts.navbar')
@endsection

@section('header')
  @include('layouts.intro')
@endsection

@section('content')

	<a  name="about"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Genetic testing</h2>
                    <p class="lead">
                      <strong>How the platform works:</strong><br/><br/>
                      All living things are defined by their DNA, and looking at
                      genetic information makes it possible to identify and characterise the
                      biology around us. As the result, genetic testing methodology
                      has become the foundation of modern research, diagnostics and recently,
                      consumer genotyping. We provide the simple means to obtain
                      genetic information, based on quantitative PCR methodology
                      and tests designed by us and our users.<br/><br/>

                      As a platform user you can view shared genetic test results published by testers. If you are a student
                      at a registered school, here you will also receive additional resources to help you learn genetics.
                      Logged in as a user, you can also support genetics education by donating money to make these practical
                      experiments available to more schools.
                    </p>
                </div>
                <div class="col-lg-4 col-lg-offset-0 col-sm-6">
                    <img class="img-responsive" src="img/DNA3.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->
    </div>
    <a  name="tests"></a>
      <div class="content-section-b">
          <div class="container">
              <div class="row">
                  <div class="col-lg-6 col-lg-offset-0 col-sm-push-5  col-sm-6">
                      <hr class="section-heading-spacer">
                      <div class="clearfix"></div>
                      <h2 class="section-heading">Available tests</h2>
                      <p class="lead">It is possible to target any known
                        gene and this way answer questions ranging from sandwitch
                        content to eye colors. For educational purposes we focus
                        on testing food, however this section will be updated with
                        additional tests, as they are designed by our users.
                      </p>
                      <br/>
                      <br/>
                      <a class="btn btn-lg btn-success col-lg-4 col-sm-6" href="/database" role="button">View available tests</a>
                  </div>
                  <div class="col-lg-4 col-sm-pull-6  col-sm-6 hidden-xs">
                      <img class="img-responsive" src="img/Tests.png" alt="">
                  </div>
              </div>
          </div>
          <!-- /.container -->
      </div>
      <!-- /.content-section-b -->


	<a  name="method"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">How it works</h2>
                    <p class="lead">
                      We use polymerase chain reaction to amplify genes,
                      which are only present in the species of interest. This way it
                      is possible to test for the presence or absence of specific
                      biological species in your sample.
                      <br/>
                      <br/>
                      <a class="btn btn-lg btn-primary col-lg-4 col-sm-6" href="/qpcr" role="button">More information</a>
                    </p>
                </div>
                <div class="col-lg-4 col-lg-offset-1 col-sm-6 hidden-xs">
                    <img class="img-responsive" src="img/How.png" alt="">
                </div>
            </div>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <!-- /.content-section-a -->
	<a  name="education"></a>
    <div class="content-section-b">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-0 col-sm-push-5  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Genetics education</h2>
                    <p class="lead">
                      We are passionate proponents of learning by doing,
                      which is why we have created an this easy to use tool
                      kit. During the experiment you will have the opportunity to
                      take a different perspective on the food around you, by performing
                      genetic testing for different types of meat.
                      <br/>
                      <br/>
                      <a class="btn btn-lg btn-success col-lg-4 col-sm-6" href="/experiment" role="button">More information</a>
                    </p>
                </div>
                <div class="col-lg-4 col-sm-pull-6  col-sm-6 hidden-xs">
                    <img class="img-responsive" src="img/Edu.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->


    <a  name="pricing"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-6">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <h2 class="section-heading">Pricing</h2>
                      <div class="row">
                        <div class="col-lg-11 col-lg-push-1 col-sm-6">
                          <div class="col-lg-4">
                              <div class="panel panel-success">
                                  <div class="panel-heading">
                                      <h4 class="text-center">FREE package</h4>
                                  </div>
                                  <div class="panel-body text-center">
                                      <p class="lead">
                                          <strong>£0</strong>
                                      </p>
                                  </div>
                                  <ul class="list-group list-group-flush text-center">
                                      <li class="list-group-item">
                                          View shared genetic tests
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Learn to evaluate genetic data
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Donate for school experiments
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Run genetic tests
                                          <span class="glyphicon glyphicon-remove pull-right"></span>
                                      </li>
                                  </ul>
                                  <div class="panel-footer">
                                      <a class="btn btn-lg btn-block btn-success" href="/login">Log in</a>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-4">
                              <div class="panel panel-info">
                                  <div class="panel-heading">
                                      <h4 class="text-center">Classroom experiment</h4>
                                  </div>
                                  <div class="panel-body text-center">
                                      <p class="lead">
                                          <strong>£100 / classroom</strong>
                                      </p>
                                  </div>
                                  <ul class="list-group list-group-flush text-center">
                                      <li class="list-group-item">
                                          Teach practical genetics skills
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Designed for classrooms of 12-36
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Includes 12 DNA extractions
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Includes 2 gene panels
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                  </ul>
                                  <div class="panel-footer">
                                      <a class="btn btn-lg btn-block btn-info" href="/login">Log in</a>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-4">
                              <div class="panel panel-primary">
                                  <div class="panel-heading">
                                      <h4 class="text-center">Custom experiments</h4>
                                  </div>
                                  <div class="panel-body text-center">
                                      <p class="lead">
                                          <strong>£30 / day + £4 / test</strong>
                                      </p>
                                  </div>
                                  <ul class="list-group list-group-flush text-center">
                                      <li class="list-group-item">
                                          Designed for customized tests
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Test any gene target
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          Includes 1 day instrument rent
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                      <li class="list-group-item">
                                          1 DNA extraction and panel of 6 genes
                                          <span class="glyphicon glyphicon-ok pull-right"></span>
                                      </li>
                                  </ul>
                                  <div class="panel-footer">
                                      <a class="btn btn-lg btn-block btn-primary" href="/login">Log in</a>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                <!-- <div class="col-lg-4 col-lg-offset-1 col-sm-6">
                    <img class="img-responsive" src="img/Price.png" alt="">
                </div> -->
            </div>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

	<a  name="contact"></a>
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Login to start testing:</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                      @if(!Auth::check())
                        <li>
                            <a href="/login" class="btn btn-default btn-lg"><i class="fa fa-user fa-fw"></i> <span class="network-name">Login</span></a>
                        </li>
                        <li>
                            <a href="/register" class="btn btn-default btn-lg"><i class="fa fa-plus fa-fw"></i> <span class="network-name">Register</span></a>
                        </li>
                        @else
                        <li>
                            <a href="/user/home" class="btn btn-default btn-lg"><i class="fa fa-user fa-fw"></i> <span class="network-name">My account</span></a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.banner -->
@endsection

@section('footer')
  @include('layouts.footer')
@endsection
