
@extends('layouts.master')

@section('title', 'VioletGene')

@section('content')
  <div class="content-section-a">
      <div class="container">
        <a href="/">Homepage</a>
        <h1> About @yield('title')</h1>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                  <img class="img-responsive" src="img/Edu.png" alt="">
              </div> -->
              <h3 class="section-heading">Our purpose</h3>
              <p class="lead">
                Genetics is one of the few tools, that enables us to objetively
                answer questions about living objects and subjects. Traditionally,
                it has been reserved to specialists in research environments,
                but now everyone can use genetics services to gather biological
                information. We believe that the purpose of science is to empower
                and we do it by providing the tools and basic education for
                responsible data gathering, using and sharing.
                <br/>
                <br/>
                <strong>Karolis Leonavicius</strong> - @yield('title') author and EIT Health fellow.
                <div class="row">
                  <div class="col-lg-4">
                    <button class="btn btn-lg btn-primary btn-block register-btn" type="" id="in_touch">Get in touch</button>
                  </div>
                </div>
                @if(isset($status))
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i>Message sent</h4>
                  Your message has been sent.
                </div>
                @endif
              </p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                  <img class="img-responsive" src="img/Edu.png" alt="">
              </div> -->
              <h3 class="section-heading">Thanks</h3>
              <p class="lead">
                <strong>EIT Health and Oxford University</strong><br/>
                The project has been started up by the European Institute of Technology (EIT Health)
                and Oxford University. We are also very grateful for all the organizational support
                provided by Oxford University Innovations, Ltd. It is not an overstatement, that without
                them, the initiative would not have been possible.
              </p>
              <img src="/img/eit.png"> <img src="/img/OU.png">
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-lg-12 col-sm-6">
              <hr class="section-heading-spacer">
              <div class="clearfix"></div>
              <h3 class="section-heading">More thanks</strong></h3>
              <p class="lead">
                We are very thankful for the pleasure of working alongside some of the worlds best
                educational organisations.
              </p>
              <br/>
            </div>
          </div> -->
    </div>
    <!-- /.container -->
  </div>

  <div class="modal fade" id="questions" tabindex="-1" role="dialog" aria-labelledby="Answers">

  </div>

@endsection

@section('js')
<script>
  var in_touch = document.getElementById("in_touch");
  var AnswerWindow = document.getElementById("questions");

  AnswerWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    in_touch.addEventListener('click', DisplayWin, false);
  }

  $(document).ready(function () {
      $('#questions').modal({show: false}) //Off by default
  });

  function DisplayWin(){
    //Get a topic for the topic window
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', Forms);
    requestX.open("get", "get_mail");
    requestX.send();
  }

  function Forms(data){
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      AnswerWindow.innerHTML = "";
      AnswerWindow.innerHTML = resp.html;
      $('#questions').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }
</script>
@endsection
