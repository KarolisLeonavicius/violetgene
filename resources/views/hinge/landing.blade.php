@extends('layouts.master')

@section('title', 'HingeProduction')



@section('content')
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <h2 class="section-heading">Wellcome to Hinge Production Log</h2>
                <form  method="get" action="/simon/productionlog/update">
                  <button type="submit" class="btn btn-lg btn-success col-lg-5">Refresh database</button>
                </form>
                <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                    <img class="img-responsive" src="img/Edu.png" alt="">
                </div> -->
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>

@if(isset($message))
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <h4 class="section-heading">{{$message}}</h4>
                <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                    <img class="img-responsive" src="img/Edu.png" alt="">
                </div> -->
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>
@endif


<div class="content-section-b">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-sm-push-0  col-sm-4">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Database summary</h2>
                <p class="lead">
                  @if(isset($summary))
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Total devices</th>
                          <th>Total entries</th>
                          <th>Last updated</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">{{$summary["total_devices"]}}</th>
                          <td>{{$summary["total_entries"]}}</td>
                          <td>{{$summary["last_updated"]}}</td>
                        </tr>
                      </tbody>
                    </table>
                  @endif
                </p>
            </div>
        </div>
        <hr class="section-heading-spacer">
        <div class="clearfix"></div>
        @if(isset($hinge_display))
        <table class="table">
          <caption>Hinge production log</caption>
          <thead>
            <tr>
              <th>Device ID</th>
              <th>Bluetooth name</th>
              <th>Battery</th>
              <th>Pitch</th>
              <th>Roll</th>
              <th>X initial</th>
              <th>X final</th>
              <th>Y initial</th>
              <th>Y final</th>
              <th>Z initial</th>
              <th>Z final</th>
              <th>Details</th>
            </tr>
          </thead>
          <tbody>
          @foreach($hinge_display as $key => $device)
          <tr>
            <th scope="row">{{$key}}</th>
            <td>@if(isset($device["bt_name"])){{$device["bt_name"]}}@else ---- @endif</td>
            <td>@if(isset($device["battery"])){{$device["battery"]}}@else ---- @endif</td>
            <td>@if(isset($device["pitch"])){{$device["pitch"]}}@else ---- @endif</td>
            <td>@if(isset($device["roll"])){{$device["roll"]}}@else ---- @endif</td>
            <td>@if(isset($device["X_init"])){{$device["X_init"]}}@else ---- @endif</td>
            <td>@if(isset($device["X_fin"])){{$device["X_fin"]}}@else ---- @endif</td>
            <td>@if(isset($device["Y_init"])){{$device["Y_init"]}}@else ---- @endif</td>
            <td>@if(isset($device["Y_fin"])){{$device["Y_fin"]}}@else ---- @endif</td>
            <td>@if(isset($device["Z_init"])){{$device["Z_init"]}}@else ---- @endif</td>
            <td>@if(isset($device["Z_fin"])){{$device["Z_fin"]}}@else ---- @endif</td>
            <td><a href="/simon/productionlog/view/{{$key}}" type="button" class="btn btn-primary">View</a></td>
          </tr>
          @endforeach
          </tbody>
        </table>
        @endif

    </div>
    <!-- /.container -->
</div>
@endsection
