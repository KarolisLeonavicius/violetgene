@extends('layouts.master')

@section('title', 'HingeDevice')

@section('content')
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <h2 class="section-heading">Device summary: {{$MAC}}</h2>
                <form  method="get" action="/simon/productionlog/view/0">
                  <button type="submit" class="btn btn-lg btn-success col-lg-5">Back to device list</button>
                </form>
                <br/>
                <hr>
                <h4 class="section-heading">Step1: Bluetooth name:</h4>
                <ul>
                  @if(isset($BT_names))
                    @foreach($BT_names as $BT_name)
                    <li>
                      {{json_decode($BT_name, true)[0]}} <br/>
                    </li>
                    @endforeach
                  @endif
                </ul>
                <hr>
                <h4 class="section-heading">Step2: Battery readings:</h4>
                <div class="row">
                  @foreach($battery_charts as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                </div>
                <hr>

                <h4 class="section-heading">Step3: Initial readings:</h4>
                <div class="row">
                  @foreach($initial_IMU_charts as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                  @foreach($initial_IMU_chartsXY as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                </div>
                <hr>

                <h4 class="section-heading">Step4: Post calibration readings:</h4>
                <div class="row">
                  @foreach($final_IMU_charts as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                  @foreach($final_IMU_chartsXY as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                </div>
                <hr>

                <h4 class="section-heading">Step5: Readings after main firmware upload</h4>
                <div class="row">
                  @foreach($cal_IMU_charts as $key => $chart)
                    <div class="col-md-4">
                      <div id="chart_div{{$key}}"></div>
                      {!!$chart!!}
                    </div>
                  @endforeach
                </div>
                <hr>

                <h4 class="section-heading">Raw messages</h4>
                @if(isset($hinge))
                <table class="table">
                  <caption>Message log</caption>
                  <thead>
                    <tr>
                      <th>MessageType</th>
                      <th>Timestamp</th>
                      <th>Details</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($hinge as $entry)
                  <tr>
                    <th scope="row">{{$entry->MessageType}}</th>
                    <th scope="row">{{$entry->timestamp}}</th>
                    <td><th scope="row">{{$entry->message}}</th></td>
                  </tr>
                  @endforeach
                  </tbody>
                @endif
                <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                    <img class="img-responsive" src="img/Edu.png" alt="">
                </div> -->
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>
@endsection
