{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'View orders')

@section('head')
@stop

@section('content')

<div class="box box-primary box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">My orders</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Id</strong></p>
      </td>
      <td>
        <p class=""><strong>Panels</strong></p>
      </td>
      <td>
        <p class=""><strong>DNA extractions</strong></p>
      </td>
      <td>
        <p class=""><strong>Booking days</strong></p>
      </td>
      <td>
        <p class=""><strong>From</strong></p>
      </td>
      <td>
        <p class=""><strong>To</strong></p>
      </td>
      <td>
        <p class=""><strong>Print</strong></p>
      </td>
      <td>
        <p class=""><strong>Payment</strong></p>
      </td>
      <td>
        <p class=""><strong>Paid</strong></p>
      </td>
      <td>
        <p class=""><strong>Created</strong></p>
      </td>
    </tr>
    @foreach($quotes as $quote)
    <tr>
      <td>
        <p class="" >{{$quote->id}}</p>
      </td>
      <td>
        <p class="" >{{$quote->panels}}</p>
      </td>
      <td>
        <p class="">{{$quote->extractions}}</p>
      </td>
      <td>
        <p class="">{{$quote->instruments}}</p>
      </td>
      <td>
        @if(isset($quote->from))
          <p class="">{{\Carbon\Carbon::createFromTimestamp($quote->from)->format('Y-m-d')}}</p>
        @endif
      </td>
      <td>
        @if(isset($quote->to))
          <p class="">{{\Carbon\Carbon::createFromTimestamp($quote->to-86400)->format('Y-m-d')}}</p>
        @endif
      </td>
      <td>
        @if($quote->paid)
          <a class="btn btn-sm btn-success btn-block register-btn" href="/order/view/{{$quote->id}}" target="_blank">Invoice</a>
        @else
          <a class="btn btn-sm btn-success btn-block register-btn" href="/order/view/{{$quote->id}}" target="_blank">Quote</a>
        @endif
      </td>
      <td>
        @if($quote->paid)
          <a class="btn btn-sm btn-success btn-block register-btn" disabled>Pay</a>
        @else
          <a class="btn btn-sm btn-success btn-block register-btn" disabled>Pay</a>
        @endif
      </td>
      <td>
        @if($quote->paid)
          Yes
        @else
          No
        @endif
      </td>
      <td>
          <p class="">{{$quote->created_at->diffForHumans()}}</p>
      </td>
    </tr>
    @endforeach
    </table>
  </div>
  <!-- /.box-body -->
</div>
@stop

@section('js')
@stop
