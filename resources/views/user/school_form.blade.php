
@include('includes.errors')

<h3>About your school</h3>
<button id="show_dialog" class="btn btn-lg btn-info" type="button">Please select your school</button>

@if(is_null($school))
    <div id="main_form">
      <div class="form-group">
        <label>School type</label>
        <select name="type" class="form-control">
          <option >Secondary school</option>
          <option >Sixth-form college</option>
          <option >Faith school</option>
          <option >Academy</option>
          <option >Other</option>
          <option >Not a school</option>
        </select>
      </div>

      <div class="form-group">
        <label>Examination board</label>
        <select name="board" class="form-control">
          <option >AQA</option>
          <option >OCR</option>
          <option >Edexel</option>
          <option >WJEC</option>
        </select>
      </div>

      <div class="row">
        <div class="col-lg-6">
          <label>Total biology students (Year 11-13)</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-users"></i></span>
            <input name="total" type="number" class="form-control" placeholder="64" min="0" max="1000" required="" value="">
          </div>
          </div>
          <div class="col-lg-6">
          <label>Average class size</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input name="class" type="number" class="form-control" placeholder="18" min="0" max="100" required="" value="">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6">
          <label>School name</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-bank"></i></span>
            <input name="name" type="text" class="form-control" placeholder="E.g.: New college" required="" value="">
          </div>
          </div>
          <div class="col-lg-6">
          <label>Postcode</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-bullseye"></i></span>
            <input name="postcode" type="text" class="form-control" placeholder="E.g.: AV13 15TH" required="" value="">
          </div>
        </div>
      </div>

    <div class="form-group">
      <h3>Delivery/pickup address</h3>
      <div class="row">
        <div class="col-lg-12">
          <label>Street address</label>
          <input name="street" type="text" class="form-control" placeholder="E.g.: 13 France av." required="" value="">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <label>City</label>
          <input name="city" type="text" class="form-control" placeholder="E.g.: Leicester" required="" value="">
          </div>
          <div class="col-lg-6">
          <label>Country</label>
          <input name="country" type="text" class="form-control" placeholder="E.g.: UK" required="" value="">
        </div>
      </div>
    </div>
  </div>
@else
  <div class="form-group">
    <label>School type</label>
    <select name="type" class="form-control">
      <option <?php if($school->type=="Secondary school") echo "selected"?> >Secondary school</option>
      <option <?php if($school->type=="Sixth-form college") echo "selected"?> >Sixth-form college</option>
      <option <?php if($school->type=="Faith school") echo "selected"?> >Faith school</option>
      <option <?php if($school->type=="Academy") echo "selected"?> >Academy</option>
      <option <?php if($school->type=="Other") echo "selected"?> >Other</option>
      <option <?php if($school->type=="Not a school") echo "selected"?> >Not a school</option>
    </select>
  </div>

  <div class="form-group">
    <label>Examination board</label>
    <select name="board" class="form-control">
      <option <?php if($school->board=="AQA") echo "selected"?> >AQA</option>
      <option <?php if($school->board=="OCR") echo "selected"?> >OCR</option>
      <option <?php if($school->board=="Edexel") echo "selected"?> >Edexel</option>
      <option <?php if($school->board=="WJEC") echo "selected"?> >WJEC</option>
    </select>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <label>Total biology students (Year 11-13)</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-users"></i></span>
        <input name="total" type="number" class="form-control" placeholder="64" required="" value="{{$school->students}}">
      </div>
      </div>
      <div class="col-lg-6">
      <label>Average class size</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <input name="class" type="number" class="form-control" placeholder="18" required="" value="{{$school->class_size}}">
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <label>School name</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-bank"></i></span>
        <input name="name" type="text" class="form-control" placeholder="E.g.: New college" required="" value="{{$school->name}}">
      </div>
      </div>
      <div class="col-lg-6">
      <label>Postcode</label>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-bullseye"></i></span>
        <input name="postcode" type="text" class="form-control" placeholder="E.g.: AV13 15TH" required="" value="{{$school->postcode}}">
      </div>
    </div>
  </div>

  <div class="form-group">
    <h3>Delivery/pickup address</h3>
    <div class="row">
      <div class="col-lg-12">
        <label>Street address</label>
        <input name="street" type="text" class="form-control" placeholder="E.g.: 13 France av." required="" value="{{$school->address}}">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <label>City</label>
        <input name="city" type="text" class="form-control" placeholder="E.g.: Leicester" required="" value="{{$school->city}}">
        </div>
        <div class="col-lg-6">
        <label>Country</label>
        <input name="country" type="text" class="form-control" placeholder="E.g.: UK" required="" value="{{$school->country}}">
      </div>
    </div>
  </div>
@endif
