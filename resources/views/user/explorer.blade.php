{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Explore genetic testing</h1>
@stop

@section('content')
<div class="row">

    <div class="col-lg-6">
      <!-- About Me Box -->
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Genetic tests</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="tests">
              @if(isset($testsHTML))
                {!!$testsHTML!!}
              @endif
          </div>
          <!-- /.tab-pane -->
      </div>
    </div>
  <!-- /.col-lg6 -->

  <div class="col-lg-6">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active" ><a href="#experiments" data-toggle="tab">Test templates</a></li>
        <li><a href="#panels" data-toggle="tab">Gene panels</a></li>
        <li><a href="#targets" data-toggle="tab">Gene targets</a></li>
        <li><a href="#extractions" data-toggle="tab">Extraction methods</a></li>
      </ul>
      <div class="tab-content">

        <div class="active tab-pane" id="experiments">

            @if(isset($templatesHTML))
              {!!$templatesHTML!!}
            @endif

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="panels">

            @if(isset($panelsHTML))
              {!!$panelsHTML!!}
            @endif

        </div>
        <!-- /.tab-pane -->

        <div class="tab-pane" id="targets">

            @if(isset($targetsHTML))
              {!!$targetsHTML!!}
            @endif

        </div>
        <!-- /.tab-pane -->

        <div class="tab-pane" id="extractions">

          @if(isset($extractionsHTML))
            {!!$extractionsHTML!!}
          @endif

        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
  </div>
  <!-- /.col-lg6 -->


</div>
<!-- /.row -->
@stop

@section('css')
@stop

@section('js')
    <script>
    function quick_view(type, id){
      var fd = new FormData();
      var requestX = new  XMLHttpRequest();
      fd.append("_token", "{{csrf_token()}}");
      requestX.addEventListener('load', ViewResponse);
      if(type == 'test'){
        requestX.open("post", "/test/getTest/"+id);
        requestX.send(fd);
      }
    }

    function ViewResponse(data){
      console.log(data.currentTarget.response);
      var resp = JSON.parse(data.currentTarget.response);

      if(resp.success>0){
        document.getElementById("test_container").innerHTML = resp.html;
      }
      else{
        document.getElementById("test_container").innerHTML = "Could not load test.";
      }
    }

    function like(link, id){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessLink);
      requestX.open("post", link);
      if(document.getElementById("test_"+id).innerHTML != "Cheers!"){
        document.getElementById("test_"+id).innerHTML = "Cheers!";
        requestX.send(formdata);
      }
    }

    function ProcessLink(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        document.getElementById("like_count"+resp.id).innerHTML = parseInt(document.getElementById("like_count"+resp.id).innerHTML) + 1;
      }
      else{
        console.log(data.currentTarget.response);
      }
    }

    function comment(link, id){
      console.log(document.getElementById("comment_test"+id).value);
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("comment", document.getElementById("comment_test"+id).value);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessResponse);
      requestX.open("post", link);
      requestX.send(formdata);
      document.getElementById("comment_box_test"+id).innerHTML = document.getElementById("comment_box_test"+id).innerHTML + "<img class='img-circle img-sm' src='/uploads/avatars/{{$user->avatar}}' alt='User Image'><div class='comment-text'><span class='username'>{{$user->first_name}} {{$user->last_name}} <span class='text-muted pull-right'>just now</span></span>"+document.getElementById("comment_test"+id).value+"</div>";
    }

    function ProcessResponse(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        document.getElementById("comment_count"+resp.id).innerHTML = parseInt(document.getElementById("comment_count"+resp.id).innerHTML) + 1;
      }
      else{
        console.log(data.currentTarget.response);
      }
    }

    </script>
@stop
