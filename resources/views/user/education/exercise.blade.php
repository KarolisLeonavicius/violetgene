{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Editing exercises')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

@if(isset($status))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-warning"></i>Status update</h4>
  Operation successful.
</div>
@endif

<div class="row">
  <div class="col-lg-6">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Multiple choice questions</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="TopicWindow">
        @if(!isset($exercise))
        <form action="/topic/createExam", class="form-signin", method="post">
        @else
        <form action="/topic/editExam/{{$exercise->id}}", class="form-signin", method="post">
        @endif
          {{csrf_field()}}
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label>Base topic:</label>
                @if(!isset($exercise))
                <select name="topic" class="form-control">
                  <option value="None">None</option>
                  @if(isset($topics))
                    @foreach($topics as $topic_item)
                      <option value="{{$topic_item->id}}">{{$topic_item->title}}</option>
                    @endforeach
                  @endif
                </select>
                @else
                <select name="topic" class="form-control">
                  <option value="None" @php if(!isset($exercise->topic_id)) echo "selected" @endphp> None</option>
                  @if(isset($topics))
                    @foreach($topics as $topic_item)
                      <option @php if(isset($exercise->topic_id)) if($exercise->topic_id == $topic_item->id) echo "selected" @endphp value="{{$topic_item->id}}">{{$topic_item->title}}</option>
                    @endforeach
                  @endif
                </select>
                @endif
              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <label>Privacy:</label>
                @if(!isset($exercise))
                <select name="privacy" class="form-control">
                  <option>Shared</option>
                  <option>Public</option>
                  <option>Private</option>
                </select>
                @else
                <select name="privacy" class="form-control">
                  <option @php if($exercise->privacy == "Shared") echo "selected" @endphp>Shared</option>
                  <option @php if($exercise->privacy == "Public") echo "selected" @endphp>Public</option>
                  <option @php if($exercise->privacy == "Private") echo "selected" @endphp>Private</option>
                </select>
                @endif
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
                <h3>Question body:</h3>
                @if(!isset($exercise))
                <textarea class="input-block-level" id="summernote" name="content" rows="5"><h3>Heading</h3><p>Please enter exercise content here...</p></textarea>
                @else
                <textarea class="input-block-level" id="summernote" name="content" rows="5">{{$content}}</textarea>
                @endif
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#summernote').summernote({
                      height: "200px"
                    });
                  });
                  var postForm = function() {
                    var content = $('textarea[name="content"]').html($('#summernote').code());
                  }
                </script>
              </div>
            <!-- /.box-body -->
          </div>
          <hr/>

          <div class="row">
            <div class="col col-lg-12">
                <h3>Add multiple choices:</h3>
                <div id="multiple_choices">
                  <div class="row">
                    <div class="col-xs-9">
                        <h4>Question:</h4>
                    </div>
                    <div class="col-xs-3">
                        <h4>Correct answer</h4>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  @if(!isset($exercise))
                  <div class="form-group" id="question_0">
                    <div class="row">
                      <div class="col-xs-9">
                          <input class="form-control" type="text" name="question_0" value="" placeholder="Enter question text" required>
                      </div>
                      <div class="col-xs-3">
                        <label><input type="checkbox" value="" name="answer_0">True?</label>
                      </div>
                    </div>
                  </div>
                  @else
                    @foreach($exercise->questions as $question)
                      <div class="row" id="question_{{$question->id}}">
                        <div class="col-md-6 col-sm-12">
                            <input class="form-control" type="text" name="question_{{$question->id}}" value="{{$question->id}}" placeholder="Enter question text" required>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <input class="form-control" type="checkbox" name="answer_{{$question->id}}" @if($exercise->question->answer) checked @endif>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            <!-- /.box-body -->
          </div>
          <div class="row">
            <div class="col col-lg-4">
              <button type="button" class="btn btn-lg btn-success" onclick="add_question()">+ add question</button>
            </div>
          </div>

          <hr/>
          <div class="row">
            <div class="col-lg-4">
              <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save exercise</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.box -->
  </div>

<!-- @if(\App\Models\Role::find($user->roles()->first()->id)->name=='admin')
  @if(!$user->activated)
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. Your approval as an administrator is currently pending.
  </div>
  @else
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status update</h4>
    You can edit educational material as an administrator at: {{$school->name}}.
  </div>
  @endif
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. You are currently registered as a {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
  </div>
@endif -->

@if(isset($exercises))
  <div class="col-lg-6">
    <div class="box box-default box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Edit your exercises</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="box-body" id="TopicWindow">
            <form action="/topic/editExam", class="form-signin", method="get">
              {{csrf_field()}}
              <div class="col-lg-8">
                <div class="form-group">
                  <select name="id" class="form-control">
                    @foreach($exercises as $exercise_item)
                      <option value="{{$exercise_item->id}}">{{$exercise_item->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-default btn-block register-btn">Edit exercise</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
@endif
</div>

<div class="modal fade" id="FlashListModal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')

<script>

  var ModalWindow = document.getElementById("FlashListModal");

  ModalWindowinnerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    $('FlashListModal').modal({show: false}) //Off by default
  }

  function GetList(){
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', ProcessList);
    requestX.open("get", "/topic/getFlashList");
    requestX.send();
    document.getElementById("GetListButton").innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  }

  function ProcessList(data){
    document.getElementById("GetListButton").innerHTML = 'Get my Flash Card List';
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      ModalWindow.innerHTML = "";
      ModalWindow.innerHTML = resp.html;
      $('#FlashListModal').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }

  @if(!isset($exercise))
    var question_count = 0;
  @else
    var question_count = {{count($exercise->questions)}};
  @endif

  function add_question(){
    var question_div  = document.getElementById("multiple_choices");
    question_count++;
    var node = document.createElement("div");
    node.Id = "question_"+question_count;
    node.innerHTML = '<div class="form-group" id="question_'+question_count.toString()+'"><div class="row"><div class="col-xs-9"><input class="form-control" type="text" name="question_0" value="" placeholder="Enter question text" required></div><div class="col-xs-2"><label><input type="checkbox" value="" name="answer_0">True?</label></div><div class="col-xs-1"><button type="button" class="btn btn-danger" onclick="remove_question('+question_count.toString()+')">X</button></div></div></div>';
    question_div.appendChild(node);
  }

  function remove_question(id){
    var question_div  = document.getElementById("multiple_choices");
    var question  = document.getElementById("question_"+id);
    question.parentNode.removeChild(question);
  }
</script>

@endsection
