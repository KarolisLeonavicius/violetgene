{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'New Topic')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

@if(\App\Models\Role::find($user->roles()->first()->id)->name=='admin')
  @if(!$user->activated)
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. Your approval as an administrator is currently pending.
  </div>
  @else
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status update</h4>
    You can edit educational material as an administrator at: {{$school->name}}.
  </div>
  @endif
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. You are currently registered as a {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
  </div>
@endif

@if(isset($status))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-warning"></i>Status update</h4>
  Operation successful.
</div>
@endif

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create topic content</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="TopicWindow">
        <form action="/topic/createTopic", class="form-signin", method="post">
          {{csrf_field()}}
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label>Topic category</label>
                <select name="school_role" class="form-control">
                  <option>Genetics</option>
                  <option>Testing technologies</option>
                  <option>Data science</option>
                </select>
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <label>Builds upon:</label>
                <select name="school_role" class="form-control">
                  <option>None</option>
                  <option>Intro to Genetics</option>
                  <option>Recombinant technology</option>
                </select>
              </div>
            </div>
            
            <div class="col-lg-3">
              <div class="form-group">
                <label>Privacy:</label>
                <select name="school_role" class="form-control">
                  <option>Shared</option>
                  <option>Public</option>
                  <option>Private</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Topic title:</label>
                  <input name="title" type="text" class="form-control" value="" required="" placeholder="Topic title...">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div id="summernote"><h3>Heading</h3><p>Please enter topic content here...</p></div>
                <script>
                  $(document).ready(function() {
                  $('#summernote').summernote();
                  });
                </script>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="row">
            <div class="col-lg-4">
              <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save topic</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.box -->
  </div>

  <div class="col-lg-12">
    <div class="box box-default box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Edit a topic</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        @if(isset($topic))
          {!! $topic !!}
        @endif
        <div class="row">
          <div class="box-body">
            <form action="/topic/editTopic", class="form-signin", method="post">
              {{csrf_field()}}
              <div class="col-lg-8">
                <div class="form-group">
                  <select name="Title" class="form-control">
                    <option>Topic1</option>
                    <option>Topi2</option>
                    <option>Topic3</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-default btn-block register-btn">Edit topic</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

<div class="modal fade" id="FlashListModal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')

<script>

  var ModalWindow = document.getElementById("FlashListModal");

  ModalWindowinnerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    $('FlashListModal').modal({show: false}) //Off by default
  }

  function GetList(){
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', ProcessList);
    requestX.open("get", "/topic/getFlashList");
    requestX.send();
    document.getElementById("GetListButton").innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  }

  function ProcessList(data){
    document.getElementById("GetListButton").innerHTML = 'Get my Flash Card List';
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      ModalWindow.innerHTML = "";
      ModalWindow.innerHTML = resp.html;
      $('#FlashListModal').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }
</script>

@endsection
