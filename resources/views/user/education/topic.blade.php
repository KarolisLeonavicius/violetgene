{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Editing topics')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

@if(isset($status))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-warning"></i>Status update</h4>
  Operation successful.
</div>
@endif

<div class="row">
  <div class="col-lg-6">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create and edit genetics topics</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="TopicWindow">
        @if(!isset($topic))
        <form action="/topic/createTopic", class="form-signin", method="post">
        @else
        <form action="/topic/editTopic/{{$topic->id}}", class="form-signin", method="post">
        @endif
          {{csrf_field()}}
          <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label>Topic category:</label>
                @if(!isset($topic))
                <select name="category" class="form-control">
                  <option>Genetics</option>
                  <option>Testing technologies</option>
                  <option>Data science</option>
                </select>
                @else
                <select name="category" class="form-control">
                  <option @php if($topic->category == "Genetics") echo "selected" @endphp>Genetics</option>
                  <option @php if($topic->category == "Testing technologies") echo "selected" @endphp>Testing technologies</option>
                  <option @php if($topic->category == "Data science") echo "selected" @endphp>Data science</option>
                </select>
                @endif
              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <label>Builds upon topic:</label>
                @if(!isset($topic))
                <select name="parent" class="form-control">
                  <option value="None">None</option>
                  @if(isset($topics))
                    @foreach($topics as $topic_item)
                      <option value="{{$topic_item->id}}">{{$topic_item->title}}</option>
                    @endforeach
                  @endif
                </select>
                @else
                <select name="parent" class="form-control">
                  <option value="None" @php if(!isset($topic->parent)) echo "selected" @endphp> None</option>
                  @if(isset($topics))
                    @foreach($topics as $topic_item)
                      <option @php if(isset($topic->parent)) if($topic->parent == $topic_item->id) echo "selected" @endphp value="{{$topic_item->id}}">{{$topic_item->title}}</option>
                    @endforeach
                  @endif
                </select>
                @endif
              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <label>Privacy:</label>
                @if(!isset($topic))
                <select name="privacy" class="form-control">
                  <option>Shared</option>
                  <option>Public</option>
                  <option>Private</option>
                </select>
                @else
                <select name="privacy" class="form-control">
                  <option @php if($topic->privacy == "Shared") echo "selected" @endphp>Shared</option>
                  <option @php if($topic->privacy == "Public") echo "selected" @endphp>Public</option>
                  <option @php if($topic->privacy == "Private") echo "selected" @endphp>Private</option>
                </select>
                @endif
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Topic title:</label>
                @if(!isset($topic))
                  <input name="title" type="text" class="form-control" value="" required="" placeholder="Topic title...">
                @else
                  <input name="title" type="text" class="form-control" value="{{$topic->title}}" required="" placeholder="Topic title...">
                @endif
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
                <h3>Topic content:</h3>
                @if(!isset($topic))
                <textarea class="input-block-level" id="summernote" name="content" rows="10"><h3>Heading</h3><p>Please enter topic content here...</p></textarea>
                @else
                <textarea class="input-block-level" id="summernote" name="content" rows="10">{{$content}}</textarea>
                @endif
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#summernote').summernote({
                      height: "400px"
                    });
                  });
                  var postForm = function() {
                    var content = $('textarea[name="content"]').html($('#summernote').code());
                  }
                </script>
              </div>
            <!-- /.box-body -->
          </div>

          <div class="row">
            <div class="col-lg-4">
              <button type="submit" class="btn btn-primary btn-block register-btn">Save topic</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>

<!-- @if(\App\Models\Role::find($user->roles()->first()->id)->name=='admin')
  @if(!$user->activated)
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. Your approval as an administrator is currently pending.
  </div>
  @else
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status update</h4>
    You can edit educational material as an administrator at: {{$school->name}}.
  </div>
  @endif
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. You are currently registered as a {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
  </div>
@endif -->

@if(isset($topics))
<div class="row">
  <div class="col-lg-6">
    <div class="box box-default box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Edit your topics</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="box-body" id="TopicWindow">
            <form action="/topic/EditTopicWindow", class="form-signin", method="get">
              {{csrf_field()}}
              <div class="col-lg-8">
                <div class="form-group">
                  <select name="id" class="form-control">
                    @foreach($topics as $topic_item)
                      <option value="{{$topic_item->id}}">{{$topic_item->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-default btn-block register-btn">Edit topic</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@endif

<div class="modal fade" id="FlashListModal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')

<script>

  var ModalWindow = document.getElementById("FlashListModal");

  ModalWindowinnerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    $('FlashListModal').modal({show: false}) //Off by default
  }

  function GetList(){
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', ProcessList);
    requestX.open("get", "/topic/getFlashList");
    requestX.send();
    document.getElementById("GetListButton").innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  }

  function ProcessList(data){
    document.getElementById("GetListButton").innerHTML = 'Get my Flash Card List';
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      ModalWindow.innerHTML = "";
      ModalWindow.innerHTML = resp.html;
      $('#FlashListModal').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }
</script>

@endsection
