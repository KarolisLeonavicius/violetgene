{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
@stop

@section('content')

<!-- @if(\App\Models\Role::find($user->roles()->first()->id)->name=='admin')
  @if(!$user->activated)
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. Your approval as an administrator is currently pending.
  </div>
  @else
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status update</h4>
    You can edit educational material as an administrator at: {{$school->name}}.
  </div>
  @endif
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    To edit educational material you need to be registered as an administrator. You are currently registered as a {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
  </div>
@endif -->

@if(isset($status))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-warning"></i>Status update</h4>
  Operation successful.
</div>
@endif

<div class="row">
  <div class="col-lg-6">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a Flash Card</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="">
          {!! Form::open(['url' => url('/topic/createFlash'), 'class' => 'form-signin', 'method' => 'post'] ) !!}
          <div class="row spacer">
            <div class="col-lg-12 col-sm-6">
              <input name='body' type="text" class="form-control" pattern=".{10,200}" required title="10 to 200 characters" placeholder="Question body, up to 200 characters...">
            </div>
          </div>
          <div class="row spacer">
            <div class="col-lg-6 col-sm-6">
              <input name='correct' type="text" class="form-control" pattern=".{1,25}" required title="1 to 25 characters" placeholder="Correct answer, up to 25 characters.">
            </div>
            <div class="col-lg-6 col-sm-6">
              <input name='incorrect' type="text" class="form-control" pattern=".{1,25}" required title="1 to 25 characters" placeholder="Incorrect answer, up to 25 characters.">
            </div>
          </div>
          <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Create a Flash Card</button>
          {!! Form::close() !!}
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>

  <div class="col-lg-6">
    <div class="box box-default box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Edit a Flash Card</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <button onclick = "GetList()" class="btn btn-md btn-info btn-block register-btn" id="GetListButton">Get my Flash Card List</button>
        @if(isset($flash_card))
          {!! $flash_card !!}
        @endif
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

<div class="modal fade" id="FlashListModal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')
<script>
  var ModalWindow = document.getElementById("FlashListModal");

  ModalWindowinnerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
  var request = new  XMLHttpRequest();
  window.onload = function () {
    $('FlashListModal').modal({show: false}) //Off by default
  }

  function GetList(){
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', ProcessList);
    requestX.open("get", "/topic/getFlashList");
    requestX.send();
    document.getElementById("GetListButton").innerHTML = 'Retrieving...';
  }

  function ProcessList(data){
    document.getElementById("GetListButton").innerHTML = 'Get my Flash Card List';
    var resp = JSON.parse(data.currentTarget.response);
    if(resp.success>0){
      ModalWindow.innerHTML = "";
      ModalWindow.innerHTML = resp.html;
      $('#FlashListModal').modal('show');
    }
    // else{
    //   AnswerWindow.innerHTML = "No answers found on our site";
    // }
  }
</script>
@endsection
