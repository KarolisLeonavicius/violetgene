@if(\App\Models\Role::find($user->roles()->first()->id)->name=='admin')
  @if(!$user->activated)
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    Your approval as an administrator is currently pending.
  </div>
  @else
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status update</h4>
    You are currently approved as a test administrator at: {{$school->name}}.
  </div>
  @endif
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are currently registered as a {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
  </div>
@endif

@if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin')&&(sizeof($user->admin()->first())))
{!! Form::open(['url' => url('/user/administrator_register'), 'class' => 'form-signin'] ) !!}
@include('includes.errors')
<div class="row">
  <div class="col-lg-6 col-sm-6">
    <h3>About you</h3>
    <div class="form-group">
      <label>Your role</label>
      <select name="school_role" class="form-control">
        <option @php if($admin->school_role=="Biology teacher") echo "selected" @endphp >Biology teacher</option>
        <option @php if($admin->school_role=="Head teacher") echo "selected" @endphp >Head teacher</option>
        <option @php if($admin->school_role=="Laboratory assistant") echo "selected" @endphp >Laboratory assistant</option>
        <option @php if($admin->school_role=="Student") echo "selected" @endphp >Student</option>
        <option @php if($admin->school_role=="Other") echo "selected" @endphp >Other</option>
      </select>
    </div>

    <div class="form-group">
      <label>I would like to</label>
      <div class="checkbox">
        <label>
          <input name="run_tests" type="checkbox" @php if($admin->run_tests) echo "checked" @endphp >
          Run genetic tests
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="design_tests" type="checkbox" @php if($admin->design_tests) echo "checked" @endphp >
          Design tests
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="edit_material" type="checkbox" @php if($admin->edit_material) echo "checked" @endphp >
          Edit educational material
        </label>
      </div>
    </div>

    <div class="form-group">
      <label>Platform use:</label>
      <div class="checkbox">
        <label>
          <input name="curricular" type="checkbox" @php if($admin->curricular) echo "checked" @endphp >
          Curricular
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="extracurricular" type="checkbox" @php if($admin->extracurricular) echo "checked" @endphp >
          Extracurricular
        </label>
      </div>
    </div>

    <label>Required training</label>
    <select name="training" class="form-control">
      <option @php if($admin->training=="No training") echo "selected" @endphp >No training</option>
      <option @php if($admin->training=="Teleconference") echo "selected" @endphp >Teleconference</option>
      <option @php if($admin->training=="Demonstration") echo "selected" @endphp >Demonstration</option>
    </select>

    <label>Phone number</label>
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
      <input name="phone" type="phone" class="form-control" value="{{$admin->phone}}" required="">
    </div>

    </div>
    <div class="col-lg-6 col-sm-6">
      @include('user.school_form')
    </div>

</div>
<button id="submit_button" class="btn btn-lg btn-primary btn-block register-btn" type="submit">Edit membership</button>
{!! Form::close() !!}
@else
{!! Form::open(['url' => url('/user/administrator_register'), 'class' => 'form-signin'] ) !!}
@include('includes.errors')
<div class="row">
  <div class="col-lg-6 col-sm-6">
    <h3>About you</h3>
    <div class="form-group">
      <label>Your role</label>
      <select name="school_role" class="form-control">
        <option>Biology teacher</option>
        <option>Head teacher</option>
        <option>Laboratory assistant</option>
        <option>Student</option>
        <option>Other</option>
      </select>
    </div>

    <div class="form-group">
      <label>I would like to</label>
      <div class="checkbox">
        <label>
          <input name="run_tests" type="checkbox">
          Run genetic tests
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="design_tests" type="checkbox">
          Design tests
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="edit_material" type="checkbox">
          Edit educational material
        </label>
      </div>
    </div>

    <div class="form-group">
      <label>Platform use:</label>
      <div class="checkbox">
        <label>
          <input name="curricular" type="checkbox">
          Curricular
        </label>
      </div>

      <div class="checkbox">
        <label>
          <input name="extracurricular" type="checkbox">
          Extracurricular
        </label>
      </div>
    </div>

    <label>Required training</label>
    <select name="training" class="form-control">
      <option>No training</option>
      <option>Teleconference</option>
      <option>Demonstration</option>
    </select>

    <label>Phone number</label>
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
      <input name="phone" type="phone" class="form-control" placeholder="Format: +44 123 1234567" required="">
    </div>

    </div>
    <div class="col-lg-6 col-sm-6">
      @include('user.school_form')
    </div>

</div>
<button id="submit_button" class="btn btn-lg btn-primary btn-block register-btn" type="submit">Start approval process</button>
<p>* If you are a student, after registering as an administrator you will loose your status.
{!! Form::close() !!}
@endif
