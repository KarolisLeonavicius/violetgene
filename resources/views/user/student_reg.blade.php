{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
@stop

@section('content')
<div class="box box-primary box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Student registration</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-lg-12 col-sm-6">
      <p class="lead">
        By registering as a student you will have access to additional educational
        content provided by your school, as well as the data obtained during your genetic
        testing practical experiments. In order to register, please provide the postcode of your
        school and either get approved by an email to your teacher or provide a code sent to you
        earlier.
      </p>
      </div>
    </div>

    <div class="content-section-a">
      <div class="row">
        <div class="col-lg-12 col-sm-6">
            @if(\App\Models\Role::find($user->roles()->first()->id)->name=='student')
              @if(!$user->activated)
              {!! Form::open(['url' => url('/user/use_code'), 'class' => 'form-signin'] ) !!}
              @include('includes.errors')
              <h3>Get approved using a code</h3>
              <input name='code' type="text" class="form-control" pattern=".{5,10}" required title="5 to 10 characters" placeholder="VXG01001010001">
              <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Approval by code</button>
              {!! Form::close() !!}
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                @if(isset($errors)){{$errors}}<br/>@endif
                Your approval as a student is currently pending.
              </div>
              @else
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i>Status update</h4>
                You are currently approved as a student at: {{$school->name}}.<br/>
                {!! Form::open(['url' => url('/user/unregister')] ) !!}
                @include('includes.errors')
                  Click here, if you would like to <button class="btn-md btn-link" type="submit">unregister</button>
                {!! Form::close() !!}
                @endif
              </div>
              {!! Form::open(['url' => url('/user/unregister'), 'class' => 'form-signin'] ) !!}
            @else
              {!! Form::open(['url' => url('/user/use_code'), 'class' => 'form-signin'] ) !!}
              @include('includes.errors')
              <h3>Get approved using code</h3>
              <input name='code' type="text" class="form-control" pattern=".{5,10}" required title="5 to 10 characters" placeholder="VXG01001010001">
              <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Approval by code</button>
              {!! Form::close() !!}
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                You are currently not approved as a student, but {{\App\Models\Role::find($user->roles()->first()->id)->name}}.
              </div>
            @endif
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>

@stop

@section('js')

@stop
