{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
  @if(isset($status))
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i>Status update</h4>
      {{$status}}
    </div>
  @endif

  <h2 class="section-heading">Import flash cards</h2>
  <p class="lead">
    To import flash cards, please upload a csv file containing
    five columns: title, question, answer1, answer2,
    number 1 or 2 indicating which answer is correct.</p>

  {!! Form::open(['url' => url('/topic/putFlash'), 'enctype' => "multipart/form-data", 'class' => 'form-signin', 'method' => 'post'] ) !!}
    @include('includes.errors')
    {!! Form::file('file_input', null, [
        'name'  => 'file_input'
    ]) !!}
    <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Upload file</button>
  {!! Form::close() !!}

  @include('includes.errors')

@stop

@section('css')

@stop

@section('js')
    <!-- <script> console.log('Loaded successfully!'); </script> -->
@stop
