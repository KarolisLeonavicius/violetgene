{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <div class="clearfix"></div> -->
    <h1>Dear {{$user->first_name}}, welcome to genetic testing</h1>
@stop

@section('content')
<br/>
<section id="introduction">
  <p class="lead">
    <b>VioletGene</b> is an eduational and genetic information sharing platform.
    It makes it possible to run simple genetic tests using mobile qPCR instruments and
    could-based data analysis. The platform is open to everyone, although designed
    specifically for teaching the practical aspects of genetics.
  </p>
</section>

<section class="content">

  @if(isset($status))
    @include('user.includes.dashboard_markers')
  @endif

    <div class="row">

      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Violetgene tests</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            @if(isset($tests))
            {!!$tests!!}
            @endif
          </div>
        </div>
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <div class="active tab-pane" id="news">
          <a class="twitter-timeline"  href="https://twitter.com/hashtag/genetics" data-widget-id="872502708267356160">#genetics Tweets</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
      </div>

      <div class="col-md-4">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Learn Genetics</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <!-- About Me Box -->
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Flash Cards of the day</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body" id="FlashWindow">
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Genetics topics</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body" id="FlashWindow">

                <div class="tab-pane" id="learning">
                  <p class="">
                    The purpose of the site is to teach how to evaluate genetic
                    data, without getting into the full depth of the genetics subject.
                    It is however necessary to understand the foundation of what
                    genes are, how they are transmitted and how they act in an
                    organism. We teach the basics using flash cards, which will become
                    more difficult as you answer them correctly, there is a section on
                    past exam materials, and finally, if you are adventurous, have a
                    look at the questions, which are asked in a real world scenario
                    on StackExchange. You can find these topics in the education section.
                    <a href="/user/education/0" class="btn btn-lg btn-primary btn-block"><b>Education Section</b></a>
                  </p>
                  <hr>
                  <div class="row">
                    <div class="col-lg-6">
                      <p class="lead">
                        <strong>Flash cards</strong> are quick questions with two option answers. They are
                        matched to your level and will become more difficult as you answer correctly.
                        <a href="/user/education" class="btn btn-lg btn-default btn-block"><b>Flash Cards</b></a>
                      </p>
                    </div>
                    <div class="col-lg-6">
                      <p class="lead">
                        <strong>Exercises</strong> shows questions related to genetic testing and
                        data interpretation which have been created by our users.
                        <a href="/user/education" class="btn btn-lg btn-default btn-block"><b>Exam questions</b></a>
                      </p>
                    </div>
                  </div>
                  <br/>
                  <p class="lead">
                    <strong>Real world</strong> questions often do not have easy answers, if they have
                    answers at all. However, they are often the most interesting and useful.
                    If you are interested in genetics beyond the curricullum, have a look at
                    discussions happening on BioStackExchange.
                    <a href="http://biology.stackexchange.com" class="btn btn-lg btn-default btn-block"><b>View Stack Exchange</b></a>
                  </p>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->


          </div>
          <!-- /.box-body -->
        </div>

      </div>
      <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
    <!-- /.content section -->

<div class="modal fade" id="FlashModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Genetics Flash Cards</h4>
      </div>
      <div class="modal-body" id=FlashWindow2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@stop

@section('css')

@stop

@section('js')
    <script>
    var TopicWindow = document.getElementById("FlashWindow");
    var TopicWindow2 = document.getElementById("FlashWindow2");
    var Ans1 = document.getElementById("Ans1");
    var Ans2 = document.getElementById("Ans2");

    TopicWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    TopicWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    var request = new  XMLHttpRequest();
    window.onload = function () {
      $('#FlashModal').modal({show: false}) //Off by default
      fetchFlash();
      // lookup_button.addEventListener('click', LookupCodes, false);
    }

    function Ans_One(){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("flash_id", document.getElementById('flash_id').innerHTML);
      formdata.append("answer", 1);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessAnswer);
      requestX.open("post", "/topic/submitFlash");
      requestX.send(formdata);
      TopicWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
      TopicWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    }

    function Ans_Two(){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("flash_id", document.getElementById('flash_id').innerHTML);
      formdata.append("answer", 2);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessAnswer);
      requestX.open("post", "/topic/submitFlash");
      requestX.send(formdata);
      TopicWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
      TopicWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    }

    function fetchFlash(){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessTopic);
      requestX.open("get", "/topic/fetchFlash");
      requestX.send();
    }

    function ProcessAnswer(data){
      // console.log(data.currentTarget.response);
      // console.log(JSON.parse(data.currentTarget.response));
      fetchFlash();
      $('#FlashModal').modal('show');
    }

    function ProcessTopic(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        TopicWindow.innerHTML = "";
        TopicWindow.innerHTML = resp.html;
        TopicWindow2.innerHTML = "";
        TopicWindow2.innerHTML = resp.html;
      }
      else{
        TopicWindow.innerHTML = "No flash cards found...";
        TopicWindow2.innerHTML = "No flash cards found...";
      }
    }

    function like(link, id){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessLink);
      requestX.open("post", link);
      if(document.getElementById("test_"+id).innerHTML != "Cheers!"){
        document.getElementById("test_"+id).innerHTML = "Cheers!";
        requestX.send(formdata);
      }
    }

    function ProcessLink(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        document.getElementById("like_count"+resp.id).innerHTML = parseInt(document.getElementById("like_count"+resp.id).innerHTML) + 1;
      }
      else{
        console.log(data.currentTarget.response);
      }
    }

    function comment(link, id){
      console.log(document.getElementById("comment_test"+id).value);
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("comment", document.getElementById("comment_test"+id).value);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessResponse);
      requestX.open("post", link);
      requestX.send(formdata);
      document.getElementById("comment_box_test"+id).innerHTML = document.getElementById("comment_box_test"+id).innerHTML + "<img class='img-circle img-sm' src='/uploads/avatars/{{$user->avatar}}' alt='User Image'><div class='comment-text'><span class='username'>{{$user->first_name}} {{$user->last_name}} <span class='text-muted pull-right'>just now</span></span>"+document.getElementById("comment_test"+id).value+"</div>";
    }

    function ProcessResponse(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        document.getElementById("comment_count"+resp.id).innerHTML = parseInt(document.getElementById("comment_count"+resp.id).innerHTML) + 1;
      }
      else{
        console.log(data.currentTarget.response);
      }
    }
    </script>
@stop
