{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
@stop

@section('content')

<div class="box box-primary box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Register as test administrator</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <h2></h2>
    <div class="row">
      <div class="col-lg-12 col-sm-6">
      <p class="lead">
        In order to run genetic tests, we will need additional information for
        the approval process and your address to which the instrument will be
        shipped and from which it will be picked up.
      </p>
      </div>
    </div>
    <div class="content-section-a">
      @include('user.admin_form')
    </div>
  </div>
  <!-- /.box-body -->
</div>

<div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you at a registered school?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <input type="hidden" id="csrfToken" value="{{csrf_token()}}">
            <button id="lookup_button" class="btn btn-md btn-info" type="button">Search</button>
          </div>
          <div class="col-md-10">
            <input id="postcode" type="text" class="form-control" placeholder="Postcode", required="">
          </div>
        </div>
        <ul id="school_list">
        </ul>
        School not yet registered? <a href="#" data-dismiss="modal" onclick="ShowForm()">click here</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@stop

@section('js')
    <script>
      $('#yourModal').modal({ show: false})

      @if(is_null($school))
        document.getElementById("main_form").style.display = 'none';
        document.getElementById("show_dialog").style.display = 'block';
      @else
        document.getElementById("show_dialog").style.display = 'none';
      @endif

      var lookup_button = document.getElementById("lookup_button");
      var show_button = document.getElementById("show_dialog");

      var request = new  XMLHttpRequest();
      window.onload = function () {
        lookup_button.addEventListener('click', LookupCodes, false);
        show_button.addEventListener('click', ShowWindow, false);
      }

      function make_form_data(){
        var formdata = new FormData();
        formdata.append("_token", document.getElementById('csrfToken').value);
        formdata.append("postcode", document.getElementById('postcode').value);
        return formdata;
      }

      function LookupCodes(evt) {
        // evt.preventDefault();
        document.getElementById("school_list").innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
        var requestX = new  XMLHttpRequest();
        requestX.addEventListener('load', Complete);
          requestX.open("post", "/user/postcode_lookup");
          requestX.send(make_form_data());
      }

      function ShowWindow(){
        $('#yourModal').modal('show');
      }

      function ShowForm(){
        document.getElementById("show_dialog").style.display = 'none';
        document.getElementById("main_form").style.display = 'block';
      }

      function Complete(data){
        var resp = JSON.parse(data.currentTarget.response);
        console.log(resp.schools.length);
        document.getElementById("school_list").innerHTML = "";
        if(resp.schools.length>0)
          for (var i = 0; i < resp.schools.length; i++) {
            var node = document.createElement("LI");
            var createA = document.createElement('a');
            var createAText = document.createTextNode(resp.schools[i]);
            var createText = document.createTextNode(', postcode: '+ resp.postcodes[i]);
            createA.setAttribute('href', "/user/assign_school/"+ resp.ids[i]);
            createA.appendChild(createAText);
            node.appendChild(createA);
            node.appendChild(createText);
            document.getElementById("school_list").appendChild(node);
          }
        else{
          var node = document.createElement("LI");
          var createText = document.createTextNode("No schools found.");
          node.appendChild(createText);
          document.getElementById("school_list").appendChild(node);
        }
      }

    </script>
@stop
