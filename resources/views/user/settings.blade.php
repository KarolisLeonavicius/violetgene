{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
@stop

@section('content')
<div class="row">
  <div class="col col-md-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Basic account settings</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col col-lg-6 col-sm-12">
          <p class="lead">
            Please update your profile information, which will be used for representing
            you online during your interactions with other platform users. Please upload a profile
            picture and a short description, since other users will be more likely to share test
            information with you. If you are a student at a school, please register below for
            accessing additional content relating to genetics education. If you would like to
            administer genetic tests yourself, please start the approval process at the bottom of the page.
          </p>

            <div class="content-section-a">
              <div class="row">
                <div class="col-lg-12">
                  {!! Form::open(['url' => url('/user/update_avatar'), 'enctype' => "multipart/form-data", 'class' => 'form-signin', 'method' => 'post'] ) !!}
                    @include('includes.errors')

                    <img class="profile-user-img img-responsive img-circle" alt="User profile picture" src="data:image/png;base64,{{$imdata}}">
                    {!! Form::file('avatar', null, [
                        'name'  => 'avatar'
                    ]) !!}
                    <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Update avatar</button>
                  {!! Form::close() !!}

                  {!! Form::open(['url' => url('/user/update_profile'), 'class' => 'form-signin']) !!}
                  @include('includes.errors')

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <strong><i class="fa fa-graduation-cap margin-r-5"></i>Flash card score</strong>
                      <p class="text-muted">
                        {{$user->flash_score}}
                      </p>
                      <div id="chart_div"></div>
                      <?php echo Lava::render('LineChart', 'FlashChart', 'chart_div');?>
                    </li>
                  </ul>

                   @if(\App\Models\Role::find($user->roles()->first()->id)->name=='user')
                     <hr>
                     <strong><i class="fa fa-pencil margin-r-5"></i> Edit membership</strong>
                     <a href="{{config('app.admin_registration')}}" class="btn btn-primary btn-block"><b>Register as administrator</b></a>
                     <a href="{{config('app.student_registration')}}" class="btn btn-primary btn-block"><b>Register as a student</b></a>
                   @endif

                  <div class="row">
                    <div class="col-lg-12">
                      <label for="inputLastName">Email address</label>
                      <input name="email" type="email" class="form-control", required="", value="{{$user->email}}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <label for="inputLastName">First name</label>
                      <input name="first_name" type="text" class="form-control", required="", value="{{$user->first_name}}">
                    </div>
                    <div class="col-lg-6">
                      <label for="inputLastName">Last name</label>
                      <input name="last_name" type="text" class="form-control", required="", value="{{$user->last_name}}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <label for="inputLastName">City</label>
                      <input name="city" type="text" class="form-control", required="", value="{{$user->city}}">
                    </div>
                    <div class="col-lg-6">
                      <label for="inputLastName">Country</label>
                      <input name="country" type="text" class="form-control", required="", value="{{$user->country}}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <label for="inputDescription" class="">Description</label>
                      <textarea name="description" class="form-control" rows="3" required="">{{$user->description}}</textarea>
                    </div>
                  </div>
                  <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Update profile</button>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

          <div class="col col-md-6 col-sm-12">
            <div class="" id="timeline">
              @if(isset($timeline))
                {!!$timeline!!}
              @endif
            </div>
          </div>

          </div>

      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@stop
