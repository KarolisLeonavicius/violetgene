<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="Answers">Answer for: {{$topic->title}}</h4>
    </div>
    <div class="modal-body">
      <p class="lead">

        @foreach($answers as $answer)
          <div class="row">
            <div class="col-lg-12">
                {!!$answer['body']!!}
            </div>
          </div>
          <hr>
          <br/>
        @endforeach

        <div class="row">
          <div class="col-lg-8">
            <a href="{{$topic->link}}" class="btn btn-lg btn-success btn-block">View and discuss on Stack Exchange</a>
          </div>
        </div>
      </p>
    </div>
    <div class="modal-footer">
      <p>Powered by
        <img src="/img/BioStackLogo.png" style="height:20px">
        <a href="http://biology.stackexchange.com">BioStackExchange</a>
      </p>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
