<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      @if(isset($topic))
        <h4 class="modal-title" id="myModalLabel">{{$topic->category}}</h4>
      @endif
    </div>
    <div class="modal-body">
      <table class="table">
        <thead>
          <tr>
            <th>
              Builds upon:
            </th>
            <th>
              Current topic:
            </th>
            <th>
              Next topics:
            </th>
          </tr>
        </thead>
        <tr>
          <td>
            @if(isset($parent))
              <a href="#" onclick="fetchTopicTree({{$parent->id}})" class="btn btn-default btn-toolbar" aria-expanded="false">
                {{$parent->title}}
              </a>
            @endif
          </td>
          <td>
            @if(isset($topic))
              <a href="#" onclick="fetchTopicTree({{$topic->id}})" class="btn btn-primary btn-toolbar" aria-expanded="false">
                {{$topic->title}}
              </a>
            @endif
          </td>
          <td>
            @if(isset($children))
              @foreach($children as $child)
              <a href="#" onclick="fetchTopicTree({{$child->id}})" class="btn btn-block btn-info btn-toolbar" aria-expanded="false">
                {{$child->title}}
              </a>
              <br/>
              @endforeach
            @endif
          </td>
        </tr>
      </table>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
