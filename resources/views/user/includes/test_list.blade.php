<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Panels</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Type</strong></p>
        </td>
        <td>
          <p class=""><strong>Name</strong></p>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
      </tr>
      @foreach($tests as $test)
      <tr>
        <div style="Display:none" id="id_{{$test->id}}">{{$test->id}}</div>
        <td>
          <p class="">{{$test->name}}</p>
        </td>
        <td>
          <p class="">{{$test->comment}}</p>
        </td>
        <td>
          <button class="btn btn-sm btn-success btn-block register-btn" onclick="select_test({{$test->id}})">Select</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
