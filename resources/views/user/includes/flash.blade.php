  <div class="row">
    <div class="col-lg-12">
        <p class="lead">
          {!!$flash->body!!}
        </p>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-lg-12">
      <div class="btn-group btn-block">
        <a href="#" onclick="Ans_One()" class="btn margin btn-md btn-info btn-block btn-toolbar" aria-expanded="false">
          <span>{!!$flash->answer1!!}</span>
        </a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="btn-group btn-block">
        <a href="#" onclick="Ans_Two()" class="btn margin btn-md btn-info btn-block btn-toolbar" aria-expanded="false">
          <span>{!!$flash->answer2!!}</span>
        </a>
      </div>
    </div>
  </div>
  <div style="visibility: hidden" type="hidden" id="flash_id">{{$flash->id}}</div>
