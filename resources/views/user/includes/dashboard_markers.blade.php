<div class="row">
  <div class="col-md-4">
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{$status->testCount}}</h3>
        <p>My genetic tests</p>
      </div>
      <div class="icon">
        <i class="fa fa-bar-chart"></i>
      </div>
      <a href="/test/my_tests/0" class="small-box-footer">Run and view my tests<i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-md-4">
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{$status->experiments}}</h3>
          <p>Genes targets to test for</p>
        </div>
        <div class="icon">
          <i class="fa fa-flask"></i>
        </div>
        <a href="/order/start" class="small-box-footer">Order a new genetic test<i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
  </div>

  <div class="col-md-4">
    <div class="small-box bg-warning">
      <div class="inner">
        <h3>{{$status->exercises}}</h3>
        <p>Genetics exercises</p>
      </div>
      <div class="icon">
        <i class="fa fa-graduation-cap"></i>
      </div>
      <a href="#" class="small-box-footer">Solve an exercise <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
</div>
