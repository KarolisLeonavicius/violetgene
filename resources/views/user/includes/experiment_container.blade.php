<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <div class="user-block">
          @php
            $tmp = $experiment->user()->first();
          @endphp
          <img class="img-circle" src="/uploads/avatars/{{$tmp->avatar}}" alt="User Image">
          <span class="username"><a href="#"><strong>{{$experiment->name}}</strong>
            </a>
          </span>
          <span class="description">Created by:
            <a href="#">
            @php
              echo $tmp->first_name[0];
              echo ".";
              echo $tmp->last_name[0];
              echo ".";
            @endphp
            </a>
            : {{$experiment->created_at->diffForHumans()}}
          </span>

        </div>
        <h4>DNA extraction method:</h4>
        <p>Cooked food</p>
        <div class="">
          <a href="#"><span class="pull-left badge bg-green">7 Steps</span></a>
          <a href="#"><span class="pull-left badge bg-yellow">under 60min</span></a>
          <a href="#"><span class="pull-left badge bg-green">3 solutions</span></a>
          <a href="#"><span class="pull-left badge bg-red">Accuracy: low</span></a>
        </div>
        <br/>
        <h4>Gene panel:</h4>
        <p>Educational sandwich test</p>
        <div class="">
          <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
          <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
          <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
          <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
          <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
          <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
        </div>

        <!-- /.user-block -->
        <div class="box-body">
        {{$experiment->comments}}
        <hr>
        <div class="row">
          <div class="col-md-6">
            <button type="button" onclick="select_experiment('{{$experiment->id}}')" class="btn btn-success btn-md">
              Select template
            </button>
          </div>
          <div class="col-md-6">
            <button type="button" onclick="select_experiment('0')" class="btn btn-default btn-md">
              Close window
            </button>
          </div>
        </div>
        <hr>

        <!-- /.box-footer -->
        <div class="box-footer">
          <form action="/comment/post/experiment/{{$experiment->id}}" method="post">
            {{csrf_field()}}
            <img class="img-responsive img-circle img-sm" src="/uploads/avatars/{{$user->avatar}}" alt="Avatar">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <input type="text" name="comment" class="form-control input-sm" required placeholder="Press enter to post comment">
            </div>
          </form>
        </div>

        <hr/>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
