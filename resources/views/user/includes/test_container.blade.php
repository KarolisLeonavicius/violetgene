<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">

    <div class="user-block">
      @php
        $tmp = $test->user()->first();
      @endphp
      <img class="img-circle" src="/uploads/avatars/{{$tmp->avatar}}" alt="User Image">
      <span class="username"><a href="#"><strong>{{$test->title}}</strong>
        </a>
      </span>
      <span class="description">Created by:
        <a href="#">
        @php
          echo $tmp->first_name[0];
          echo ".";
          echo $tmp->last_name[0];
          echo ".";
        @endphp
        </a>
        : {{$test->created_at->diffForHumans()}}
      </span>

    </div>
    <a href="/test/fullscreen/{{$test->id}}" target="_blank" class="btn-link">
      View test details
    </a>

    <div class="info-box">
      @if(isset($test->imdata))
        <span class="info-box-icon bg-white"><img style="margin:-11px 0px 0px 0px" src="data:image/png;base64,{{$test->imdata}}"></i></span>
      @endif
      <div class="info-box-content" id="panel_entry">
          @if(isset($panelHTML))
            {!!$panelHTML!!}
          @endif
      </div>
      <!-- /.info-box-content -->
    </div>

    <!-- Insert content -->

    <!-- /.user-block -->
    <div class="box-body">
    <button id="test_{{$test->id}}" onclick="like('/comment/like/test/{{$test->id}}', '{{$test->id}}')" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
    @if(!empty($comments)&&!empty($likes))
        <span class="pull-right text-muted"><span id="like_count{{$test->id}}">{{count($likes)}}</span> likes - <span id="comment_count{{$test->id}}">{{count($comments)}}</span> comments</span>
    @endif
      <!-- /.box-body -->
    </div>

    <div class="box-footer box-comments" id="comment_box_test{{$test->id}}">
      @if(!empty($comments))
        @if(count($comments)>0)
        <p class="lead">
          Comments:
        </p>
          @foreach($comments as $comment)
          <div class="box-comment">
            <!-- User image -->
            @php
              $comment_user = $comment->user()->first();
              echo "<img class='img-circle img-sm' src='/uploads/avatars/".$comment_user->avatar."' alt='User Image'>";
              echo "<div class='comment-text'>";
              echo "<span class='username'>";
                      echo $comment_user->first_name;
                      echo " ";
                      echo $comment_user->last_name;
                    @endphp
                    <span class="text-muted pull-right">{{$comment->created_at->diffForHumans()}}</span>

                  </span><!-- /.username -->
                {{$comment->comment}}
            </div>
            <!-- /.comment-text -->
          </div>
          @endforeach
        @endif
      @endif

      <!-- /.box-comment -->
    </div>
    <!-- /.box-footer -->
    <div class="box-footer">
        <img class="img-responsive img-circle img-sm" src="/uploads/avatars/{{$user->avatar}}" alt="Avatar">
        <!-- .img-push is used to add margin to elements next to floating images -->
        <div class="img-push">
          <input type="text" name="" class="form-control input-sm" required placeholder="Press enter to post comment" id="comment_test{{$test->id}}" onclick="this.select()" onKeyDown="if(event.keyCode==13) comment('/comment/post/test/{{$test->id}}', '{{$test->id}}');">
        </div>
    </div>

    <hr/>
  </div>
  <!-- /.box-body -->
</div>
