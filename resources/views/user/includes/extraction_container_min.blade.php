
  <p class="lead"><b>DNA extraction method:</b></p>
  <a class="btn-link" href="/test/ExtractionProtocol/{{$extraction->id}}" target="_blank">View protocol</a>
  <input type="hidden" name="extraction_id" value="{{$extraction->id}}">
  <div class="">
    <a href="#"><span class="pull-left badge bg-grey">{{$extraction->type}}</span></a>
    <a href="#"><span class="pull-left badge bg-green">{{$extraction->steps}} Steps</span></a>
    <a href="#"><span class="pull-left badge bg-yellow">{{$extraction->duration}}</span></a>
    <a href="#"><span class="pull-left badge bg-green">{{$extraction->privacy}}</span></a>
    @if($extraction->approved)
      <a href="#"><span class="pull-left badge bg-green">Approved</span></a>
    @else
      <a href="#"><span class="pull-left badge bg-red">Not approved</span></a>
    @endif
  </div>
  <br/>
