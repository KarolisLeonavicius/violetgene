<form action="/topic/editFlash/{{$flash_card->id}}", class="form-signin", method="post">
  {{csrf_field()}}
  <div class="row spacer">
    <div class="col-lg-12 col-sm-6">
      <input name='body' type="text" class="form-control" pattern=".{10,200}" value = "{{$flash_card->body}}" required title="10 to 200 characters" placeholder="Question body, up to 200 characters...">
    </div>
  </div>
  <div class="row spacer">
    <div class="col-lg-6 col-sm-6">
      <input name='correct' type="text" class="form-control" pattern=".{1,25}" value = "{{$flash_card->correctAns}}" required title="1 to 25 characters" placeholder="Correct answer, up to 25 characters.">
    </div>
    <div class="col-lg-6 col-sm-6">
      <input name='incorrect' type="text" class="form-control" pattern=".{1,25}" value = "{{$flash_card->incorrectAns}}" required title="1 to 25 characters" placeholder="Incorrect answer, up to 25 characters.">
    </div>
  </div>
  <button class="btn btn-lg btn-primary btn-block register-btn" type="submit">Save Flash Card</button>
</form>
