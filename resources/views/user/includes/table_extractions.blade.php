<h3 class="modal-title">DNA extraction methods</h3>
Cannot find what you a looking for?

<div class="row">
  <div class="col-lg-6">
    <a href="/test/design_extraction/0" target="_blank" class="btn btn-md btn-primary btn-block register-btn">Design a new DNA extraction</a>
  </div>
  <div class="col-lg-6">
    <div class="input-group input-group-md">
      <input type="text" class="form-control" placeholder="Submit a suggestion">
        <span class="input-group-btn">
          <button type="button" class="btn btn-primary btn-flat">Submit</button>
        </span>
    </div>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-lg-12">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Type</strong></p>
      </td>
      <td>
        <p class=""><strong>Duration</strong></p>
      </td>
      <td>
        <p class=""><strong>Number of steps</strong></p>
      </td>
      <td>
        <p class=""><strong>Privacy</strong></p>
      </td>
      <td>
        <p class=""><strong>Created</strong></p>
      </td>
      <td>
        <p class=""><strong>View protocol</strong></p>
      </td>
    </tr>
    @if(isset($extractions))
      @foreach($extractions as $extraction)
      <tr>
        <td>
          <p class="">{{$extraction->type}}</p>
        </td>
        <td>
          <p class="">{{$extraction->duration}}</p>
        </td>
        <td>
          <p class="">{{$extraction->steps}}</p>
        </td>
        <td>
          <p class="">{{$extraction->privacy}}</p>
        </td>
        <td>
          <p class="">{{$extraction->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <a href="/test/ExtractionProtocol/{{$extraction->id}}" target="_blank" class="btn btn-sm btn-primary btn-block register-btn">View</a>
        </td>
        <td>

        </td>
      </tr>
      @endforeach
    @endif
    </table>
  </div>
</div>
