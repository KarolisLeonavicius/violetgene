  @foreach($panel->genes() as $gene)
    @if(($gene->type!="Negative Control")&&($gene->type!="Positive Control"))
      <a href="#"><span class="pull-left badge bg-purple">{{$gene->name}}</span></a>
    @endif
  @endforeach
