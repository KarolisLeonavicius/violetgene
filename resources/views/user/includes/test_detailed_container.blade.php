<div class="user-block">
  @php
    $tmp = $test->user()->first();
  @endphp
  <img class="img-circle" src="/uploads/avatars/{{$tmp->avatar}}" alt="User Image">
  <span class="username"><a href="#"><strong>{{$test->title}}</strong>
    </a>
  </span>
  <span class="description">Created by:
    <a href="#">
    @php
      echo $tmp->first_name[0];
      echo ".";
      echo $tmp->last_name[0];
      echo ".";
    @endphp
    </a>
    : {{$test->created_at->diffForHumans()}}
  </span>

</div>
<button type="button" id="fullscreenTopicButtonA" onclick="fullscreenTopic({{$test->id}})" class="btn-link">
  View test details
</button>

<div class="info-box">
  <!-- Insert content -->

  <span class="info-box-icon bg-white"><img style="margin:-11px 0px 0px 0px" src="/uploads/avatars/default.png"></i></span>

  <div class="info-box-content">
    <span class="info-box-text"><a href="#"> Gene panel: </a></span>
    <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
    <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
    <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
    <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
    <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
    <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
  </div>
  <!-- /.info-box-content -->
</div>

<!-- /.user-block -->
<div class="box-body">
{{$test->comments}}
<hr>
  <!-- /.box-body -->
</div>
