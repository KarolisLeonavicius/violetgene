@if(isset($results))
<table class="table">
  <thead>
    <tr>
        <th>Gene</th>
        <th>Ct</th>
        <th>DNA copies</th>
        <th>Data quality</th>
    </tr>
  </thead>
  @if(count($results)>1)
    @foreach($results as $result)
    <tr>
      <td>{{$result->name}}</td>
      <td>{{$result->Ct}}</td>
      @if($result->quality>50)
        @if($result->copyNr > 1000000)
          <td>{{$result->copyNr/1000000}}mln.</td>
        @else
          <td>{{$result->copyNr}}</td>
        @endif
      @else
        <td> --- </td>
      @endif
      <td>
        @if($result->quality<=30)
          <div class="progress progress-md active">
            <div class="progress-bar
            progress-bar-danger
            progress-bar-striped"
            role="progressbar"
            aria-valuenow="{{$result->quality}}"
            aria-valuemin="0"
            aria-valuemax="100"
            style="width: {{$result->quality}}%">
              <span class="sr-only">{{$result->quality}}</span>
              <span>{{$result->quality}}%</span>
            </div>
          </div>
        @elseif($result->quality>70)
          <div class="progress progress-md active">
            <div class="progress-bar
            progress-bar-success
            progress-bar-striped"
            role="progressbar"
            aria-valuenow="{{$result->quality}}"
            aria-valuemin="0"
            aria-valuemax="100"
            style="width: {{$result->quality}}%">
              <span class="sr-only">{{$result->quality}}</span>
              <span>{{$result->quality}}%</span>
            </div>
          </div>
        @else
          <div class="progress progress-md active">
            <div class="progress-bar
            progress-bar-warning
            progress-bar-striped"
            role="progressbar"
            aria-valuenow="{{$result->quality}}"
            aria-valuemin="0"
            aria-valuemax="100"
            style="width: {{$result->quality}}%">
              <span class="sr-only">{{$result->quality}}</span>
              <span>{{$result->quality}}%</span>
            </div>
          </div>
        @endif
      </td>
    </tr>
    @endforeach
  @endif
</table>
@endif
