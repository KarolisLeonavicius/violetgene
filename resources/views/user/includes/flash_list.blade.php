<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">My Flash Cards</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class="lead"><strong>Question</strong></p>
        </td>
        <td>
          <p class="lead"><strong>Incorrect</strong></p>
        </td>
        <td>
          <p class="lead"><strong>Correct</strong></p>
        </td>
        <td>
          <p class="lead"><strong>Count</strong></p>
        </td>
        <td>
          <p class="lead"><strong>Edit</strong></p>
        </td>
        <td>
          <p class="lead"><strong>Delete</strong></p>
        </td>
      </tr>
      @foreach($flash_cards as $flash)
      <tr>
        <td>
          {{$flash->body}}
        </td>
        <td>
          {{$flash->answer1}}
        </td>
        <td>
          {{$flash->answer2}}
        </td>
        <td>
          {{$flash->answer_count}}
        </td>
        <td>
          <form action="/topic/getFlash/{{$flash->id}}", class="form-signin", method="post">
            {{csrf_field()}}
            <button class="btn btn-sm btn-warning btn-block register-btn" type="submit">Edit</button>
          </form>
        </td>
        <td>
          <form action="/topic/deleteFlash/{{$flash->id}}", class="form-signin", method="post">
            {{csrf_field()}}
            <button class="btn btn-sm btn-danger btn-block register-btn" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
