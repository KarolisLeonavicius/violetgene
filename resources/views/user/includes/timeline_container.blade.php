<ul class="timeline timeline-inverse">

  <!-- timeline time label -->
  <li class="time-label">
        <span class="bg-purple">
          {{Carbon\Carbon::now()->toFormattedDateString()}}
        </span>
  </li>
  <!-- /.timeline-label -->

  @foreach($events as $event)

    @if($event->event_type == "test")
    <!-- timeline item -->
      <li>
        <i class="fa fa-flask bg-cyan"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$event->created_at->diffForHumans()}} </span>

          <h3 class="timeline-header"><a href="#">New genetic test</a></h3>

          <div class="timeline-body">
            {{$event->content}}
          </div>
          <!-- <div class="timeline-footer">
            <a class="btn btn-primary btn-xs">Read more</a>
            <a class="btn btn-danger btn-xs">Delete</a>
          </div> -->
        </div>
      </li>
    <!-- END timeline item -->
    @endif

    @if($event->event_type == "panel")
    <!-- timeline item -->
      <li>
        <i class="fa fa-bar-chart bg-blue"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$event->created_at->diffForHumans()}} </span>

          <h3 class="timeline-header"><a href="#">New gene panel</a></h3>

          <div class="timeline-body">
            {{$event->content}}
          </div>
          <!-- <div class="timeline-footer">
            <a class="btn btn-primary btn-xs">Read more</a>
            <a class="btn btn-danger btn-xs">Delete</a>
          </div> -->
        </div>
      </li>
    <!-- END timeline item -->
    @endif

    @if($event->event_type == "topic")
    <!-- timeline item -->
      <li>
        <i class="fa fa-graduation-cap bg-blue"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$event->created_at->diffForHumans()}} </span>

          <h3 class="timeline-header"><a href="#">New genetics topic</a></h3>

          <div class="timeline-body">
            {{$event->content}}
          </div>
          <!-- <div class="timeline-footer">
            <a class="btn btn-primary btn-xs">Read more</a>
            <a class="btn btn-danger btn-xs">Delete</a>
          </div> -->
        </div>
      </li>
    <!-- END timeline item -->
    @endif

    @if($event->event_type == "register")
    <!-- timeline time label -->
    <li class="time-label">
          <span class="bg-purple">
            {{$event->created_at->toFormattedDateString()}}
          </span>
    </li>
    <!-- timeline item -->
      <li>
        <i class="fa fa-user bg-green"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$event->created_at->diffForHumans()}} </span>

          <h3 class="timeline-header no-border">{{$event->content}}</h3>
        </div>
      </li>
    <!-- END timeline item -->
    @endif

    @if($event->event_type == "start")
    <!-- timeline time label -->
    <li class="time-label">
          <span class="bg-purple">
            {{$event->created_at->toFormattedDateString()}}
          </span>
    </li>
    <!-- timeline item -->
      <li>
        <i class="fa fa-user bg-green"></i>

        <div class="timeline-item">
          <span class="time"><i class="fa fa-clock-o"></i> {{$event->created_at->diffForHumans()}} </span>

          <h3 class="timeline-header no-border">{{$event->content}}</h3>
          <div class="timeline-body">
            <a href="settings/basic">View settings</a>
          </div>
        </div>
      </li>
    <!-- END timeline item -->

    <!-- /.timeline-label -->
    @endif
    <!-- END timeline item -->
  @endforeach
  <li>
    <i class="fa fa-clock-o bg-gray"></i>
  </li>
</ul>
