<h3 class="modal-title">Experimental templates</h3>

<div class="row">
  <div class="col-lg-6">
    <a href="/test/new/custom" target="_blank" class="btn btn-md btn-primary btn-block register-btn">Design a new test</a>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-lg-12">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Template name</strong></p>
      </td>
      <td>
        <p class=""><strong>View gene panel</strong></p>
      </td>
      <td>
        <p class=""><strong>View DNA extraction</strong></p>
      </td>
      <td>
        <p class=""><strong>Privacy</strong></p>
      </td>
      <td>
        <p class=""><strong>Created</strong></p>
      </td>
    </tr>
    @if(isset($templates))
      @foreach($templates as $template)
      <tr>
        <td>
          <p class="">{{$template->name}}</p>
        </td>
        <td>
          <p class=""><a href="/test/PanelDescription/{{$template->gene_panel_id}}" target="_blank" class="btn-link btn-sm btn-default">View</a></p>
        </td>
        <td>
          <p class=""><a href="/test/ExtractionProtocol/{{$template->extraction_id}}" target="_blank" class="btn-link btn-sm btn-default">View</a></p>
        </td>
        <td>
          <p class="">{{$template->privacy}}</p>
        </td>
        <td>
          <p class="">{{$template->created_at->diffForHumans()}}</p>
        </td>
        <td>

        </td>
      </tr>
      @endforeach
    @endif
    </table>
  </div>
</div>
