
<!-- /.box-header -->
<div class="box-body">
  <h4>DNA extraction method:</h4>
  <p>Cooked food</p>
  <div class="">
    <a href="#"><span class="pull-left badge bg-green">7 Steps</span></a>
    <a href="#"><span class="pull-left badge bg-yellow">under 60min</span></a>
    <a href="#"><span class="pull-left badge bg-green">3 solutions</span></a>
    <a href="#"><span class="pull-left badge bg-red">Accuracy: low</span></a>
  </div>
  <br/>
  <hr/>
  <h4>Gene panel:</h4>
  <p>Educational sandwich test</p>
  <div class="">
    <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
    <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
    <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
    <a href="#"><span class="pull-left badge bg-yellow">Pork</span></a>
    <a href="#"><span class="pull-left badge bg-green">Chicken</span></a>
    <a href="#"><span class="pull-left badge bg-red">Beef</span></a>
  </div>
</div>
