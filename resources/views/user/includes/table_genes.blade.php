<h3 class="modal-title">Gene targets</h3>
Cannot find what you a looking for?

<div class="row">
  <div class="col-lg-6">
    <a href="/test/design_target/0" target="_blank" class="btn btn-sm btn-primary btn-block register-btn">Design a new gene target</a>
  </div>

  <div class="col-lg-6">
    <div class="input-group input-group-md">
      <input type="text" class="form-control" placeholder="Submit a suggestion">
        <span class="input-group-btn">
          <button type="button" class="btn btn-primary btn-flat">Submit</button>
        </span>
    </div>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-lg-12">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Type</strong></p>
      </td>
      <td>
        <p class=""><strong>Name</strong></p>
      </td>
      <td>
        <p class=""><strong>Gene ID</strong></p>
      </td>
      <td>
        <p class=""><strong>Validated</strong></p>
      </td>
      <td>
        <p class=""><strong>Privacy</strong></p>
      </td>
      <td>
        <p class=""><strong>Created</strong></p>
      </td>
    </tr>
    @if(isset($genes))
      @foreach($genes as $gene)
      <tr>
        <td>
          <p class="">{{$gene->type}}</p>
        </td>
        <td>
          <p class="">{{$gene->name}}</p>
        </td>
        <td>
          <p class="">
            @if(!is_null($gene->gene_id))
              <a href="https://www.ncbi.nlm.nih.gov/nuccore/{{$gene->gene_id}}" target="_blank" class="btn btn-link btn-block">Database</a>
            @endif
          </p>
        </td>
        <td>
          <p class="">
            @if($gene->approved)
              Yes
            @else
              Untested
            @endif
          </p>
        </td>
        <td>
          <p class="">{{$gene->privacy}}</p>
        </td>
        <td>
          <p class="">{{$gene->created_at->diffForHumans()}}</p>
        </td>
        <td>

        </td>
      </tr>
      @endforeach
    @endif
    </table>
  </div>
</div>
