<p class="lead">
  <strong>{{$topic->title}}</strong>
  <div style="visibility: hidden" id="question_id_div">{{$topic->question_id}}</div>
  <div class="row">
    <div class="col-lg-12">
        {!!$topic->body!!}
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-lg-4">
      <div class="btn-group btn-block">
        <a href="{{$topic->link}}" class="btn btn-lg btn-info btn-block btn-toolbar" aria-expanded="false">View on Stack Exchange</a>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="btn-group btn-block">
        <a href="#" onclick="getAnswers()" class="btn btn-lg btn-success btn-block btn-toolbar" aria-expanded="false">Answer</a>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="btn-group btn-block">
        <a href="#" onclick="fetchTopic()" class="btn btn-lg btn-primary btn-block btn-toolbar" aria-expanded="false">Next question</a>
      </div>
    </div>
  </div>
</p>

<p>Powered by
  <img src="/img/BioStackLogo.png" style="height:20px">
  <a href="http://biology.stackexchange.com">BioStackExchange</a>
</p>
