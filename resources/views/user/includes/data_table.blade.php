@if(isset($data))
<table class="table">
  <thead>
    <tr>
      @if(isset($titles))
        @foreach($titles as $title)
        <th>{{$title}}</th>
        @endforeach
      @endif
    </tr>
  </thead>
  @if(count($data)>1)
    @foreach($data as $row)
    <tr>
      @foreach($row as $value)
      <td>{{$value}}</td>
      @endforeach
    </tr>
    @endforeach
  @endif
</table>
@endif
