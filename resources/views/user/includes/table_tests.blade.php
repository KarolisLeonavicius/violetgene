<h3 class="modal-title">Genetic tests</h3>
<div class="row">
  <div class="col-lg-6">
    <a href="/test/new/standard" target="_blank" class="btn btn-md btn-primary register-btn">Run a new test</a>
  </div>
</div>
<br/>
Recent genetic tests

<hr/>

<div class="row">
  <div class="col-lg-12">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Test title</strong></p>
      </td>
      <td>
        <p class=""><strong>View gene panel</strong></p>
      </td>
      <td>
        <p class=""><strong>View DNA extraction</strong></p>
      </td>
      <td>
        <p class=""><strong>Privacy</strong></p>
      </td>
      <td>
        <p class=""><strong>Created</strong></p>
      </td>
      <td>
        <p class=""><strong>Updated</strong></p>
      </td>
      <td>
        <p class=""><strong>Quick View</strong></p>
      </td>
    </tr>
    @if(isset($tests))
      @foreach($tests as $test)
      <tr>
        <td>
          <p class="">{{$test->title}}</p>
        </td>
        <td>
          <p class=""><a href="/test/PanelDescription/{{$test->panel_id}}" target="_blank" class="btn btn-link btn-block btn-default">Panel</a></p>
        </td>
        <td>
          <p class=""><a href="/test/ExtractionProtocol/{{$test->extraction_id}}" target="_blank" class="btn btn-link btn-block btn-default">Extraction</a></p>
        </td>
        <td>
          <p class="">{{$test->privacy}}</p>
        </td>
        <td>
          <p class="">{{$test->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <p class="">{{$test->updated_at->diffForHumans()}}</p>
        </td>
        <td>
          <p class=""><a href="/test/fullscreen/{{$test->id}}" class="btn btn-block btn-success">View</a</p>
        </td>
      </tr>
      @endforeach
    @endif
    </table>
  </div>
</div>
