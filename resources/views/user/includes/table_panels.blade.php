<h3 class="modal-title">Gene panels</h3>
Cannot find what you a looking for?

<div class="row">
  <div class="col-lg-6">
    <a href="/test/design_panel/0" target="_blank" class="btn btn-md btn-primary btn-block register-btn">Design a new gene panel</a>
  </div>
  <div class="col-lg-6">
    <div class="input-group input-group-md">
      <input type="text" class="form-control" placeholder="Submit a suggestion">
        <span class="input-group-btn">
          <button type="button" class="btn btn-primary btn-flat">Submit</button>
        </span>
    </div>
  </div>
</div>

<hr/>

<div class="row">
  <div class="col-lg-12">
    <table class="table">
    <tr>
      <td>
        <p class=""><strong>Type</strong></p>
      </td>
      <td>
        <p class=""><strong>Name</strong></p>
      </td>
      <td>
        <p class=""><strong>Pos/Neg controls</strong></p>
      </td>
      <td>
        <p class=""><strong>Panel size</strong></p>
      </td>
      <td>
        <p class=""><strong>Privacy</strong></p>
      </td>
      <td>
        <p class=""><strong>View panel</strong></p>
      </td>
    </tr>
    @if(isset($panels))
      @foreach($panels as $panel)
      <tr>
        <td>
          <p class="">{{$panel->type}}</p>
        </td>
        <td>
          <p class="">{{$panel->name}}</p>
        </td>
        <td>
          <p class="">{{$panel->pos_controls}} / {{$panel->neg_controls}}</p>
        </td>
        <td>
          <p class="">{{$panel->reactions}} reactions</p>
        </td>
        <td>
          <p class="">{{$panel->privacy}}</p>
        </td>
        <td>
          <a href="/test/PanelDescription/{{$panel->id}}"class="btn btn-sm btn-primary btn-block register-btn">View</a>
        </td>
        <td>

        </td>
      </tr>
      @endforeach
    @endif
    </table>
  </div>
</div>
