<div class="col-lg-6">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active" ><a href="#current" data-toggle="tab">Current</a></li>
      <li><a href="#genetics" data-toggle="tab">Genetics foundation</a></li>
      <li><a href="#testing" data-toggle="tab">Testing technologies</a></li>
      <li><a href="#statistics" data-toggle="tab">Data science</a></li>
    </ul>
    <div class="tab-content">
      <div class="active tab-pane" id="current">
        <!-- Topic -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Current topic</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="GeneticsTopic">
                  @if(isset($FirstTopic))
                    {!!$FirstTopic->htmlContent!!}
                  @else
                    No topic currently loaded.
                  @endif
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.Topic -->
      </div>
      <!-- /.tab-pane -->

      <div class="tab-pane" id="genetics">
        <!-- TopicList -->
        @if(isset($FoundationTopics))
          @foreach($FoundationTopics as $topic)
            @if($topic->category == "Genetics")
              <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    {{$topic->title}}
                  </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  {!!$topic->htmlContent!!}
                </div>
                <!-- /.box-body -->
              </div>
            @endif
          @endforeach
        @endif
        <!-- /.Topic -->
      </div>
      <!-- /.tab-pane -->

      <div class="tab-pane" id="testing">
        <!-- TopicList -->
        @if(isset($FoundationTopics))
          @foreach($FoundationTopics as $topic)
            @if($topic->category == "Testing technologies")
              <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    {{$topic->title}}
                  </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  {!!$topic->htmlContent!!}
                </div>
                <!-- /.box-body -->
              </div>
            @endif
          @endforeach
        @endif
        <!-- /.Topic -->
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="statistics">
        <!-- TopicList -->
        @if(isset($FoundationTopics))
          @foreach($FoundationTopics as $topic)
            @if($topic->category == "Data science")
              <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    {{$topic->title}}
                  </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  {!!$topic->htmlContent!!}
                </div>
                <!-- /.box-body -->
              </div>
            @endif
          @endforeach
        @endif
        <!-- /.Topic -->
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
</div>
<!-- /.col-lg6 -->
