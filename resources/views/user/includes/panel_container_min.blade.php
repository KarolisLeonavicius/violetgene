
  <p class="lead"><b>Gene panel:</b></p>
  {{$panel->name}} (
  <a class="btn-link" href="/test/PanelDescription/{{$panel->id}}" target="_blank">view panel</a>
  ).
  <br/>
  <a href="#"><span class="pull-left badge bg-green">{{$panel->privacy}}</span></a>
  @if($panel->approved)
    <a href="#"><span class="pull-left badge bg-green">Approved</span></a>
  @else
    <a href="#"><span class="pull-left badge bg-red">Not approved</span></a>
  @endif
  <br/>
  <input type="hidden" name="panel_id" value="{{$panel->id}}">
  @foreach($panel->genes() as $gene)
    @if(($gene->type!="Negative Control")&&($gene->type!="Positive Control"))
      <a href="#"><span class="pull-left badge bg-grey">{{$gene->name}}</span></a>
    @endif
  @endforeach
