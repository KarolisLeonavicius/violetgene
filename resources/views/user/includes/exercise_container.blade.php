<div class="user-block">
  <img class="img-circle" src="/uploads/avatars/{{$user->avatar}}" alt="User Image">
  <span class="username"><a href="#"><strong>Created by:</strong>
      @php
        $tmp = $FirstExercise->user()->first();
        echo $tmp->first_name;
        echo " ";
        echo $tmp->last_name;
      @endphp
    </a>
  </span>
  <span class="description">Privacy: {{$FirstExercise->privacy}} - {{$FirstExercise->created_at->diffForHumans()}}</span>
  <button type="button" id="fullscreenExerciseButtonA" onclick="fullscreenExercise({{$FirstExercise->id}})" class="btn margin btn-sm btn-default">
    <i class="fa fa-desktop"></i>
    Fullscreen
  </button>
  <button type="button" onclick="fetchGeneticsTopic({{$FirstExercise->topic_id}})" class="btn margin btn-sm btn-default">
    <i class="fa fa-plus"></i>
    Get the topic
  </button>
</div>
  <!-- /.user-block -->
<div class="box-body">
{!!$content!!}

@if(!empty($children))
  @if(count($children)>0)
  <p class="lead">
    Next Exercises:
  </p>
    @foreach($children as $child)
      <button type="button" onclick="fetchExercise({{$child->id}})" class="btn btn-sm btn-success">
        <i class="fa fa-plus"></i>
        {{$child->title}}
      </button>
    @endforeach
  @endif
@endif

<hr/>
<button id="exercise_{{$FirstExercise->id}}" onclick="like('/comment/like/exercise/{{$FirstExercise->id}}', 'exercise_{{$FirstExercise->id}}')" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>

@if(!empty($comments)&&!empty($likes))
    <span class="pull-right text-muted">{{count($likes)}} likes - {{count($comments)}} comments</span>
@endif
  <!-- /.box-body -->
</div>

<div class="box-footer box-comments">
  @if(!empty($comments))
    @if(count($comments)>0)
    <p class="lead">
      Topic comments:
    </p>
      @foreach($comments as $comment)
      <div class="box-comment">
        <!-- User image -->
        @php
          $comment_user = $comment->user()->first();
          echo "<img class='img-circle img-sm' src='/uploads/avatars/".$comment_user->avatar."' alt='User Image'>";
          echo "<div class='comment-text'>";
          echo "<span class='username'>";
                  echo $comment_user->first_name;
                  echo " ";
                  echo $comment_user->last_name;
                @endphp
                <span class="text-muted pull-right">{{$comment->created_at->diffForHumans()}}</span>

              </span><!-- /.username -->
            {{$comment->comment}}
        </div>
        <!-- /.comment-text -->
      </div>
      @endforeach
    @endif
  @endif

  <!-- /.box-comment -->
</div>
<!-- /.box-footer -->
<div class="box-footer">
  <form action="/comment/post/exercise/{{$FirstExercise->id}}" method="post">
    {{csrf_field()}}
    <img class="img-responsive img-circle img-sm" src="/uploads/avatars/{{$user->avatar}}" alt="Avatar">
    <!-- .img-push is used to add margin to elements next to floating images -->
    <div class="img-push">
      <input type="text" name="comment" class="form-control input-sm" required placeholder="Press enter to post comment">
    </div>
  </form>
</div>
