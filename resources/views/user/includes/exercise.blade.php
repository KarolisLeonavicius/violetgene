<div class="row">
  <div class="col-lg-12">
    <p class="lead">
    @if(isset($exercise))
        {{$exercise->title}}
    @endif
    </p>
  </div>
</div>
<hr>
<br/>

<div class="row">
  <div class="col-lg-12">
    @if(isset($content)
      {{$content}}
    @endif
  </div>
</div>
<hr>
<br/>
