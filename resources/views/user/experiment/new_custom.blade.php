{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Custom test')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">Run a genetic test</h3>
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a custom test</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
        <div class="box-body">
          <form action="/test/createRun" class="form-signin" enctype ="multipart/form-data" method="post" id="experiment_data">
            {{csrf_field()}}
            <input type="hidden" name="id_form" value="{{md5(time())}}">

            <div class="row">
              <div class="col-lg-4">
                <div class="form-group" id="extraction_entry">
                  <button
                    onclick="getExtractionList()"
                    id="get_extractions"
                    class="btn btn-lg btn-info btn-block register-btn"
                    type="button">Select DNA extraction
                  </button>
                  </div>
                  <div class="form-group" id="panel_entry">
                    <button
                      onclick="getPanelList()"
                      id="get_panels"
                      class="btn btn-lg btn-info btn-block register-btn"
                      type="button">Select a gene panel
                    </button>
                    </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Test title:</label>
                    <input name="title" type="text" class="form-control" value="" required="" placeholder="Topic title...">
                </div>

                <label>Attach a picture for your experiment</label>
                <img class="image" src="">
                {!! Form::file('avatar', null, [
                    'name'  => 'avatar'
                ]) !!}
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label>Privacy:</label>
                  <select name="privacy" class="form-control">
                    <option>Shared</option>
                    <option>Public</option>
                    <option>Private</option>
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-lg-12">
                <textarea class="input-block-level" id="summernote" name="notes" rows="10"><p>No comment...</p></textarea>
                  <script>
                    $(document).ready(function() {
                      $('#summernote').summernote({
                        height: 150,                 // set editor height
                        minHeight: null,             // set minimum height of editor
                        maxHeight: null,
                      });
                    });
                    var postForm = function() {
                      var content = $('textarea[name="notes"]').html($('#summernote').code());
                    }
                  </script>
                </input>
              </div>
              <!-- /.box-body -->
            </div>

            <div class="row">
              <div class="col-lg-4">
                <button type="button" onclick="save_test()" id="save_tests" class="btn btn-lg btn-default btn-block register-btn">Save experiment</button>
              </div>
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save and run</button>
              </div>
            </div>
          </form>
          <div id="message">
            @if(isset($message))
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
              <h4><i class='icon fa fa-warning'></i>Status</h4>
              {{$message}}
            </div>
            @endif
          </div>
          <div id="message">
            @if(isset($message))
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
              <h4><i class='icon fa fa-warning'></i>Status</h4>
              {{$message}}
            </div>
            @endif
          </div>
          @if($approved)
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Status</h4>
              You are currently approved as a genetic test administrator at: {{$school->name}}.
            </div>
          @else
            <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Alert!</h4>
              You are not currently approved as a test administrator.
            </div>
          @endif
        </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">
</div>
@stop

@section('js')
<script>
var ModalWindow = document.getElementById("Modal");
var ExtractionWindow = document.getElementById("extraction_entry");
var PanelWindow = document.getElementById("panel_entry");

var extraction_selected = false;
var panel_selected = false;

window.onload = function () {
}

$( document ).ajaxComplete(function() {
  $( ".log" ).text( "Triggered ajaxComplete handler." );
});


function getExtractionList(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getExtractionSelect");
  requestX.send();
  document.getElementById("get_extractions").innerHTML = 'Loading...';
}

function select_extraction(id){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessExtraction);
  requestX.open("get", "/test/getExtractionMin/"+id);
  requestX.send();
  document.getElementById("get_extractions").innerHTML = "Loading...";
  $('#Modal').modal('hide');
}


function ProcessList(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "Not found...";
  }
  document.getElementById("get_extractions").innerHTML = "Select DNA extraction";
}

function ProcessExtraction(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    ExtractionWindow.innerHTML = "";
    ExtractionWindow.innerHTML = resp.html;
    extraction_selected = true;
    $('#Modal').modal('hide');
  }
}

function getPanelList(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessPanelList);
  requestX.open("get", "/test/getPanelListSelect");
  requestX.send();
  document.getElementById("get_panels").innerHTML = 'Loading...';
}

function select_panel(id){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessPanel);
  requestX.open("get", "/test/getPanelMin/"+id);
  requestX.send();
  document.getElementById("get_panels").innerHTML = "Loading...";
  $('#Modal').modal('hide');
}


function ProcessPanelList(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "Not found...";
  }
  document.getElementById("get_panels").innerHTML = "Select a gene panel";
}

function ProcessPanel(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    PanelWindow.innerHTML = "";
    PanelWindow.innerHTML = resp.html;
    panel_selected = true;
    $('#Modal').modal('hide');
  }
}

function save_test(){
  if(extraction_selected && panel_selected){
    var fd = new FormData(experiment_data);
    var requestX = new  XMLHttpRequest();
    // fd.append('notes', document.getElementById('summernote').innerHTML);
    requestX.addEventListener('load', SaveResponse);
    requestX.open("post", "/test/saveTest");
    requestX.send(fd);
    document.getElementById("save_tests").innerHTML = "Saving...";
  }
  else{
    alert("Please select a DNA extration kit and Gene panel");
  }
}

function SaveResponse(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  document.getElementById("message").innerHTML ="";
  if(resp.success>0){
    document.getElementById("message").innerHTML = "<div class='alert alert-success alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>Successfully saved</div>"
  }
  else{
    document.getElementById("message").innerHTML = "<div class='alert alert-warning alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>"+resp.message+"</div>"
  }
  document.getElementById("save_tests").innerHTML = "Save experiment";
}
</script>
@stop
