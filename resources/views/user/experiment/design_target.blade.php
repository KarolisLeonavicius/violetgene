{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Gene Target')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">Designing gene targets (amplicons)</h3>
<!-- @if($approved)
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-success"></i>Status</h4>
    You are approved as a genetic test administrator at: {{$school->name}}.
  </div>
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are not currently approved as a test administrator.
  </div>
@endif -->
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a gene target</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="FormWindow">

        @if(isset($target))
          {!! $target !!}
        @else
          <form action="/test/createTarget", class="form-signin", method="post", enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
              <div class="col-lg-2">
                <div class="form-group">
                  <label>Display name:*</label>
                  <input type="text" class="form-control" name="name" placeholder="3 to 15 characters" required pattern=".{3,15}" title="Three to fifteen characters">
                </div>
              </div>

              <div class="col-lg-2">
                <div class="form-group">
                  <label>Target type:</label>
                  <select name="type" class="form-control">
                    <option>Species identification</option>
                    <option>Genotyping</option>
                    <option>Positive Control</option>
                    <option>Negative Control</option>
                  </select>
                </div>
              </div>

              <div class="col-lg-2">
                <div class="form-group">
                  <label>Reference:</label>
                  <input type="text" class="form-control" placeholder="E.g.: Link to a publication..." name="reference" value="">
                </div>
              </div>

              <div class="col-lg-2">
                <div class="form-group">
                  <label>Genbank accession id:</label>
                  <input type="text" class="form-control" placeholder="E.g.: KX550271.1" name="gene_id" value="">
                </div>
              </div>

              <div class="col-lg-2">
                <div class="form-group">
                  <label>Privacy:</label>
                  <select name="privacy" class="form-control">
                    <option>Public</option>
                    <option>Shared</option>
                    <option>Private</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-5">
                <div class="form-group">
                  <label>Forward primer (5'-3'):*</label>
                  <input class="form-control" name="forward_primer" id="for_prim" placeholder="ATCCG..., 10 to 60 bases" pattern="[ATCGN]{10,60}" title="Ten to sixty DNA bases (A, T, C, G)" title="Ten to sixty DNA bases">
                </div>
              </div>

              <div class="col-lg-5">
                <div class="form-group">
                  <label>Backward primer (5'-3'):*</label>
                  <input class="form-control" name="backward_primer" id="back_prim" placeholder="TTGCG..., 10 to 60 bases" required pattern="[ATCGN]{10,60}" title="Ten to sixty DNA bases (A, T, C, G)" title="Ten to sixty DNA bases">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-10">
                <div class="input-group margin">
                  <input type="hidden" value="" name="derived_from" id="derived">
                  <input type="text" class="form-control" placeholder="Target mixture..." value="" id="derived_view">
                      <span class="input-group-btn">
                        <button type="button" onclick="get_genes()" class="btn btn-info btn-flat" id="list_button">Add gene targets</button>
                      </span>
                </div>
                </div>
              </div>

            <div class="row">
              <div class="col-lg-4">
                <button type="submit" class="btn margin btn-lg btn-primary btn-block register-btn">Save gene target</button>
              </div>
              <div class="col-lg-4">
                <button type="button" onclick="get_genes2()" class="btn margin btn-lg btn-default btn-block" id="list_button2">Edit my gene targets</button>
              </div>
            </div>
          </form>
        @endif

        <hr class="section-heading-spacer">
        <div class="clearfix"></div>

        <p class="lead">
          <strong>A gene target</strong> (or an amplicon) is a pair of primers, which are designed to detect a gene of interest,
          by aligning to its specific sequence. If the gene of interest is present in the sample, the pair of primers will specifically
          replicate a DNA fragment, which is the basis of qPCR.<br/><br/>
          <strong>Designing</strong> a gene target usually involves searching the published literature for
          verified forward and backward primer sequences. BLAST alignment tool can then be used to
          check primer alignemt against different genes and species, which may be present in your sample.<br/><br/>
          <strong>Primer mixtures</strong> contain a mixture of primers for either positive controls or testing the presence of
          several amplicons at once. Care should be taken to avoid primer cross-reactivity.<br/><br/>
          <strong>Gene target types</strong> differ by:
          <ul>
            <li><strong>Species identification</strong> - the primers are designed for identifying a species in the presence of others.</li>
            <li><strong>Genotyping</strong> - primers are optimized for identifying genetic variants of a particular species.</li>
            <li><strong>Negative control</strong> - primers are optimized to give no signal, when no DNA is present.</li>
            <li><strong>Positive control</strong> - targets are optimized to give a non-specific positive signal, when DNA is present.</li>
          </ul>
        </p>

      </div>
    </div>
    <!-- /.box -->
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')
<script>

var Button = document.getElementById("list_button");
var ModalWindow = document.getElementById("Modal");
var additional_list = [];

var request = new  XMLHttpRequest();
window.onload = function () {
  $('#Modal').modal({show: false}) //Off by default
  // lookup_button.addEventListener('click', LookupCodes, false);
}

function get_genes(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getTargetList");
  requestX.send();
  Button.innerHTML = 'Retrieving gene targets...';
}

function get_genes2(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getTargetListEdit");
  requestX.send();
  document.getElementById("list_button2").innerHTML = 'Retrieving gene targets...';
}

function select(id){
  additional_list.push(id);
  document.getElementById("for_prim").value = "NNNNNNNNNN";
  document.getElementById("back_prim").value = "NNNNNNNNNN";
  document.getElementById("derived_view").value = additional_list.join();
  document.getElementById("derived").value = JSON.stringify(additional_list);
}

function ProcessList(data){
  // console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    // gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "No genes found...";
  }
  Button.innerHTML = "Add gene targets";
  document.getElementById("list_button2").innerHTML = 'Edit my gene targets';
}

</script>
@stop
