{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'New test')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">Run a genetic test</h3>
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a new standard test</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
        <div class="box-body">

          <form action="/test/createRun" class="form-signin" enctype ="multipart/form-data" method="post" id="experiment_data">
            {{csrf_field()}}
            <input type="hidden" name="id_form" value="{{md5(time())}}">

            <div class="row">
              <div class="col-lg-4">
                <div class="form-group" id="experiment_type">
                  <label>Select test type:</label>
                  <button
                    onclick="get_experiments()"
                    id="get_tests"
                    class="btn btn-lg btn-info btn-block register-btn"
                    type="button">Select template from database
                  </button>
                  <a href="/test/new/custom"
                    class="btn btn-lg btn-info btn-block register-btn"
                    type="button">Create a custom test
                  </a>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Test title:</label>
                    <input name="title" type="text" class="form-control" value="" required="" placeholder="Name your test...">
                </div>

                <label>Attach a picture for your experiment</label>
                <img class="image" src="">
                {!! Form::file('avatar', null, [
                    'name'  => 'avatar'
                ]) !!}
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <label>Privacy:</label>
                  <select name="privacy" class="form-control">
                    <option>Shared</option>
                    <option>Public</option>
                    <option>Private</option>
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-lg-12">
                <textarea class="input-block-level" id="summernote" name="notes" rows="10"><p>No comment...</p></textarea>
                  <script>
                    $(document).ready(function() {
                      $('#summernote').summernote({
                        height: 150,                 // set editor height
                        minHeight: null,             // set minimum height of editor
                        maxHeight: null,
                      });
                    });
                    var postForm = function() {
                      var content = $('textarea[name="notes"]').html($('#summernote').code());
                    }
                  </script>
              </div>
              <!-- /.box-body -->
            </div>

            <div class="row">
              <div class="col-lg-4">
                <button type="button" onclick="save_test()" id="save_tests" class="btn btn-lg btn-primary btn-block register-btn">Save experiment</button>
              </div>
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save and run</button>
              </div>
            </div>
          </form>
          <div id="message">
            @if(isset($message))
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
              <h4><i class='icon fa fa-warning'></i>Status</h4>
              {{$message}}
            </div>
            @endif
          </div>
          @if($approved)
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Status</h4>
              You are currently approved as a genetic test administrator at: {{$school->name}}.
            </div>
          @else
            <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Alert!</h4>
              You are not currently approved as a test administrator.
            </div>
          @endif
        </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')
<script>
var ModalWindow = document.getElementById("Modal");
var ExperimentWindow = document.getElementById("experiment_type");

window.onload = function () {
}


function get_experiments(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getExperimentList");
  requestX.send();
  document.getElementById("get_tests").innerHTML = 'Loading...';
}

function select_experiment(id){
  instrument_id = id;
  if(id!=0){
    var requestX = new  XMLHttpRequest();
    requestX.addEventListener('load', ProcessExperiment);
    requestX.open("get", "/test/getExperimentMin/"+id);
    requestX.send();
    document.getElementById("get_tests").innerHTML = "Loading...";
  }
  $('#Modal').modal('hide');
}


function ProcessList(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "No tests found...";
  }
  document.getElementById("get_tests").innerHTML = "Select from Database";
}

function ProcessExperiment(data){
  var resp = JSON.parse(data.currentTarget.response);
  console.log(resp.hrml);
  if(resp.success>0){
    ExperimentWindow.innerHTML = "";
    ExperimentWindow.innerHTML = resp.html;
  }
}

function save_test(){
  var fd = new FormData(experiment_data);
  var requestX = new  XMLHttpRequest();
  // fd.append('notes', document.getElementById('summernote').innerHTML);
  requestX.addEventListener('load', SaveResponse);
  requestX.open("post", "/test/saveTest");
  requestX.send(fd);
  document.getElementById("save_tests").innerHTML = "Saving...";
}

function SaveResponse(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("message").innerHTML = "<div class='alert alert-success alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>Successfully saved</div>"
  }
  else{
    document.getElementById("message").innerHTML = "<div class='alert alert-warning alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>"+resp.message+"</div>"
  }
  document.getElementById("save_tests").innerHTML = "Update test details";
}
</script>
<script>
function like(link, id){
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessLink);
  requestX.open("post", link);
  if(document.getElementById("experiment_"+id).innerHTML != "Cheers!"){
    document.getElementById("experiment_"+id).innerHTML = "Cheers!";
    requestX.send(formdata);
  }
}

function ProcessLink(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("like_count"+resp.id).innerHTML = parseInt(document.getElementById("like_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}

function comment(link, id){
  console.log(document.getElementById("comment_test"+id).value);
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  formdata.append("comment", document.getElementById("comment_test"+id).value);
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessResponse);
  requestX.open("post", link);
  requestX.send(formdata);
  document.getElementById("comment_box_test"+id).innerHTML = document.getElementById("comment_box_test"+id).innerHTML + "<div class='comment-text'><span class='username'>Me: <span class='text-muted pull-right'>just now</span></span>"+document.getElementById("comment_test"+id).value+"</div>";
}

function ProcessResponse(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("comment_count"+resp.id).innerHTML = parseInt(document.getElementById("comment_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}
</script>
@stop
