{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">Designing DNA extraction methods</h3>
<!-- @if($approved)
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-success"></i>Status</h4>
    You are approved as a genetic test administrator at: {{$school->name}}.
  </div>
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are not currently approved as a test administrator.
  </div>
@endif -->

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a DNA extraction method</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="FormWindow">
        <button onclick = "ShowExtractions()" class="btn btn-md btn-info btn-block register-btn" id="GetListButton">Derive from existing methods</button>
        <hr class="section-heading-spacer">
        <div class="clearfix"></div>
        @if(isset($method))
          {!! $method !!}
        @else
          <form action="/test/createExtraction", class="form-signin", method="post", enctype="multipart/form-data" onsubmit="return postForm()">
            {{csrf_field()}}
            <input type="hidden" value="0" name="derived_from">
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                  <label>Sample type:</label>
                  <select name="type" class="form-control">
                    <option>Cooked food</option>
                    <option>Uncooked food</option>
                    <option>Liquids</option>
                    <option>Micro samples</option>
                  </select>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="form-group">
                  <label>Number of steps:</label>
                  <select name="steps" class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                  </select>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="form-group">
                  <label>Time, minutes:</label>
                  <select name="duration" class="form-control">
                    <option> under 10 mins</option>
                    <option> under 30 mins</option>
                    <option> under 60 mins</option>
                    <option> over 60 mins</option>
                  </select>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="form-group">
                  <label>Privacy:</label>
                  <select name="privacy" class="form-control">
                    <option>Shared</option>
                    <option>Public</option>
                    <option>Private</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12">
                <h3>Protocol:</h3>
                <textarea class="input-block-level" id="summernote" name="protocol" rows="10"><h3>Heading</h3><p>Please enter topic content here...</p></textarea>
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#summernote').summernote({
                      height: "400px"
                    });
                  });
                  var postForm = function() {
                    var content = $('textarea[name="protocol"]').html($('#summernote').code());
                  }
                </script>
              </div>
              <!-- /.box-body -->
            </div>

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label>Additional requirements:</label>
                  <input class="form-control" type="text" name="additional" placeholder="E.g.: boiling water, timer..." value="">
                </div>
              </div>

              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save extraction method</button>
              </div>
            </div>
          </form>
        @endif
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>
@stop

@section('js')
<script>

var Button = document.getElementById("GetListButton");
var ModalWindow = document.getElementById("Modal");

var request = new  XMLHttpRequest();
window.onload = function () {
  $('#Modal').modal({show: false}) //Off by default
  // lookup_button.addEventListener('click', LookupCodes, false);
}

function ShowExtractions(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getExtractionList");
  requestX.send();
  Button.innerHTML = 'Retrieving list...';
}

function select_extraction(id){
  window.location = "/test/design_extraction/"+id;
}


function add(id){
  value = parseInt(document.getElementById("quantity_"+id).innerHTML)+1;
  if(value<25)
  document.getElementById("quantity_"+id).innerHTML=value.toString();
}

function subtract(id){
  value = parseInt(document.getElementById("quantity_"+id).innerHTML)-1;
  if(value>0)
    document.getElementById("quantity_"+id).innerHTML=value.toString();
}

function ProcessList(data){
  // console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "No methods found...";
  }
  Button.innerHTML = "Derive from existing methods";
}

</script>
@stop
