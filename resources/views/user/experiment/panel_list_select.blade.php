<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Panels</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Name</strong></p>
        </td>
        <td>
          <p class=""><strong>Type</strong></p>
        </td>
        <td>
          <p class=""><strong>Reaction #</strong></p>
        </td>
        <td>
          <p class=""><strong>Approved</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
      </tr>
      @foreach($panels as $panel)
      <tr>
        <div style="Display:none" id="id_{{$panel->id}}">{{$panel->id}}</div>
        <td>
          <p class="">{{$panel->name}}</p>
        </td>
        <td>
          <p class="">{{$panel->type}}</p>
        </td>
        <td>
          <p class="">{{$panel->reactions}}</p>
        </td>
        <td>
          @if(!is_null($panel->approved))
            <p class="">Yes</p>
          @else
            <p class="">No</p>
          @endif
        </td>

        <td>
          <p class="">{{$panel->created_at->diffForHumans()}}</p>
        </td>

        <td>
          <button href="" onclick="select_panel('{{$panel->id}}')" class="btn btn-sm btn-primary btn-block register-btn">Select</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
