<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Gene targets</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Type</strong></p>
        </td>
        <td>
          <p class=""><strong>Name</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Approved</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Add</strong></p>
        </td>
      </tr>
      @foreach($targets as $target)
      <tr>
        <td>
          <p class="">{{$target->type}}</p>
        </td>
        <td>
          <p class="">{{$target->name}}</p>
        </td>
        <td>
          @if(!is_null($target->gene_id))
            <a href="https://www.ncbi.nlm.nih.gov/nuccore/{{$target->gene_id}}"target="_blank">View</a>
          @endif
        </td>

        <td>
          @if(!is_null($target->approved))
            @if($target->approved)
              <p class="">Yes</p>
            @else
              <p class="">No</p>
            @endif
          @endif
        </td>

        <td>
          <p class="">{{$target->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <button class="btn btn-sm btn-warning btn-block register-btn" onclick="select({{$target->id}})" id="select_{{$target->id}}">Select</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
