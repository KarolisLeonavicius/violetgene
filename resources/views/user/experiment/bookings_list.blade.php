<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Panels</h4>
    </div>
    <div class="modal-body">
      <h4>My bookings:</h4>
      @if(isset($bookings))
      <table class="table">
      <tr>
        <td> <p class=""><strong>Instrument</strong></p> </td>
        <td> <p class=""><strong>Booking from</strong></p> </td>
        <td> <p class=""><strong>Booking to</strong></p> </td>
        <td> <p class=""><strong>Attach instrument</strong></p> </td>
      </tr>
      @foreach($bookings as $booking)
      @if((Carbon\Carbon::createFromTimestamp($booking->from)->diffInMinutes(null, false)>0)&&(Carbon\Carbon::createFromTimestamp($booking->to)->diffInMinutes(null, false)<0))
        <tr>
          <td> <p class="">{{$booking->instrument()->first()->code}}</p> </td>
          <td> <p class="">{{Carbon\Carbon::createFromTimestamp($booking->from)->diffForHumans()}}</p> </td>
          <td> <p class="">{{Carbon\Carbon::createFromTimestamp($booking->to)->diffForHumans()}}</p> </td>
          <td>
            <button type="submit" onclick="connect_instrument({{$booking->instrument_id}})" class="btn btn-sm btn-primary btn-block register-btn">Select</button>
          </td>
        </tr>
        @endif
      @endforeach
      </table>
      @else
        <p class="lead">You have to book an instrument to run a test</p>
      @endif
      <hr/>
      <table class="table">
      <tr>
        <td> <p class="">Simulation</p> </td>
        <td> <p class="">N/A</p> </td>
        <td> <p class="">N/A</p> </td>
        <td>
            <button type="submit" onclick="connect_instrument(0)" class="btn btn-sm btn-primary btn-block register-btn">Select</button>
        </td>
      </tr>
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
