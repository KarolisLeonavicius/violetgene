@extends('layouts.master')

@section('title', 'Gene panel')

@section('content')
<div class="content-section-a">
  <div class="container">
    <h2>Gene panel: {{$panel->name}}</h2>
    <p>Panel id: {{$panel->id}}</p>
    <p>Panel type: {{$panel->type}}</p>
    <h3>Gene list:</h3>
      {!!$gene_list!!}
    <h3>Description:</h3>
    <p class="lead">
      {!!$panel->content!!}
    </p>
  </div>
</div>

@endsection

@section('js')

@endsection
