@extends('layouts.master')

@section('title', 'View test')

@section('content')
<div class="content-section-a">
  <div class="container">
    <h2>Genetic test record</h2>
    <br/>
    <br/>
    @if(isset($test))
      <h3>{{$test->title}}</h3>
      <p class="lead">Created at: {{$test->created_at}}, updated: {{$test->updated_at->diffForHumans()}}</p>
      <p class="lead">Created at: {{$test_user->city}}/{{$test_user->country}}, by: {{$test_user->first_name}} {{$test_user->last_name[0]}}.</p>
      <p class="lead">Privacy: {{$test->privacy}}</p>

      @if(isset($test->imdata))
        <p class="lead">Image:</p>
        <img class="img-responsive" alt="Test picture" src="data:image/png;base64,{{$test->imdata}}">
      @endif
      <hr>
      @if(isset($test->comments))
      {!!$test->comments!!}
      @endif
    @endif
  </div>
</div>


<div class="content-section-b">
  <div class="container">
    @if(isset($panelHTML))
      {!!$panelHTML!!}
    @endif
  </div>
</div>

<div class="content-section-a">
  <div class="container">
    @if(isset($extrHTML))
      {!!$extrHTML!!}
    @endif
  </div>
</div>

<div class="content-section-b">
  <div class="container">
    <h2>Test results</h2>
    @if(isset($resultHTML))
      {!!$resultHTML!!}
    @endif
  </div>
</div>

<div class="content-section-b">
  <div class="container">
    <h2>Comments:</h2>
    <button id="test_{{$test->id}}" onclick="like('/comment/like/test/{{$test->id}}', '{{$test->id}}')" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
    @if(!empty($comments)&&!empty($likes))
        <span class="pull-right text-muted"><span id="like_count{{$test->id}}">{{count($likes)}}</span> likes - <span id="comment_count{{$test->id}}">{{count($comments)}}</span> comments</span>
    @endif
    <hr>
    <div class="box-footer box-comments" id="comment_box_test{{$test->id}}">
      @if(!empty($comments))
        @if(count($comments)>0)
          @foreach($comments as $comment)
          <div class="box-comment">
            <!-- User image -->
            @php
              $comment_user = $comment->user()->first();
              echo "<div class='comment-text'>";
              echo "<span class='username'>";
                      echo $comment_user->first_name;
                      echo " ";
                      echo $comment_user->last_name;
                    @endphp
                    <span class="text-muted pull-right">{{$comment->created_at->diffForHumans()}}</span>
                  </span><!-- /.username -->
                <span class="text-muted">{{$comment->comment}}</span>
            </div>
            <!-- /.comment-text -->
          </div>
          @endforeach
        @endif
      @endif
      <!-- /.box-comment -->
    </div>
    <div class="img-push">
      <input type="text" name="" class="form-control input-sm" required placeholder="Press enter to post comment" id="comment_test{{$test->id}}" onclick="this.select()" onKeyDown="if(event.keyCode==13) comment('/comment/post/test/{{$test->id}}', '{{$test->id}}');">
    </div>
  </div>
</div>


@endsection

@section('js')
<script>
function like(link, id){
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessLink);
  requestX.open("post", link);
  if(document.getElementById("test_"+id).innerHTML != "Cheers!"){
    document.getElementById("test_"+id).innerHTML = "Cheers!";
    requestX.send(formdata);
  }
}

function ProcessLink(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("like_count"+resp.id).innerHTML = parseInt(document.getElementById("like_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}

function comment(link, id){
  console.log(document.getElementById("comment_test"+id).value);
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  formdata.append("comment", document.getElementById("comment_test"+id).value);
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessResponse);
  requestX.open("post", link);
  requestX.send(formdata);
  document.getElementById("comment_box_test"+id).innerHTML = document.getElementById("comment_box_test"+id).innerHTML + "<div class='comment-text'><span class='username'>Me: <span class='text-muted pull-right'>just now</span></span>"+document.getElementById("comment_test"+id).value+"</div>";
}

function ProcessResponse(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("comment_count"+resp.id).innerHTML = parseInt(document.getElementById("comment_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}
</script>
@endsection
