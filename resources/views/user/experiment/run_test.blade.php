{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Running a test')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
@stop

@section('content')

<h3 class="">Run a genetic test</h3>
<div class="row margin">
  <div class="col-lg-12">
    <a href="{!!env('SOCKET_SERVER')!!}/test/runTest/{{$test->id}}" class="btn btn-default">Reload this page in HTTP</a>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Instrument control</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-lg-12">
            <button id="get_bookings" type="button" class="btn btn-success" onclick="get_bookings()">Attach instrument</button>
          </div>
        </div>
        <hr/>
        @if(isset($instrument))
          @if($instrument->type != "Simulation")
            @include('user.instrument.chai_panel')
          @else
          <div class="row">
            <div class="col-lg-12">

                <div class="col-sm-2">
                  <button class="btn btn-app" onclick='start_run({{$test->id}})'>
                    <i class="fa fa-play"></i> Start run
                    <span class="badge bg-green" id="status">Online</span>
                  </button>
                </div>

                <div class="col-sm-2">
                  <button class="btn btn-app" onclick='stop_run({{$test->id}})'>
                    <i class="fa fa-square"></i> Stop
                  </button>
                </div>

                <div class="col-sm-2">
                  <p class="lead">
                    <strong>Instrument:</strong> <br/>Simulator
                  </p>
                </div>

                <div class="col-sm-2">
                  <p class="lead">
                    <strong>Temperature:</strong> <br/><div id="temp">--</div>
                  </p>
                </div>

                <div class="col-sm-2">
                  <p class="lead">
                    <strong>Cycles:</strong> <br/><div id="cycles">--</div>
                  </p>
                </div>

                <div class="col-sm-2">
                  <p class="lead">
                    <strong>Last updated:</strong> <br/><div id="last_updated">--</div>
                  </p>
                </div>

              </div>
            </div>
            <form id="simulation">
              {{csrf_field()}}
              <input type="hidden"  id = "progress" name="progress" value="default">
              <input type="hidden" id = "time" name="time" value="default">
              <input type="hidden" id = "test" name="test" value="{{$test->id}}">
            </form>
          @endif
          <div class="progress-bar progress-bar-success progress-bar-striped"
           role="progressbar" aria-valuenow="20" aria-valuemin="0"
           aria-valuemax="100"
           style="width: 20%" id="progress_bar">
            <span class="sr-only" id="progress_value">
              20% Complete
            </span>
          </div>
        @else
        Please attach a booked instrument to continue.
        @endif
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary collapsed-box">
      <div class="box-header with-border">
        <h3 class="box-title">Test details</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <form action="" class="form-signin" enctype ="multipart/form-data" method="post" id="experiment_data">
          {{csrf_field()}}
          <input type="hidden" name="id_form" value="{{$test->id_form}}">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Test title:</label>
                  <input name="title" type="text" class="form-control" value="{{$test->title}}" required="" placeholder="Test title...">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label>Privacy:</label>
                <select name="privacy" class="form-control">
                  <option @php if($test->privacy=="Shared") echo "selected" @endphp >Shared</option>
                  <option @php if($test->privacy=="Public") echo "selected" @endphp >Public</option>
                  <option @php if($test->privacy=="Private") echo "selected" @endphp >Private</option>
                </select>
              </div>
            </div>
          </div>
          <hr>

          <div class="row">
            <div class="col-lg-4">
              @if(isset($test->imdata))
                <img class="img-responsive" alt="Test picture" src="data:image/png;base64,{{$test->imdata}}">
              @endif
              {!! Form::file('avatar', null, [
                  'name'  => 'avatar'
              ]) !!}
            </div>
            <div class="col-lg-4">
              <div class="form-group" id="panel_entry">
                  @if(isset($panelHTML))
                    {!!$panelHTML!!}
                  @endif
              </div>
            </div>
              <div class="col-lg-4">
                <div class="form-group" id="extraction_entry">
                  @if(isset($extrHTML))
                    {!!$extrHTML!!}
                  @endif
              </div>
            </div>
          </div>
          <hr>

          <div class="row">
            <div class="col-lg-12">
              <textarea class="input-block-level" id="summernote" name="notes" rows="10">{!!$test->comments!!}</textarea>
                <script>
                  $(document).ready(function() {
                    $('#summernote').summernote({
                      height: 150,                 // set editor height
                      minHeight: null,             // set minimum height of editor
                      maxHeight: null,
                    });
                  });
                  var postForm = function() {
                    var content = $('textarea[name="notes"]').html($('#summernote').code());
                  }
                </script>
              </input>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="row">
            <div class="col-lg-4">
              <button type="button" onclick="save_test()" id="save_tests" class="btn btn-lg btn-default btn-block register-btn">Update test details</button>
            </div>
          </div>
        </form>

        <div class="col-lg-12">
          <div id="message">
            @if(isset($message))
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
              <h4><i class='icon fa fa-warning'></i>Status</h4>
              {{$message}}
            </div>
            @endif
          </div>

          @if($approved)
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Status</h4>
              You are currently approved as a genetic test administrator at: {{$school->name}}.
            </div>
          @else
            <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-warning"></i>Alert!</h4>
              You are not currently approved as a test administrator.
            </div>
          @endif
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary collapsed-box">
      <div class="box-header with-border">
        <h3 class="box-title">Raw fluorescence data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <div class="box-body">
        <div id="chart">
          @if(isset($rawHTML))
            {!!$rawHTML!!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Test results</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <div class="box-body">
        <!-- <button href = "/test/view_analysis/{{$test->id}}" target="_blank" type="button" class="btn btn-default">
          View analysis
        </button> -->
        <div id="results">
          @if(isset($resultHTML))
            {!!$resultHTML!!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">
</div>
@endsection

@section('js')
<script>

var ModalWindow = document.getElementById("Modal");

window.onload = function () {
  $('#result').collapse('show');;
}

$( document ).ajaxComplete(function() {
  $( ".log" ).text( "Triggered ajaxComplete handler." );
});

function get_bookings(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ShowBookings);
  requestX.open("get", "/instrument/getBookings");
  requestX.send();
  document.getElementById("get_bookings").innerHTML = "Loading...";
}

function ShowBookings(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  document.getElementById("get_bookings").innerHTML = "Attach instrument";
}

function connect_instrument(id){
  var fd = new FormData(experiment_data);
  var requestX = new  XMLHttpRequest();
  fd.append('_token', "{{csrf_token()}}");
  fd.append('instrument', id);
  fd.append('test', "{{$test->id}}");

  requestX.addEventListener('load', AttachResponse);
  requestX.open("post", "/instrument/attach");
  requestX.send(fd);
}

function AttachResponse(data){
   console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    location.reload();
  }
}

function save_test(){
  var fd = new FormData(experiment_data);
  var requestX = new  XMLHttpRequest();
  // console.log(document.getElementById('summernote').innerHTML);
  // fd.append('notes', document.getElementById('summernote').innerHTML);
  requestX.addEventListener('load', SaveResponse);
  requestX.open("post", "/test/saveTest");
  requestX.send(fd);
  document.getElementById("save_tests").innerHTML = "Saving...";
}

function SaveResponse(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  document.getElementById("message").innerHTML ="";
  if(resp.success>0){
    document.getElementById("message").innerHTML = "<div class='alert alert-success alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>Successfully saved</div>"
  }
  else{
    document.getElementById("message").innerHTML = "<div class='alert alert-warning alert-dismissible'>"+
    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+
    "<h4><i class='icon fa fa-warning'></i>Status</h4>"+resp.message+"</div>"
  }
  document.getElementById("save_tests").innerHTML = "Update test details";
}

@if(isset($instrument))
  @if($instrument->type == "Simulation")
    //Instrument simulator-------------------------------------------------
    //--------------------------
    var simulator_server = '{!!env("SIMULATOR_SERVER")!!}';
    var socket_server = '{!!env("SOCKET_SERVER")!!}';

    var socket = io(socket_server);
    var current_value = 0;
    var interval = 200;
    var progress = 0.0;
    var run_status = false;

    socket.on("test-channel:App\\Events\\DataEvent", function(message){
        $('#cycles').text(message.data.cycles);
        $('#temp').text(message.data.temp);
        $('#last_updated').text(message.data.time);
        $('#progress_bar').attr('aria-valuenow', progress).css('width',progress);
        $('#progress_value').text(progress);
        document.getElementById("chart").innerHTML = message.data.chartHTML;
        document.getElementById("results").innerHTML = message.data.resultHTML;
    });

    function qPCR_progress() {
      document.getElementById("time").value = Math.floor(Date.now() / 1000);
      document.getElementById("progress").value = progress;
      progress+=5;

      if(run_status)
        $.ajax({
              type: 'POST',
              url: simulator_server,
              data: $(simulation).serialize(),
              dataType: 'jsonp',
              success: function (data) {
                    if(progress>100) stop_run();
                    else setTimeout(qPCR_progress, interval);
              },
              error:function(err){
                  console.log("Error");
                  console.log(err.responseText);
                  if(progress>100) stop_run();
              }
            });
    }

    function start_run(id) {
      run_status = true;
      progress=0;
      qPCR_progress();
    }

    function stop_run(id) {
      progress=0;
      run_status = false;
    }

  @else
    //Websockets--------------------------------------------------
    //--------------------------
    var socket_server = '{!!env("SOCKET_SERVER")!!}';
    var socket = io(socket_server);
    socket.on("test-channel:App\\Events\\DataEvent", function(message){
        // increase the power everytime we load test route
        console.log(message);
        $('#cycles').text(message.data.cycles);
        $('#last_updated').text(message.data.time);
        document.getElementById("chart").innerHTML = message.data.chartHTML;
        document.getElementById("results").innerHTML = message.data.resultHTML;
    });

    @include('user.instrument.chai_functions');


  @endif
@endif

</script>
@stop
