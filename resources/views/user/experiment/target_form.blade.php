<form action="/test/editTarget", class="form-signin", method="post", enctype="multipart/form-data">
  {{csrf_field()}}
  <input type="hidden" name="id" value="{{$target->id}}">
  <div class="row">
    <div class="col-lg-2">
      <div class="form-group">
        <label>Display name:*</label>
        <input type="text" class="form-control" name="name" placeholder="E.g. Beef" value="{{$target->name}}" placeholder="3 to 15 characters" required pattern=".{3,15}" title="Three to fifteen characters">
      </div>
    </div>

    <div class="col-lg-2">
      <div class="form-group">
        <label>Target type:</label>
        <select name="type" class="form-control">
          <option @php if($target->type=="Species identification") echo "selected" @endphp >Species identification</option>
          <option @php if($target->type=="Genotyping") echo "selected" @endphp >Genotyping</option>
          <option @php if($target->type=="Positive Control") echo "selected" @endphp >Positive Control</option>
          <option @php if($target->type=="Negative Control") echo "selected" @endphp >Negative Control</option>
        </select>
      </div>
    </div>

    <div class="col-lg-2">
      <div class="form-group">
        <label>Reference:</label>
        <input type="text" class="form-control" placeholder="E.g.: Link to a publication..." name="reference" value="{{$target->reference}}">
      </div>
    </div>

    <div class="col-lg-2">
      <div class="form-group">
        <label>Genbank accession id:</label>
        <input type="text" class="form-control" placeholder="E.g.: KX550271.1" name="gene_id" value="{{$target->gene_id}}">
      </div>
    </div>

    <div class="col-lg-2">
      <div class="form-group">
        <label>Privacy:</label>
        <select name="privacy" class="form-control">
          <option @php if($target->privacy=="Public") echo "selected" @endphp>Public</option>
          <option @php if($target->privacy=="Shared") echo "selected" @endphp>Shared</option>
          <option @php if($target->privacy=="Private") echo "selected" @endphp>Private</option>
        </select>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-5">
      <div class="form-group">
        <label>Forward primer (5'-3'):</label>
        <input class="form-control" name="forward_primer" placeholder="ATCCG..., 10 to 60 bases" placeholder="" required pattern="[ATCG]{10,60}" title="Ten to sixty DNA bases (A, T, C, G)" value="{{$target->forward_primer}}">
      </div>
    </div>

    <div class="col-lg-5">
      <div class="form-group">
        <label>Backward primer (5'-3'):</label>
        <input class="form-control" name="backward_primer" placeholder="ATCCG..., 10 to 60 bases" placeholder="" required pattern="[ATCG]{10,60}" title="Ten to sixty DNA bases (A, T, C, G)" value="{{$target->backward_primer}}">
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-10">
      <input type="hidden" value="{{$target->derived_from}}" name="derived_from" id="derived">
      <script>
      document.onload = function () {
        additional_list = JSON.parse({{$target->derived_from}});
      }
      </script>
      <div class="input-group margin">
        @php
          if(!is_null($target->derived_from)){
            $multiple = json_decode($target->derived_from);
            $implode = implode(', ', $multiple);
          }
          else{
            $implode = null;
          }
        @endphp
        <input type="text" class="form-control" placeholder="Target mixture..." value="{{$implode}}" id="derived_view">
            <span class="input-group-btn">
              <button type="button" onclick="get_genes()" class="btn btn-info btn-flat" id="list_button">Add gene targets</button>
            </span>
      </div>
      </div>
    </div>


  <div class="row">
    <div class="col-lg-4">
      <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save gene target</button>
    </div>
    <div class="col-lg-4">
      <button type="button" onclick="get_genes2()" class="btn btn-lg btn-default btn-block" id="list_button2">Edit my gene targets</button>
    </div>
  </div>
</form>

<br/>
@if(isset($status))
  @if($status=="success")
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-success"></i>Success</h4>
        The gene target has been updated and is awaiting approval.
    </div>
  @else
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        {{$status}}
    </div>
  @endif
@endif
