{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((isset($admin)) && ($user->activated) && (isset($school)))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Order new')

@section('head')
    <link rel="stylesheet" href="/css/settings.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

@stop

@section('content')

<h3 class="">Order a genetic test</h3>
@if($approved)
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Status</h4>
    You are currently approved as a genetic test administrator at: {{$school->name}}.
  </div>
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are not currently approved as a test administrator.
  </div>
@endif

@include('includes.errors')
<div class="row">
  <div class="col-lg-6">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Standard genetic testing experiment</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
        <div class="box-body">
          <p class="lead">
            Standard test is designed for testing food items for the presence of three commonly
            used meats: chicken, pork and beef. Using processed foods makes it easier to extract DNA
            within a 45 minute practical and the three species can be easily identified using genetic
            variations in their Cytochrome B genes. The experiment is designed to be performed by 12 to
            24 students (working in pairs).<br/>
          </p>
          <div class="row">
            <div class="col-lg-6 col-sm-6">
              <h3>Contents:</h3>
              <table class="table table-condensed">
                <tr class="active">
                  <td>DNA extractions:</td>
                  <td>12 kits</td>
                </tr>
                <tr class="success">
                  <td>Gene panels:</td>
                  <td>2 for chicken/pork/beef</td>
                </tr>
                <tr class="active">
                  <td>Instrument time</td>
                  <td><span id="days_reserved">Book instrument to reserve time</span></td>
                </tr>
                <tr class="success">
                  <td>Price:</td>
                  <td><strong>from 100£</strong></td>
                </tr>
              </table>
            </div>
            <div class="col-lg-6 col-sm-6">
              <h3><a href="/view_experiment/1">View instructions</a></h3>
              <table class="table table-condensed">
                <tr class="active">
                  <td><a href="/test/ExtractionProtocol/1">DNA extraction</a></td>
                  <td>estimated 35 minutes</td>
                </tr>
                <tr class="success">
                  <td><a href="/test/PanelDescription/1">Using gene panel</a></td>
                  <td>estimated 5 minutes</td>
                </tr>
                <tr class="active">
                  <td><a href="/test/view_instrument/1">Using instrument</a></td>
                  <td>estimated 2 minutes</td>
                </tr>
              </table>
              <div class="form-group">
                <button id="book_instrument2"
                  onclick="get_instruments(true)"
                  class="btn btn-md btn-info btn-block register-btn"
                  type="button">Book an instrument
                </button>
              </div>
            </div>
            {!! Form::open(['url' => url('/order/standard/0'), 'class' => 'form-signin'] ) !!}
            <input type="hidden" name="items" value="" id="cart_std">
            <input type="hidden" name="days" value="" id="booking_days_std">
            <button id="standard_submit"
              class="btn btn-lg btn-primary btn-block register-btn"
              type="submit">Reserve and generate a quote
            </button>
            {!! Form::close() !!}
        </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>

    <div class="col col-lg-6">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Custom genetic tests</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
          <div class="box-body">
            <p class="lead">
              Running a test requires the means to extract DNA from the cells
              in your samples, a gene panel containing the targets for testing
              your hypotheses and booking the instrument to run the experiment.
            </p>

              {!! Form::open(['url' => url('/order/quote'), 'class' => 'form-signin'] ) !!}
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <button
                      onclick="get_extractions()"
                      id="dna_extraction"
                      class="btn btn-md btn-info btn-block register-btn"
                      type="button">Select DNA extraction
                    </button>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <button id="gene_panel"
                      onclick="get_panels()"
                      class="btn btn-md btn-info btn-block register-btn"
                      type="button">Select a gene panel
                    </button>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <button id="book_instrument"
                      onclick="get_instruments(false)"
                      class="btn btn-md btn-info btn-block register-btn"
                      type="button">Book an instrument
                    </button>
                  </div>
                </div>
              </div>

              <div class="row">
                <input type="hidden" name="items" value="" id="cart">
                <input type="hidden" name="days" value="" id="booking_days">
                <div class="col-lg-12">
                  <div class="form-group">
                    <table class="table" id="rows">

                    </table>
                  </div>
                  <p class="lead" id="total_price"></p>
                </div>
              </div>

              <button id="custom_submit"
                class="btn btn-lg btn-primary btn-block register-btn"
                type="submit">Reserve and generate a quote
              </button>
              {!! Form::close() !!}
          </div>
        <!-- /.box-body -->
      </div>
    </div>

</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">
</div>
@stop

@section('js')
<script>
var ModalWindow = document.getElementById("Modal");
var cart = [];
var calendar_events = [];
var booking_days = [];
var bookings = [];
var instrument_id = 0;
var standard = false;

var request = new  XMLHttpRequest();
window.onload = function () {

}

$( document ).ajaxComplete(function() {
  $( ".log" ).text( "Triggered ajaxComplete handler." );
});

function get_extractions(){
  standard = false;
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getExtractionListBuy");
  requestX.send();
  document.getElementById("dna_extraction").innerHTML = 'Retrieving methods...';
}

function get_panels(){
  standard = false;
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getPanelListBuy");
  requestX.send();
  document.getElementById("gene_panel").innerHTML = 'Retrieving panels...';
}

function select_extraction(identity){
  var quant = parseInt(document.getElementById("quantity_extraction_"+identity).innerHTML);
  var priceA = {{$extraction_price}};
  var titleA = document.getElementById("title_extraction_"+identity).innerHTML;
  for (var i = 0; i < cart.length; i++) {
    if((cart[i].name == "dna_extraction")&&(cart[i].id == identity))
    {
      cart[i].quantity += quant;
      cart[i].html = "<tr><td>"+titleA+"</td><td>"+cart[i].id+"</td><td>"+cart[i].quantity+"</td><td>"+cart[i].quantity*cart[i].price+"</td><td>"+"<button onclick='delete_extraction("+identity+")' type='button' class='btn btn-sm btn-danger btn-block register-btn'>Remove</button>"+"</td></tr>";
      process_cart();
      return ;
    }
  }
  cart.push({name:"dna_extraction",
  id:identity, quantity:quant, price:priceA, shipping:0,title:titleA,
    html:"<tr><td>"+titleA+"</td><td>"+identity+"</td><td>"+quant+"</td><td>"+quant*priceA+"</td><td>"+"<button onclick='delete_extraction("+identity+")' type='button' class='btn btn-sm btn-danger btn-block register-btn'>Remove</button>"+"</td></tr>"});
  process_cart();
}

function add_extraction(id){
  value = parseInt(document.getElementById("quantity_extraction_"+id).innerHTML)+1;
  if(value<25)
  document.getElementById("quantity_extraction_"+id).innerHTML=value.toString();
}

function subtract_extraction(id){
  value = parseInt(document.getElementById("quantity_extraction_"+id).innerHTML)-1;
  if(value>0)
    document.getElementById("quantity_extraction_"+id).innerHTML=value.toString();
}

function delete_extraction(id){
  var cart_tmp = [];
  for (var i = 0; i < cart.length; i++){
    if((cart[i].name == "dna_extraction")&&(cart[i].id == id)){}
    else
      cart_tmp.push(cart[i]);
  }
  cart = cart_tmp;
  process_cart();
}

function select_panel(identity){
  var quant = parseInt(document.getElementById("quantity_panel_"+identity).innerHTML);
  var priceA = {{$panel_price}};
  var titleA = document.getElementById("title_panel_"+identity).innerHTML;
  for (var i = 0; i < cart.length; i++) {
    if((cart[i].name == "gene_panel")&&(cart[i].id == identity))
    {
      cart[i].quantity += quant;
      cart[i].html = "<tr><td>"+titleA+"</td><td>"+cart[i].id+"</td><td>"+cart[i].quantity+"</td><td>"+cart[i].quantity*cart[i].price+"</td><td>"+"<button onclick='delete_panel("+identity+")' type='button' class='btn btn-sm btn-danger btn-block register-btn'>Remove</button>"+"</td></tr>";
      process_cart();
      return ;
    }
  }
  cart.push({name:"gene_panel",
  id:identity, quantity:quant, price:priceA, shipping:0, title:titleA,
    html:"<tr><td>"+titleA+"</td><td>"+identity+"</td><td>"+quant+"</td><td>"+quant*priceA+"</td><td>"+"<button onclick='delete_panel("+identity+")' type='button' class='btn btn-sm btn-danger btn-block register-btn'>Remove</button>"+"</td></tr>"});
  process_cart();
}

function add_panel(id){
  value = parseInt(document.getElementById("quantity_panel_"+id).innerHTML)+1;
  if(value<25)
  document.getElementById("quantity_panel_"+id).innerHTML=value.toString();
}

function subtract_panel(id){
  value = parseInt(document.getElementById("quantity_panel_"+id).innerHTML)-1;
  if(value>0)
    document.getElementById("quantity_panel_"+id).innerHTML=value.toString();
}

function delete_panel(id){
  var cart_tmp = [];
  for (var i = 0; i < cart.length; i++){
    if((cart[i].name == "gene_panel")&&(cart[i].id == id)){}
    else
      cart_tmp.push(cart[i]);
  }
  cart = cart_tmp;
  process_cart();
}

function get_instruments(std){
  standard = std;
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/instrument/getInstrumentList");
  requestX.send();
  if(standard){
    var cart = [];
    var calendar_events = [];
    var booking_days = [];
    var bookings = [];
    var instrument_id = 0;
    document.getElementById("book_instrument2").innerHTML = 'Loading...';
  }
  else
    document.getElementById("book_instrument").innerHTML = 'Loading...';
}

function select_instrument(id){
  instrument_id = id;
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessCalendar);
  requestX.open("get", "/instrument/getAvailability/"+id);
  requestX.send();
  if(standard)
    document.getElementById("book_instrument2").innerHTML = 'Loading...';
  else
    document.getElementById("book_instrument").innerHTML = 'Loading...';
  ModalWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
}


function ProcessList(data){
    console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "Not found...";
  }
  document.getElementById("dna_extraction").innerHTML = "Select DNA extraction";
  document.getElementById("gene_panel").innerHTML = "Select a gene panel";
  if(standard)
    document.getElementById("book_instrument2").innerHTML = "Book an instrument";
  else
    document.getElementById("book_instrument").innerHTML = "Book an instrument";
}

function ProcessCalendar(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  if(standard)
    document.getElementById("book_instrument2").innerHTML = "Book an instrument";
  else
    document.getElementById("book_instrument").innerHTML = "Book an instrument";
  calendar_events = JSON.parse(resp.events);
  update_calendar();
}

function process_cart(){
    var total_price = 0;
    var rows = "<tr><td>Type</td><td>Id</td><td>Quantity</td><td>Price</td><td>Remove</td></tr>";
    for (var i = 0; i < cart.length; i++){
      rows = rows + cart[i].html;
      total_price += cart[i].price*cart[i].quantity+cart[i].shipping;
    }

    if(!standard){
      document.getElementById("cart").value = JSON.stringify(cart);
      document.getElementById("booking_days").value = JSON.stringify(bookings);
      document.getElementById("rows").innerHTML = rows;
      document.getElementById("total_price").innerHTML = "Total price: " + total_price + "£";
    }
    else{
      document.getElementById("cart_std").value = JSON.stringify(cart);
      document.getElementById("booking_days_std").value = JSON.stringify(bookings);
      if(booking_days.length == 1)
        document.getElementById("days_reserved").innerHTML = "1 day + 2 for shipping";
      else
        document.getElementById("days_reserved").innerHTML = booking_days.length+" days";
    }
}

function update_calendar(){
  // console.log(calendar_events);
  $('#calendar').fullCalendar({
    header:{
      left:"prev, next today",
      center:"title",
      right:"month"},
      eventLimit:false,
      events:calendar_events,
      dayClick: add_event,
  });
}

function add_event(date, jsEvent, view) {
    // change the day's background color
    // console.log('Clicked on: ' + Date.parse(date.format()));
    for (var i = 0; i < calendar_events.length; i++) {
      // console.log('Calendar start: ' + Date.parse(calendar_events[i].start));
      // console.log('Calendar end: ' + Date.parse(calendar_events[i].end));

      if((Date.parse(date.format())>Date.parse(calendar_events[i].start))&&(Date.parse(date.format())<Date.parse(calendar_events[i].end)))
        return;
      if(Date.parse(date.format())==Date.parse(calendar_events[i].start))
        return;
    }
    booking_days.push(Date.parse(date.format()));
    $(this).css('background-color', 'green');
}

function book(){
  bookings = [];
  var sorted_days = booking_days.sort();
  for (var i = 0; i < sorted_days.length; i++) {
    if(i==0){
      bookings.push({
        allDay:true,
        end:sorted_days[i]+86400000,
        id:0,
        start:sorted_days[i],
        title:"Booking"
      });
    }
    else {
      if((sorted_days[i]-sorted_days[i-1])==86400000){//Exactly one day difference means continued booking
      bookings[bookings.length-1].end = sorted_days[i]+86400000;
      }
      else{
        bookings.push({
          allDay:true,
          end:sorted_days[i]+86400000,
          id:0,
          start:sorted_days[i],
          title:"Booking"
        });
      }
    }
  }
  var booking_id = 0;
  var booking_day = {{$booking_price}};
  var booking_transport = {{$transport_price}};
  for (var i = 0; i < bookings.length; i++) {
    booking_id += 1;
    var days = parseInt((bookings[i].end-bookings[i].start)/86400000);
    cart.push({name:"booking", shipping:booking_transport, title:"Instrument booking",
    id:booking_id, quantity:days, dates:[bookings[i].end, bookings[i].start], price:booking_day,
      html:"<tr><td>"+"Instrument booking"+"</td><td>"+instrument_id+"</td><td>"+days+" d.</td><td>"+(days*booking_day+2*booking_transport)+"</td><td>"+"<button onclick='delete_instrument("+booking_id+")' type='button' class='btn btn-sm btn-danger btn-block register-btn'>Remove</button>"+"</td></tr>"});
  }
  process_cart();
  booking_days = [];
}

function delete_instrument(id){
  var cart_tmp = [];
  for (var i = 0; i < cart.length; i++){
    if((cart[i].name == "booking")&&(cart[i].id == id)){}
    else
      cart_tmp.push(cart[i]);
  }
  cart = cart_tmp;
  process_cart();
}

</script>
@stop
