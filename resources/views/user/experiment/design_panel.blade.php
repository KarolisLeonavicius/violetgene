{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'Gene Panel')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">Designing gene panels</h3>
<!-- @if($approved)
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-success"></i>Status</h4>
    You are approved as a genetic test administrator at: {{$school->name}}.
  </div>
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are not currently approved as a test administrator.
  </div>
@endif -->
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create a gene panel</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" id="FormWindow">
        @if(isset($panel))
          {!! $panel !!}
        @else
          <form action="/test/createPanel", class="form-signin", method="post", enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group">
                  <label>Panel name:*</label>
                  <input type="text" class="form-control" name="name" placeholder="E.g. Sandwitch meat test" required>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="form-group">
                  <label>Panel type:</label>
                  <select name="type" class="form-control">
                    <option>Species identification</option>
                    <option>Genotyping</option>
                    <option>Mixed</option>
                  </select>
                </div>
              </div>

              <div class="col-lg-2">
                <div class="form-group">
                  <label>Privacy:</label>
                  <select name="privacy" class="form-control">
                    <option>Public</option>
                    <option>Shared</option>
                    <option>Private</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-9">
                <div class="form-group">
                  <label>Description:</label>
                  <textarea class="form-control" name="content" placeholder="Write about your panel..."></textarea>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-9">
                <button type="button" onclick="get_genes()" class="btn btn-lg btn-success btn-block register-btn" id="GetButton"><i class="fa fa-plus"></i> Add gene targets</button>
                <h3>Gene targets in the panel:</h3>
                <input type="hidden" name="genes" value="" id="genes">
                <input type="hidden" name="reactions" value="" id="total_form">
                <input type="hidden" name="pos_controls" value="" id="pos_form">
                <input type="hidden" name="neg_controls" value="" id="neg_form">
                <table class="table">
                  <tr>
                    <td>Total reactions: <span id="total">0</span></td>
                    <td>Positive controls: <span id="positive">0</span></td>
                    <td>Negative controls: <span id="negative">0</span></td>
                  </tr>
                </table>

                <table class="table" id="rows">
                </table>

                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4">
                <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save gene panel</button>
              </div>
              <div class="col-lg-4">
                <button type="button" onclick="get_panels()" class="btn btn-lg btn-default btn-block" id="list_button2">Edit my gene panels</button>
              </div>
            </div>
          </form>
        @endif

        <hr class="section-heading-spacer">
        <div class="clearfix"></div>

        <p class="lead">
          <strong>A gene panel</strong> is a collection of gene targets, that allow you to test a formulated hypothesis.
          A single panel contains <strong>8 gene</strong> targets, but you can also design a <strong>16 gene</strong> panel to be run at the same time at double the cost.
          Increasing panel size beyond that will require multiple runs.<br/><br/>
          <strong>To design a panel</strong> you can use our gene target primer database, or add new primers by searching published literature
          for RT-PCR testing and performing BLAST searches to check primer specificity. A reliable panel also has to contain negative and positive
          controls, which would allow to verify that the reaction and DNA extraction are working properly.<br/><br/>
          <strong>Three broad types</strong> of panels include:
          <ul>
            <li><strong>Species identification</strong> - panel is designed to identify biological species present in the sample.</li>
            <li><strong>Genotyping</strong> - panel is designed to identify the genes and mutations present in a species of interest.</li>
            <li><strong>Mixed</strong> - panels include aspects of both of the above.</li>
          </ul>
          While the biological principles are exactly the same in how these different panels work, they mainly differ in data analysis.
        </p>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">
</div>
@stop

@section('js')
<script>

var Button = document.getElementById("GetButton");
var ModalWindow = document.getElementById("Modal");
var rows = document.getElementById("rows");

//Preload genes, if the panels already contains a gene list
var gene_panel = @php if(isset($genes)) echo "$genes"; else echo "[]"; @endphp;


var request = new  XMLHttpRequest();
window.onload = function () {
  $('#Modal').modal({show: false}) //Off by default
  // lookup_button.addEventListener('click', LookupCodes, false);
  @php if(isset($genes)) echo "createRows();"; @endphp;
}

function get_genes(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getTargetList");
  requestX.send();
  Button.innerHTML = 'Retrieving gene targets...';
}

function get_panels(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getPanelList");
  requestX.send();
  document.getElementById("list_button2").innerHTML = 'Retrieving panels...';
}

function select(id){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessListRow);
  requestX.open("get", "/test/getPanelRow/"+id);
  requestX.send();
}

function remove(id){
  var gene_panel_tmp = [];
  for (var i = 0; i < gene_panel.length; i++) {
    if(gene_panel[i].id != id){
      gene_panel_tmp.push(gene_panel[i]);
    }
  }
  gene_panel = gene_panel_tmp;
  createRows();
}

function ProcessList(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "No genes found...";
  }
  Button.innerHTML = "<i class='fa fa-plus'></i> Add gene targets";
  document.getElementById("list_button2").innerHTML="Edit my gene panels";
}

function ProcessListRow(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    // ModalWindow.innerHTML = resp.html;
    gene_panel.push(JSON.parse(resp.gene));
    createRows();
  }
  else{
    ModalWindow.innerHTML = "No genes found...";
  }
  Button.innerHTML = "<i class='fa fa-plus'></i> Add gene targets";
}

function createRows(){
  var count = 0;
  var pos = 0;
  var neg = 0;

  var rows = "<tr><td>Type</td><td>Name</td><td>Gene ID</td><td>Info</td><td>Remove</td></tr>";
  for (var i = 0; i < gene_panel.length; i++) {
    rows = rows+gene_panel[i].html;
    count += 1;
    if(gene_panel[i].type == "Positive Control"){
      pos += 1;
    }
    if(gene_panel[i].type == "Negative Control"){
      neg += 1;
    }
  }
  document.getElementById("rows").innerHTML= rows;
  document.getElementById("total").innerHTML= count;
  document.getElementById("positive").innerHTML= pos;
  document.getElementById("negative").innerHTML= neg;

  document.getElementById("genes").value= JSON.stringify(gene_panel);
  document.getElementById("total_form").value= count;
  document.getElementById("pos_form").value= pos;
  document.getElementById("neg_form").value= neg;
}


</script>
@stop
