<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Gene Panels</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Sample</strong></p>
        </td>
        <td>
          <p class=""><strong>Steps</strong></p>
        </td>
        <td>
          <p class=""><strong>Time</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Price, £</strong></p>
        </td>
        <td>
          <p class=""><strong>Qty</strong></p>
        </td>
        <td>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
      </tr>
      @foreach($panels as $panel)
      <tr>
        <div style="Display:none" id="id_panel_{{$panel->id}}">{{$panel->id}}</div>
        <td>
          <p class="" id="title_panel_{{$panel->id}}">{{$panel->type}}</p>
        </td>
        <td>
          <p class="">{{$panel->steps}}</p>
        </td>
        <td>
          <p class="">{{$panel->duration}}</p>
        </td>
        <td>
          <a href="/test/PanelDescription/{{$panel->id}}" target="_blank">Protocol</a>
        </td>
        <td>
          <p class="">{{$panel->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <p class="" id="price_panel_{{$panel->id}}">{{App\Models\Price::where('item', 'gene_panel')->first()->price_pounds}}</p>
        </td>
        <td>
          <p class="" id="quantity_panel_{{$panel->id}}">1</p>
        </td>
        <td>
          <i onclick="add_panel({{$panel->id}})" class="fa fa-fw fa-plus-square"></i><i onclick="subtract_panel({{$panel->id}})" class="fa fa-fw fa-minus-square"></i>
        </td>
        <td>
          <button class="btn btn-sm btn-success btn-block register-btn" onclick="select_panel({{$panel->id}})">Add</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
