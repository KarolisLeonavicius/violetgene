<tr id="row_$target->id">
  <td>{{$target->type}}</td>
  <td>{{$target->name}}</td>
  <td>{{$target->gene_id}}</td>
  <td>
    @if(isset($target->gene_id))
      <a href="https://www.ncbi.nlm.nih.gov/nuccore/{{$target->gene_id}}" target="_blank" class="btn btn-sm btn-info">More information</a>
    @else
      <a></a>
    @endif
  </td>
  <td>
    <button class="btn btn-sm btn-danger" onclick="remove({{$target->id}})">Remove</button>
  </td>
</tr>
