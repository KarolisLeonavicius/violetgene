@extends('layouts.master')

@section('title', 'Experiment')

@section('content')
<div class="content-section-a">
  <div class="container">
    <h2>Experiment:</h2>
    <h3>Gene list:</h3>
    <h3>Description:</h3>
    <p class="lead">
    </p>
  </div>
</div>


<div class="content-section-b">
  <div class="container">
    {!!$content!!}
  </div>
</div>

@endsection

@section('js')

@endsection
