<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available Panels</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Type</strong></p>
        </td>
        <td>
          <p class=""><strong>Name</strong></p>
        </td>
        <td>
          <p class=""><strong>Description</strong></p>
        </td>
        <td>
          <p class=""><strong>Reaction #</strong></p>
        </td>
        <td>
          <p class=""><strong>Approved</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
        <td>
          <p class=""><strong>Delete</strong></p>
        </td>
      </tr>
      @foreach($panels as $panel)
      <tr>
        <div style="Display:none" id="id_{{$panel->id}}">{{$panel->id}}</div>
        <td>
          <p class="">{{$panel->type}}</p>
        </td>
        <td>
          <p class="">{{$panel->name}}</p>
        </td>
        <td>
          <p class="">{{$panel->content}}</p>
        </td>
        <td>
          <p class="">{{$panel->reactions}}</p>
        </td>
        <td>
          @if(!is_null($panel->approved))
            <p class="">Yes</p>
          @else
            <p class="">No</p>
          @endif
        </td>

        <td>
          <p class="">{{$panel->created_at->diffForHumans()}}</p>
        </td>

        <td>
          <a href="/test/design_panel/{{$panel->id}}" class="btn btn-sm btn-warning btn-block register-btn" id="edit_{{$panel->id}}">Edit</a>
        </td>

        <td>
          <form action="/test/deletePanel/{{$panel->id}}" method="post">
            {{csrf_field()}}
            <button type="submit" class="btn btn-sm btn-danger btn-block register-btn">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
