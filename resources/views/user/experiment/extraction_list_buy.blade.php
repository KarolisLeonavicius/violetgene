<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available DNA extraction methods</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Sample</strong></p>
        </td>
        <td>
          <p class=""><strong>Steps</strong></p>
        </td>
        <td>
          <p class=""><strong>Time</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Price, £</strong></p>
        </td>
        <td>
          <p class=""><strong>Qty</strong></p>
        </td>
        <td>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
      </tr>
      @foreach($extractions as $extraction)
      <tr>
        <div style="Display:none" id="id_extraction_{{$extraction->id}}">{{$extraction->id}}</div>
        <td>
          <p class="" id="title_extraction_{{$extraction->id}}">{{$extraction->type}}</p>
        </td>
        <td>
          <p class="">{{$extraction->steps}}</p>
        </td>
        <td>
          <p class="">{{$extraction->duration}}</p>
        </td>
        <td>
          <a href="/test/ExtractionProtocol/{{$extraction->id}}" target="_blank">Protocol</a>
        </td>
        <td>
          <p class="">{{$extraction->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <p class="" id="price_extraction_{{$extraction->id}}">{{App\Models\Price::where('item', 'dna_extraction')->first()->price_pounds}}</p>
        </td>
        <td>
          <p class="" id="quantity_extraction_{{$extraction->id}}">1</p>
        </td>
        <td>
          <i onclick="add_extraction({{$extraction->id}})" class="fa fa-fw fa-plus-square"></i><i onclick="subtract_extraction({{$extraction->id}})" class="fa fa-fw fa-minus-square"></i>
        </td>
        <td>
          <button class="btn btn-sm btn-success btn-block register-btn" onclick="select_extraction({{$extraction->id}})">Add</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
