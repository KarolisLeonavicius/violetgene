<form action="/test/createExtraction", class="form-signin", method="post", enctype="multipart/form-data" onsubmit="return postForm()">
  {{csrf_field()}}
  <div class="row">
    <input type="hidden" value="{{$extraction->derived_from}}" name="derived_from">
    <div class="col-lg-3">
      <div class="form-group">
        <label>Sample type:</label>
        <select name="type" class="form-control">
          <option @php if($extraction->type=="Cooked food") echo "selected" @endphp >Cooked food</option>
          <option @php if($extraction->type=="Uncooked food") echo "selected" @endphp >Uncooked food</option>
          <option @php if($extraction->type=="Liquids") echo "selected" @endphp >Liquids</option>
          <option @php if($extraction->type=="Micro samples") echo "selected" @endphp >Micro samples</option>
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label>Number of steps:</label>
        <select name="steps" class="form-control">
          <option @php if($extraction->steps==1) echo "selected" @endphp >1</option>
          <option @php if($extraction->steps==2) echo "selected" @endphp >2</option>
          <option @php if($extraction->steps==3) echo "selected" @endphp >3</option>
          <option @php if($extraction->steps==4) echo "selected" @endphp >4</option>
          <option @php if($extraction->steps==5) echo "selected" @endphp >5</option>
          <option @php if($extraction->steps==6) echo "selected" @endphp >6</option>
          <option @php if($extraction->steps==7) echo "selected" @endphp >7</option>
          <option @php if($extraction->steps==8) echo "selected" @endphp >8</option>
          <option @php if($extraction->steps==9) echo "selected" @endphp >9</option>
          <option @php if($extraction->steps==10) echo "selected" @endphp >10</option>
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label>Time, minutes:</label>
        <select name="duration" class="form-control">
          <option @php if($extraction->duration=="under 10 mins") echo "selected" @endphp >under 10 mins</option>
          <option @php if($extraction->duration=="under 30 mins") echo "selected" @endphp >under 30 mins</option>
          <option @php if($extraction->duration=="under 60 mins") echo "selected" @endphp >under 60 mins</option>
          <option @php if($extraction->duration=="over 60 mins") echo "selected" @endphp >over 60 mins</option>
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label>Privacy:</label>
        <select name="privacy" class="form-control">
          <option @php if($extraction->privacy=="Shared") echo "selected" @endphp >Shared</option>
          <option @php if($extraction->privacy=="Public") echo "selected" @endphp >Public</option>
          <option @php if($extraction->privacy=="Private") echo "selected" @endphp >Private</option>
        </select>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <h3>Protocol</h3>
      <p>(method id: {{$extraction->id}})</p>
      <textarea class="input-block-level" id="summernote" name="protocol" rows="10">{{$extraction->protocol}}</textarea>
      <script type="text/javascript">
        $(document).ready(function() {
          $('#summernote').summernote({
            height: "400px"
          });
        });
        var postForm = function() {
          var content = $('textarea[name="protocol"]').html($('#summernote').code());
        }
      </script>
    </div>
    <!-- /.box-body -->
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="form-group">
        <label>Additional requirements:</label>
        <input class="form-control" type="text" name="additional" placeholder="" value="{{$extraction->additional}}">
      </div>
    </div>

    <div class="col-lg-4">
      <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save extraction method</button>
    </div>
  </div>
</form>

<br/>
@if(isset($status))
  @if($status=="success")
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-success"></i>Success</h4>
        Successfully saved and sent for approval before publishing.
    </div>
  @else
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        {{$status}}
    </div>
  @endif
@endif
