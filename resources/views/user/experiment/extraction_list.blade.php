<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available DNA extraction methods</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Sample</strong></p>
        </td>
        <td>
          <p class=""><strong>Steps</strong></p>
        </td>
        <td>
          <p class=""><strong>Time</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Created</strong></p>
        </td>
        <td>
          <p class=""><strong>Select</strong></p>
        </td>
        <td>
          <p class=""><strong>Delete</strong></p>
        </td>
      </tr>
      @foreach($extractions as $extraction)
      <tr>
        <div style="visibility:hidden" id="id_{{$extraction->id}}">{{$extraction->id}}</div>
        <td>
          <p class="">{{$extraction->type}}</p>
        </td>
        <td>
          <p class="">{{$extraction->steps}}</p>
        </td>
        <td>
          <p class="">{{$extraction->duration}}</p>
        </td>
        <td>
          <a href="/test/ExtractionProtocol/{{$extraction->id}}" target="_blank">Protocol</a>
        </td>
        <td>
          <p class="">{{$extraction->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <button class="btn btn-sm btn-warning btn-block register-btn" onclick="select_extraction({{$extraction->id}})">Select</button>
        </td>
        <td>
          @if($extraction->user_id == $user->id)
          <form action="/test/deleteExtraction/{{$extraction->id}}" method="post">
            {{csrf_field()}}
            <button type="submit" class="btn btn-sm btn-danger btn-block register-btn">Delete</button>
          </form>
          @endif
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
