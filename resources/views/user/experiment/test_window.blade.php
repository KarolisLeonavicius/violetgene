
  <div class="box-body no-padding">
  <table class="table table-striped">
    <tbody><tr>
      <th>Quality</th>
      <th>Summary</th>
      <th style="width: 40px">Score</th>
    </tr>
    <tr>
      <td>Overall</td>
      <td>
        <div class="progress progress-xs progress-striped active">
          <div class="progress-bar progress-bar-green" style="width: 80%"></div>
        </div>
      </td>
      <td><span class="badge bg-green">80%</span></td>
    </tr>
    <tr>
      <td>Overall</td>
      <td>
        <div class="progress progress-xs progress-striped active">
          <div class="progress-bar progress-bar-green" style="width: 80%"></div>
        </div>
      </td>
      <td><span class="badge bg-green">80%</span></td>
    </tr>
    <tr>
      <td>Overall</td>
      <td>
        <div class="progress progress-xs progress-striped active">
          <div class="progress-bar progress-bar-green" style="width: 80%"></div>
        </div>
      </td>
      <td><span class="badge bg-green">80%</span></td>
    </tr>
  </tbody></table>
  </div>
  <hr>
