<table class="table table-striped table-hover table-condensed">
  <tr>
    <thead>
      <th></th>
      <th>Gene target type</th>
      <tH>Gene name</th>
      <th>Gene id</th>
      <th>Database link</th>
    </thead>
  </tr>
  <tbody>
    @php $i = 1; @endphp
    @foreach($genes as $gene)
    <tr>
      <th scope="row">{{$i++}}</th>
      <td>{{$gene->type}}</td>
      <td>{{$gene->name}}</td>
      <td>{{$gene->gene_id}}</td>
      <td>
        @if(isset($gene->gene_id))
          <a href="https://www.ncbi.nlm.nih.gov/nuccore/{{$gene->gene_id}}" target="_blank" class="btn btn-sm btn-info">More information</a>
        @else
          <a></a>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
