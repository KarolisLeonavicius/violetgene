@extends('layouts.master')

@section('title', 'Instructions')

@section('content')
<div class="content-section-a">
  <div class="container">
    {!!$content!!}
  </div>
</div>

@endsection

@section('js')

@endsection
