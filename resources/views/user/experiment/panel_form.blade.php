<form action="/test/editPanel", class="form-signin", method="post", enctype="multipart/form-data">
  {{csrf_field()}}
  <input type="hidden" name="id" value="{{$panel->id}}">
  <div class="row">
    <div class="col-lg-4">
      <div class="form-group">
        <label>Panel name:</label>
        <input type="text" class="form-control" value="{{$panel->name}}" name="name" placeholder="E.g. Sandwitch meat test" required="">
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <label>Panel type:</label>
        <select name="type" class="form-control">
          <option @php if($panel->type=="Species identification") echo "selected" @endphp >Species identification</option>
          <option @php if($panel->type=="Genotyping") echo "selected" @endphp >Genotyping</option>
          <option @php if($panel->type=="Mixed") echo "selected" @endphp >Mixed</option>
        </select>
      </div>
    </div>

    <div class="col-lg-2">
      <div class="form-group">
        <label>Privacy:</label>
        <select name="privacy" class="form-control">
          <option @php if($panel->privacy=="Public") echo "selected" @endphp>Public</option>
          <option @php if($panel->privacy=="Shared") echo "selected" @endphp>Shared</option>
          <option @php if($panel->privacy=="Private") echo "selected" @endphp>Private</option>
        </select>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-9">
      <div class="form-group">
        <label>Description:</label>
        <textarea class="form-control" name="content" placeholder="Write about your panel...">{{$panel->content}}</textarea>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-9">
      <button type="button" onclick="get_genes()" class="btn btn-lg btn-success btn-block register-btn" id="GetButton"><i class="fa fa-plus"></i> Add gene targets</button>
      <h3>Gene targets in the panel:</h3>
      <input type="hidden" name="genes" value="" id="genes">
      <input type="hidden" name="reactions" value="{{$panel->reactions}}" id="total_form">
      <input type="hidden" name="pos_controls" value="{{$panel->pos_controls}}" id="pos_form">
      <input type="hidden" name="neg_controls" value="{{$panel->neg_controls}}" id="neg_form">
      <table class="table">
        <tr>
          <td>Total reactions: <span id="total">{{$panel->reactions}}</span></td>
          <td>Positive controls: <span id="positive">{{$panel->pos_controls}}</span></td>
          <td>Negative controls: <span id="negative">{{$panel->neg_controls}}</span></td>
        </tr>
      </table>

      <table class="table" id="rows">
      </table>

      <hr class="section-heading-spacer">
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4">
      <button type="submit" class="btn btn-lg btn-primary btn-block register-btn">Save gene panel</button>
    </div>
    <div class="col-lg-4">
      <button type="button" onclick="get_panels()" class="btn btn-lg btn-default btn-block" id="list_button2">Edit my gene panels</button>
    </div>
  </div>
</form>


<br/>
@if(isset($status))
  @if($status=="success")
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-success"></i>Success</h4>
        The gene panel has been updated and is awaiting approval.
    </div>
  @else
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        {{$status}}
    </div>
  @endif
@endif
