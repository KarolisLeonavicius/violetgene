
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Title</strong></p>
        </td>
        <td>
          <p class=""><strong>Genes</strong></p>
        </td>
        <td>
          <p class=""><strong>Privacy</strong></p>
        </td>
        <td>
          <p class=""><strong>Created at</strong></p>
        </td>
        <td>
          <p class=""><strong>Updated at</strong></p>
        </td>
        <td>
          <p class=""><strong>Run</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Delete</strong></p>
        </td>
      </tr>
      @foreach($tests as $test)
      <tr>
        <td>
          <p class="">{{$test->title}}</p>
        </td>
        <td>
          <p class="">{!!$test->genes()!!}</p>
        </td>
        <td>
          <p class="">{{$test->privacy}}</p>
        </td>
        <td>
          <p class="">{{$test->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <p class="">{{$test->updated_at->diffForHumans()}}</p>
        </td>
        <td>
          <a href="{{env('APP_INSECURE')}}/test/runTest/{{$test->id}}" class="btn btn-sm btn-success btn-block register-btn">Run</a>
        </td>
        <td>
          <a href="/test/my_tests/{{$test->id}}" class="btn btn-sm btn-default btn-block register-btn">View</a>
        </td>
        <td>
          <form action="/test/deleteTest/{{$test->id}}" method="post">
            {{csrf_field()}}
            <button type="submit" class="btn btn-sm btn-danger btn-block register-btn">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
      </table>
