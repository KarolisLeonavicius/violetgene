{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $admin = $user->admin()->first();
  $school = $user->school()->first();
  if((\App\Models\Role::find($user->roles()->first()->id)->name=='admin') && ($user->activated))
    $approved = true;
  else
    $approved = false;
@endphp

@extends('adminlte::page')

@section('title', 'My tests')

@section('head')
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
@stop

@section('content')

<h3 class="">My genetic tests</h3>
<!-- @if($approved)
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-success"></i>Status</h4>
    You are approved as a genetic test administrator at: {{$school->name}}.
  </div>
@else
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i>Alert!</h4>
    You are not currently approved as a test administrator.
  </div>
@endif -->
@if(isset($test))
  <div class="row">
    <div class="col-lg-12">
      <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Test details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label>Test title:</label>
                  <input name="title" type="text" class="form-control" value="{{$test->title}}" required="" placeholder="Test title...">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <label>Privacy:</label>
                <select name="privacy" class="form-control">
                  <option @php if($test->privacy=="Shared") echo "selected" @endphp >Shared</option>
                  <option @php if($test->privacy=="Public") echo "selected" @endphp >Public</option>
                  <option @php if($test->privacy=="Private") echo "selected" @endphp >Private</option>
                </select>
              </div>
            </div>
          </div>
          <hr>

          <div class="row">
            <div class="col-lg-4">
              @if(isset($test->imdata))
                <img class="img-responsive" alt="Test picture" src="data:image/png;base64,{{$test->imdata}}">
              @endif
              {!! Form::file('avatar', null, [
                  'name'  => 'avatar'
              ]) !!}
            </div>
            <div class="col-lg-4">
              <div class="form-group" id="extraction_entry">
                  @if(isset($panelHTML))
                    {!!$panelHTML!!}
                  @endif
              </div>
            </div>
              <div class="col-lg-4">
                <div class="form-group" id="panel_entry">
                  @if(isset($extrHTML))
                    {!!$extrHTML!!}
                  @endif
              </div>
            </div>
          </div>
          <hr>

          <div class="row">
            <div class="col-lg-12">
              <textarea class="input-block-level" id="summernote" name="notes" rows="10">{!!$test->comments!!}</textarea>
                <script>
                  $(document).ready(function() {
                    $('#summernote').summernote({
                      height: 150,                 // set editor height
                      minHeight: null,             // set minimum height of editor
                      maxHeight: null,
                    });
                  });
                  var postForm = function() {
                    var content = $('textarea[name="notes"]').html($('#summernote').code());
                  }
                </script>
              </input>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.user-block -->
          <hr>
          <button id="test_{{$test->id}}" onclick="like('/comment/like/test/{{$test->id}}', 'test_{{$test->id}}')" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>

          @if(!empty($comments)&&!empty($likes))
              <span class="pull-right text-muted">{{count($likes)}} likes - {{count($comments)}} comments</span>
          @endif
          <br/>
          <br/>
          <div class="box-footer box-comments">
            @if(!empty($comments))
              @if(count($comments)>0)
              <p class="lead">
                Test comments:
              </p>
                @foreach($comments as $comment)
                <div class="box-comment">
                  <!-- User image -->
                  @php
                    $comment_user = $comment->user()->first();
                    echo "<img class='img-circle img-sm' src='/uploads/avatars/".$comment_user->avatar."' alt='User Image'>";
                    echo "<div class='comment-text'>";
                    echo "<span class='username'>";
                            echo $comment_user->first_name;
                            echo " ";
                            echo $comment_user->last_name;
                          @endphp
                          <span class="text-muted pull-right">{{$comment->created_at->diffForHumans()}}</span>

                        </span><!-- /.username -->
                      {{$comment->comment}}
                  </div>
                  <!-- /.comment-text -->
                </div>
                @endforeach
              @endif
            @endif
            <form action="/comment/post/test/{{$test->id}}" method="post">
              {{csrf_field()}}
              <img class="img-responsive img-circle img-sm" src="/uploads/avatars/{{$user->avatar}}" alt="Avatar">
              <!-- .img-push is used to add margin to elements next to floating images -->
              <div class="img-push">
                <input type="text" name="comment" class="form-control input-sm" required placeholder="Press enter to post comment">
              </div>
            </form>
            <!-- /.box-comment -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Test results</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <div class="box-body">
          <!-- <button href = "/test/view_analysis/{{$test->id}}" target="_blank" type="button" class="btn btn-default">
            View analysis
          </button> -->
          <div id="results">
            @if(isset($resultHTML))
              {!!$resultHTML!!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Raw data</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <div class="box-body">
          <div id="chart">
            @if(isset($rawHTML))
              {!!$rawHTML!!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Test details</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        @if(isset($tests))
          {!!$tests!!}
        @else
          No tests yet.
        @endif
        <div class="box-footer box-comments">

          <!-- /.box-comment -->
        </div>
      </div>
      <!-- /.box-body -->
    </div>

  </div>
</div>
<br/>


<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Answers">
</div>
@stop

@section('js')
<script>
var ModalWindow = document.getElementById("Modal");
var Button = document.getElementById("view_tests");

var request = new  XMLHttpRequest();

window.onload = function () {
  $('#Modal').modal({show: false}) //Off by default
}

function view_tests(){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessList);
  requestX.open("get", "/test/getTestList2");
  requestX.send();
  Button.innerHTML = 'Retrieving my tests...';
}

function ProcessList(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    gene_list = resp.gene_list;
    ModalWindow.innerHTML = "";
    ModalWindow.innerHTML = resp.html;
    $('#Modal').modal('show');
  }
  else{
    ModalWindow.innerHTML = "No tests found...";
  }
  Button.innerHTML="View my tests";
}


function run(id){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessListRow);
  requestX.open("get", "/test/getPanelRow/"+id);
  requestX.send();
}

function view(id){
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessListRow);
  requestX.open("get", "/test/getPanelRow/"+id);
  requestX.send();
}

function remove(id){
  var gene_panel_tmp = [];
  for (var i = 0; i < gene_panel.length; i++) {
    if(gene_panel[i].id != id){
      gene_panel_tmp.push(gene_panel[i]);
    }
  }
}
</script>
<script>
function like(link, id){
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessLink);
  requestX.open("post", link);
  if(document.getElementById("test_"+id).innerHTML != "Cheers!"){
    document.getElementById("test_"+id).innerHTML = "Cheers!";
    requestX.send(formdata);
  }
}

function ProcessLink(data){
  console.log(data.currentTarget.response);
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("like_count"+resp.id).innerHTML = parseInt(document.getElementById("like_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}

function comment(link, id){
  console.log(document.getElementById("comment_test"+id).value);
  var formdata = new FormData();
  formdata.append("_token", "{{ csrf_token() }}");
  formdata.append("comment", document.getElementById("comment_test"+id).value);
  var requestX = new  XMLHttpRequest();
  requestX.addEventListener('load', ProcessResponse);
  requestX.open("post", link);
  requestX.send(formdata);
  document.getElementById("comment_box_test"+id).innerHTML = document.getElementById("comment_box_test"+id).innerHTML + "<div class='comment-text'><span class='username'>Me: <span class='text-muted pull-right'>just now</span></span>"+document.getElementById("comment_test"+id).value+"</div>";
}

function ProcessResponse(data){
  var resp = JSON.parse(data.currentTarget.response);
  if(resp.success>0){
    document.getElementById("comment_count"+resp.id).innerHTML = parseInt(document.getElementById("comment_count"+resp.id).innerHTML) + 1;
  }
  else{
    console.log(data.currentTarget.response);
  }
}
</script>
@stop
