
@extends('layouts.master')

@section('title', 'DNA extraction method')

@section('content')
<div class="content-section-a">
  <div class="container">
    <h2>DNA extraction protocol</h2>
    <p>(method id: {{$extraction->id}})</p>
    <p class="lead">
      {!!$extraction->protocol!!}
    </p>
  </div>
</div>

@endsection

@section('js')

@endsection
