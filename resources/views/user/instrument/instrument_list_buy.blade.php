<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Available instrument</h4>
    </div>
    <div class="modal-body">
      <table class="table">
      <tr>
        <td>
          <p class=""><strong>Id</strong></p>
        </td>
        <td>
          <p class=""><strong>Address</strong></p>
        </td>
        <td>
          <p class=""><strong>Distance</strong></p>
        </td>
        <td>
          <p class=""><strong>Approx. £</strong></p>
        </td>
        <td>
          <p class=""><strong>View</strong></p>
        </td>
        <td>
          <p class=""><strong>Added</strong></p>
        </td>
        <td>
          <p class=""><strong>Book</strong></p>
        </td>
      </tr>
      @foreach($instruments as $instrument)
      <tr>
        <div style="Display:none" id="id_instrument_{{$instrument->id}}">{{$instrument->id}}</div>
        <td>
          <p class="">{{$instrument->id}}</p>
        </td>
        <td>
          <p class="">{{$instrument->address}}</p>
        </td>
        <td>
          <p class="">{{$instrument->distance}}</p>
        </td>
        <td>
          <p class="" id="price_instrument_{{$instrument->id}}">{{App\Models\Price::where('item', 'booking_day')->first()->price_pounds}}£/day + shipping</p>
        </td>
        <td>
          <a href="/instrument/View/{{$instrument->id}}" target="_blank">View</a>
        </td>
        <td>
          <p class="">{{$instrument->created_at->diffForHumans()}}</p>
        </td>
        <td>
          <button class="btn btn-sm btn-success btn-block register-btn" onclick="select_instrument({{$instrument->id}})">Select</button>
        </td>
      </tr>
      @endforeach
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
