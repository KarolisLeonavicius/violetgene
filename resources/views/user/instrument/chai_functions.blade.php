var temp_data = [];
var fluor_data = [];
var melt_data = [];

var auth_token = null;
var started = 0;
var time_started = null;
var current_exp_nr = 0;

var update_multiplier = 3;
var update_multiplier_tmp = 10;

var StatusDiv = document.getElementById("status_update");
var OnlineDiv = document.getElementById("online_update");
var TempDiv = document.getElementById("temp_update");
var ProgressDiv = document.getElementById("progress_update");
var StatusDivMin = document.getElementById("status_update_min");
var OnlineDivMin = document.getElementById("online_update_min");
var TempDivMin = document.getElementById("temp_update_min");
var ProgressDivMin = document.getElementById("progress_update_min");

window.onload = function(){
  check_status();
};

function instrument_connect(call = null){
  var fd = new FormData();
  var requestX = new  XMLHttpRequest();
  fd.append('email', "{{$instrument->username}}");
  fd.append('password', "{{$instrument->password}}");
  requestX.addEventListener('load', function (data){
    //console.log(data.currentTarget.response);
    auth_token = JSON.parse(data.currentTarget.response)["authentication_token"];
    if(call != null){call();}
  });
  requestX.open("post", "http://{{$instrument->ip}}/login");
  requestX.send(fd);
}

function instrument_status(){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
            clear_status();
          }
          else {
            //console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
  // console.log(JSON.parse(data.currentTarget.response));
  if(requestX.status === 200)
    process_status(JSON.parse(data.currentTarget.response))

  });
  requestX.timeout = 3000; // sets timeout to 3 seconds
  requestX.open("get", "http://{{$instrument->ip}}:8000/status?access_token="+auth_token);
  requestX.send();
}

function stop_instrument(){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
            clear_status();
          }
          else {
            //console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
  //console.log(JSON.parse(data.currentTarget.response));

  });
  requestX.timeout = 3000; // sets timeout to 3 seconds
  requestX.open("post", "http://{{$instrument->ip}}:8000/control/stop?access_token="+auth_token);
  requestX.send();
}

function create_experiment(id){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(create_experiment);}
        else {//console.log(requestX.status); //otherwise, some other code was returned}
        }
      }
  }
  requestX.addEventListener('load', function (data){
    //console.log(JSON.parse(data.currentTarget.response));
    });

  var params = JSON.stringify({'experiment_id': parseInt(id)});
  requestX.open("POST", "http://{{$instrument->ip}}:8000/control/start?access_token="+auth_token);
  requestX.send(params);
}

function stop_run() {
  started = 0;
  console.log("Stopping the instrument");
  stop_instrument();
}

function start_run(id) {
  console.log("Starting experiment"+id);
  time_started = new Date().getTime();
  started = 1;
  clone_experiment(id, create_experiment);
}

function dna_extraction(id) {
  console.log("Starting DNA extraction sequence"+id);
  time_started = new Date().getTime();
  started = 0;
  clone_experiment(id, create_experiment);
}

function check_status() {
  instrument_status();
  // get_temp_data(5);
  setTimeout(check_status, 2000);//Recursive status updates

  if(update_multiplier_tmp<update_multiplier)
    update_multiplier_tmp+=1;
  else{
      setTimeout(get_fluor_data(current_exp_nr), 2000);//Recursive status updates
      update_multiplier_tmp=0;
  }
}

function upload_data(){
  //clone_experiment(5);
}

function clear_status(){
  ProgressDiv.innerHTML = "--------";
  TempDiv.innerHTML = "--------";
  OnlineDiv.innerHTML = "Offline";
  StatusDiv.innerHTML = "--------";
  ProgressDivMin.innerHTML = "--------";
  TempDivMin.innerHTML = "--------";
  OnlineDivMin.innerHTML = "--------";
  StatusDivMin.innerHTML = "--------";
}

function process_status(status){
  //console.log(status.heat_block)
  StatusDiv.innerHTML = status.experiment_controller.machine.state
  StatusDivMin.innerHTML = "Heating block status: "+status.experiment_controller.machine.thermal_state;


  TempDiv.innerHTML = Math.round(status.heat_block.temperature)+" ["+Math.round((parseInt(status.heat_block.zone1.target_temperature)+parseInt(status.heat_block.zone2.target_temperature))/2)+"] C";
  TempDivMin.innerHTML = "Lid temperature : "+Math.round(status.lid.temperature)+" ["+Math.round(status.lid.target_temperature)+"] C";
  if(null != status.experiment_controller.experiment){
    current_exp_nr = status.experiment_controller.experiment.id;
    started = 1;

    if(status.experiment_controller.experiment.estimated_duration !== NaN){
      ProgressDiv.innerHTML = "Rem: " + Math.round((status.experiment_controller.experiment.estimated_duration-status.experiment_controller.experiment.run_duration)/60)+"min";
      ProgressDivMin.innerHTML = "Total duration: "+Math.round(status.experiment_controller.experiment.estimated_duration/60)+"min";
    }
    else{
      ProgressDiv.innerHTML = "Init.";
      ProgressDivMin.innerHTML = "Instrument initializing";
    }

    OnlineDiv.innerHTML = "Active";
    OnlineDivMin.innerHTML = "Elapsed: "+Math.round(status.experiment_controller.experiment.run_duration/60)+"min";

  }
    else{
      ProgressDiv.innerHTML = status.experiment_controller.machine.state;
      ProgressDivMin.innerHTML = "Lid open: "+status.optics.lid_open;

      OnlineDiv.innerHTML = "Online";
      OnlineDivMin.innerHTML = "---";
    }
  if(started){
    //console.log("started");
  }
}

function get_temp_data(id){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
          }
          else {
            // console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
  if(requestX.status === 200){
    temp_data = JSON.parse(data.currentTarget.response);
    //console.log(data.currentTarget.response);
    get_fluor_data(id);
  }
  else{
    //console.log(data.currentTarget.response);
  }
  });
  requestX.timeout = 20000; // sets timeout to 3 seconds
  requestX.open("get", "http://{{$instrument->ip}}/experiments/"+id.toString()+"/temperature_data?starttime=0&access_token="+auth_token, true);
  requestX.send();
}

function get_fluor_data(id){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
          }
          else {
            //console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
  if(requestX.status === 200){
    fluor_data = JSON.parse(data.currentTarget.response);
    //console.log(fluor_data);
    if(fluor_data.total_cycles>0){
      get_melt_data(id);
    }
  }
  else{
    //console.log(data.currentTarget.response);
  }
  });
  requestX.timeout = 10000; // sets timeout to 3 seconds
  requestX.open("get", "http://{{$instrument->ip}}/experiments/"+id.toString()+"/amplification_data?raw=false&background=true&baseline=true&cq=true&access_token="+auth_token, true);
  requestX.send();
}

function get_melt_data(id){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
            clear_status();
          }
          else {
            //console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
  if(requestX.status === 200){
    melt_data = JSON.parse(data.currentTarget.response);
    if(melt_data.total_cycles>0){
      //console.log(melt_data);
    }
  }
  else{
    //console.log(data.currentTarget.response);
  }

    //Update the server with run data
      var fd = new FormData();
      var reqx = new  XMLHttpRequest();
      fd.append('_token', "{{csrf_token()}}");

      if(melt_data != null)
        fd.append('melt_data', JSON.stringify(melt_data));

      if(fluor_data != null)
        fd.append('fluor_data', JSON.stringify(fluor_data));

      if(temp_data != null)
        fd.append('temp_data', JSON.stringify(temp_data));

      fd.append('instrument', "{{$instrument->id}}");
      fd.append('user_id', "{{$user->id}}");
      fd.append('test_id', "{{$test->id}}");
      reqx.addEventListener('load', function (data){
        console.log(data.currentTarget.response);
      });
      reqx.open("post", "{{env('UPLOAD_SERVER')}}");
      reqx.send(fd);
    //end of async ajax call
  });
  requestX.timeout = 3000; // sets timeout to 3 seconds
  requestX.open("get", "http://{{$instrument->ip}}/experiments/"+id.toString()+"/melt_curve_data?raw=false&normalized=true&derivative=true&tm=true&access_token="+auth_token, true);
  requestX.send();
}

function clone_experiment(id, call){
  var requestX = new  XMLHttpRequest();
  requestX.onreadystatechange=function() {
    if (requestX.readyState === 4){   //if complete
      if(requestX.status === 401){instrument_connect(instrument_status);}
        else{
          if(requestX.status === 0){
            //console.log(requestX.status);
            clear_status();
          }
          else {
            //console.log(requestX.status); //otherwise, some other code was returned}
          }
        }
      }
  }
  requestX.addEventListener('load', function (data){
    if(requestX.status === 200){
      if(null != JSON.parse(data.currentTarget.response).experiment){
        current_exp_nr = JSON.parse(data.currentTarget.response).experiment.id;
        call(current_exp_nr);
      }
      //console.log(JSON.parse(data.currentTarget.response));
    }
  });

  var fd = new FormData();
  fd.append('authentication_token', auth_token);
  fd.append('access_token', auth_token);
  fd.append('email', "{{$instrument->username}}");
  fd.append('password', "{{$instrument->password}}");
  //var params = JSON.stringify({'authentication_token': auth_token, 'email': "{{$instrument->username}}", 'password':"{{$instrument->password}}"});
  requestX.timeout = 10000; // sets timeout to 3 seconds
  requestX.open("post", "http://{{$instrument->ip}}/experiments/"+id.toString()+"/copy?access_token="+auth_token, true);
  // requestX.setRequestHeader('Authorization', 'authentication_token : '+auth_token);
  requestX.setRequestHeader('Content-Type', 'text/plain');
  requestX.send();
}
