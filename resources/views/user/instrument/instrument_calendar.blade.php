<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Instrument booking</h4>
    </div>
    <div class="modal-body">
      <div id="calendar">
      </div>
      <div id="script_execute">
        <script>
            function calendar_render(){
                $('#calendar').fullCalendar({!! $calendar->script()->options !!});
            };
        </script>
      </div>

    </div>
    <div class="modal-footer">
      <button type="button" onclick="book()" class="btn btn-success" data-dismiss="modal">Book</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
