
<div class="row">
  <div class="col-lg-12">

    <div class="col-sm-3">
      <button class="btn btn-app btn-primary" onclick='upload_data()'>
        <i class="fa fa-refresh"></i> Check PCR
      </button>
    </div>

    <div class="col-sm-3">
      <button class="btn btn-app btn-primary" onclick='dna_extraction({{$instrument->extraction_id}})'>
        <i class="fa fa-scissors"></i> DNA extraction
      </button>
    </div>


    <div class="col-sm-3">
      <button class="btn btn-app" onclick='start_run({{$instrument->run_id}})'>
        <i class="fa fa-play"></i> Start PCR
      </button>
    </div>

    <div class="col-sm-3">
      <button class="btn btn-app" onclick='stop_run()'>
        <i class="fa fa-square"></i> Stop test
      </button>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><div id="online_update">--</div></h3>

        <p>Instrument (QPCR)</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="#" class="small-box-footer"><div id="online_update_min">--</div></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3><div id="status_update">--</div></h3>

        <p>Thermocycler status</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="#" class="small-box-footer"><div id="status_update_min">--</div></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><div id="temp_update">--</div></h3>

        <p>Sample temperature</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" class="small-box-footer"><div id="temp_update_min">--</div></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3><div id="progress_update">--</div></h3>

        <p>Progress</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="#" class="small-box-footer"><div id="progress_update_min">--</div></a>
    </div>
  </div>
  <!-- ./col -->
</div>
