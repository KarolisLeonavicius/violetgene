{{-- resources/views/admin/dashboard.blade.php --}}

@php
  $school = $user->school()->first();
  $admin = $user->admin()->first();
@endphp

@extends('adminlte::page')

@section('title', 'Genetics education')

@section('content_header')
    <h1>Learn about genetics and testing</h1>
@stop

@section('content')
<div class="row">

  <div class="col-lg-6">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#flash" data-toggle="tab">Flash cards</a></li>
        <li><a href="#exam" data-toggle="tab">Genetics exercises</a></li>
        <li><a href="#realTopic" data-toggle="tab">Real world questions</a></li>
      </ul>
      <div class="tab-content">
        <!-- /.tab-pane -->
        <div class="active tab-pane" id="flash">
          <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Flash Cards</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="FlashWindow">
              Loading a flash card...
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="exam">
          <div id="ExerciseWindow">
            <!-- Filled by ajax -->
          </div>
        </div>
        <!-- /.tab-pane -->
        <div class="active tab-pane" id="realTopic">
          <!--Populated by JS-->
        </div>
      </div>
      <!-- /.tab-content -->
    </div>
  </div>
  <!-- /.col-lg6 -->

{!!$starting_topics!!}

</div>
<!-- /.row -->

<!--/Answers modal dialog box-->

<!--/.Answers modal dialog box-->

<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="Answers">

</div>

<div class="modal fade" id="FlashModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Genetics Flash Cards</h4>
      </div>
      <div class="modal-body" id=FlashWindow2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="TopicTree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
</div>

@stop

@section('js')

    <script>

    var TopicWindow = document.getElementById("realTopic");
    var AnswerWindow = document.getElementById("answerModal");
    var FlashWindow = document.getElementById("FlashWindow");
    var FlashWindow2 = document.getElementById("FlashWindow2");
    var Ans1 = document.getElementById("Ans1");
    var Ans2 = document.getElementById("Ans2");

    var TopicTree = document.getElementById("TopicTree");
    TopicTree.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';

    var TopicTreeButtonA = document.getElementById("fetchTopicTreeButtonA");
    var fullscreenButtonA = document.getElementById("fullscreenTopicButtonA");

    var GeneticsContent = document.getElementById("GeneticsTopic");
    var ExerciseContent = document.getElementById("ExerciseWindow");
    ExerciseContent.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';

    TopicWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    FlashWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    FlashWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    var request = new  XMLHttpRequest();
    window.onload = function () {
      fetchTopic();
      $('#FlashModal').modal({show: false}) //Off by default
      $('#answerModal').modal({show: false}) //Off by default
      $('#TopicTree').modal({show: false}) //Off by default
      fetchFlash();
      fetchExercise("9");
      // lookup_button.addEventListener('click', LookupCodes, false);
    }

    function Ans_One(){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("flash_id", document.getElementById('flash_id').innerHTML);
      formdata.append("answer", 1);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessFlashAnswer);
      requestX.open("post", "/topic/submitFlash");
      requestX.send(formdata);
      FlashWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
      FlashWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    }

    function Ans_Two(){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("flash_id", document.getElementById('flash_id').innerHTML);
      formdata.append("answer", 2);
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessFlashAnswer);
      requestX.open("post", "/topic/submitFlash");
      requestX.send(formdata);
      FlashWindow.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
      FlashWindow2.innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    }

    function fetchFlash(){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessFlash);
      requestX.open("get", "/topic/fetchFlash");
      requestX.send();
    }

    function ProcessFlashAnswer(data){
      // console.log(data.currentTarget.response);
      // console.log(JSON.parse(data.currentTarget.response));
      fetchFlash();
      $('#FlashModal').modal('show');
    }

    function ProcessFlash(data){
      // console.log(data.currentTarget.response);
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        FlashWindow.innerHTML = "";
        FlashWindow.innerHTML = resp.html;
        FlashWindow2.innerHTML = "";
        FlashWindow2.innerHTML = resp.html;
      }
      else{
        FlashWindow.innerHTML = "No flash cards found...";
        FlashWindow2.innerHTML = "No flash cards found...";
      }
    }

    function getAnswers(){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      var question_id = document.getElementById("question_id_div").innerHTML;
      requestX.addEventListener('load', ShowAnswers);
      requestX.open("get", "/topic/answer/BioStack/"+question_id);
      requestX.send();
    }

    function ShowAnswers(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        AnswerWindow.innerHTML = "";
        AnswerWindow.innerHTML = resp.html;
        $('#answerModal').modal('show');
      }
      // else{
      //   AnswerWindow.innerHTML = "No answers found on our site";
      // }
    }

    function fetchTopic(){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessTopic);
      requestX.open("get", "/topic/fetchTopic");
      requestX.send();
    }

    function ProcessTopic(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        TopicWindow.innerHTML = "";
        TopicWindow.innerHTML = resp.html;
      }
      else{
        TopicWindow.innerHTML = "No questions found on our site";
      }
    }

    function fetchTopicTree(id){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessTopicTree);
      requestX.open("get", "/topic/getTopicTree/"+id);
      requestX.send();
      TopicTreeButtonA.innerHTML = '<i class="fa fa-refresh fa-spin"></i>';
    }

    function ProcessTopicTree(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        TopicTree.innerHTML = "";
        TopicTree.innerHTML = resp.html;

        $('#TopicTree').modal('show');
        $('.nav-tabs a[href="#current"]').tab('show');
        GeneticsContent.innerHTML = resp.content;
      }
      else{
        TopicTree.innerHTML = "No topics found on our site";
      }
      TopicTreeButtonA.innerHTML = '<i class="fa fa-search"></i>Search related';
    }

    function fetchGeneticsTopic(id){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessGeneticsTopic);
      requestX.open("get", "/topic/getTopic/"+id);
      requestX.send();
    }

    function ProcessGeneticsTopic(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        GeneticsContent.innerHTML = resp.content;
        $('.nav-tabs a[href="#current"]').tab('show');
      }
      else{
        GeneticsContent.innerHTML = "No questions found on our site";
      }
    }

    function fetchExercise(id){
      //Get a topic for the topic window
      var requestX = new  XMLHttpRequest();
      requestX.addEventListener('load', ProcessExercise);
      requestX.open("get", "/topic/getExercise/"+id);
      requestX.send();
    }

    function ProcessExercise(data){
      var resp = JSON.parse(data.currentTarget.response);
      if(resp.success>0){
        ExerciseContent.innerHTML = resp.content;
        $('.nav-tabs a[href="#exam"]').tab('show');
      }
      else{
        ExerciseContent.innerHTML = "No questions found on our site";
      }
    }

    function like(link, button){
      var formdata = new FormData();
      formdata.append("_token", "{{ csrf_token() }}");
      var requestX = new  XMLHttpRequest();
      // requestX.addEventListener('load', ProcessFlashAnswer);
      requestX.open("post", link);
      requestX.send(formdata);
      document.getElementById(button).innerHTML = "Cheers!";
    }

    </script>
@stop
