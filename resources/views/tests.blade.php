@extends('layouts.master')

@section('title', 'VioletGene')

@section('navigation')
  @include('layouts.navbar_mini')
@endsection


@section('content')
  <div class="content-section-a">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 col-sm-6">
                  <h2 class="section-heading">What would you like to test?</h2>
                  <form>
                    <label class="sr-only" for="inputHelpBlock">Input with help text</label>
                    <textarea type="text" id="inputHelpBlock" class="form-control" aria-describedby="helpBlock" rows="2" placeholder="e.g. I would like to figure out where my onions are from..."></textarea>
                    <span id="helpBlock" class="help-block">*Please log in, so we can also email you, when it becomes available.</span>
                    <button type="submit" class="btn btn-lg btn-success col-lg-5">Submit a suggestion</button>
                  </form>
                  <!-- <div class="col-lg-3 col-lg-offset-0 col-lg-push-0 col-sm-6">
                      <img class="img-responsive" src="img/Edu.png" alt="">
                  </div> -->
              </div>
          </div>
      </div>
      <!-- /.container -->
  </div>

  <div class="content-section-b">
      <div class="container">
          <div class="row">
            <div class="col-lg-2 col-sm-2">
                <img class="img-responsive" src="img/Tests.png" alt="">
            </div>
              <div class="col-lg-8 col-sm-push-0  col-sm-4">
                  <hr class="section-heading-spacer">
                  <div class="clearfix"></div>
                  <h2 class="section-heading">A list of published genetic tests</h2>
                  <p class="lead">
                    This contains the list of currently used genetic test designs.
                  </p>
              </div>
          </div>
          <hr class="section-heading-spacer">
          <div class="clearfix"></div>
          @if(isset($templates))
            @foreach($templates as $template)
              @include('partials.test',
              [
              $test=$template
              ])
            @endforeach
          @endif
      </div>
      <!-- /.container -->
  </div>
@endsection

@section('footer')
  @include('layouts.footer')
@endsection
