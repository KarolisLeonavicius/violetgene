<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              <ul class="list-inline">
                  <li>
                      <a href="/#about">Genetic testing</a>
                  </li>
                  <li>
                      <a href="/#education">Education</a>
                  </li>
                  <li>
                      <a href="/#method">How it works</a>
                  </li>
                  <li>
                      <a href="/#tests">Available tests</a>
                  </li>
                  <li>
                      <a href="/#pricing">Pricing</a>
                  </li>
                  <li>
                      <a href="/terms">Terms</a>
                  </li>
                  <li>
                      <a href="/about">About</a>
                  </li>
                  @if(!Auth::check())
                    <li>
                        <a href="/login">Login</a>
                    </li>
                    <li>
                        <a href="/register">Register</a>
                    </li>
                  @else
                  <li>
                      <a href="/user/home">My account</a>
                  </li>
                  @endif
              </ul>
                <p class="copyright text-muted small">Copyright &copy; Oxford University 2017. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
