<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" media="print" href="/css/bootstrap.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- Custom CSS -->
    <link href="/css/landing-page.css" rel="stylesheet" media="all">

    <!-- Custom Fonts -->
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    @yield('head')

</head>

<body>
    @yield('navigation')
    <!-- Navigation -->

    @yield('header')
    <!-- /.intro-header -->

    <!-- Page Content -->
    @yield('content')

    <!-- Footer -->
    @yield('footer')

    <!-- jQuery -->
    <script src="/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>

    <!-- JavaScript -->
    @yield('js')

</body>

</html>
