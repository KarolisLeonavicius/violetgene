<!-- Header -->
<div class="intro-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-message">
                    <h1>@yield('title')</h1>
                    <h3>DNA testing simplified</h3>
                    <hr class="intro-divider">
                    <ul class="list-inline intro-social-buttons">
                      @if(!Auth::check())
                        <li>
                            <a href="/login" class="btn btn-default btn-lg btn-success"><i class="fa fa-user fa-fw"></i> <span class="network-name">Login</span></a>
                        </li>
                        <li>
                            <a href="/register" class="btn btn-default btn-lg btn-success"><i class="fa fa-plus fa-fw"></i> <span class="network-name">Register</span></a>
                        </li>
                      @else
                        <li>
                            <a href="/user/home" class="btn btn-default btn-lg btn-success"><i class="fa fa-user fa-fw"></i> <span class="network-name">My account</span></a>
                        </li>
                      @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>
