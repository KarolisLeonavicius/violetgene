var targetPage = "http://192.168.7.2/*";

chrome.webRequest.onHeadersReceived.addListener(function(details) {
  var bkg = chrome.extension.getBackgroundPage();
  bkg.console.log("onHeaders received: ");
  bkg.console.log(details);

  var url = new URL(details.url);
  bkg.console.log(url);

  // set_or_add(details, 'Access-Control-Allow-Origin', url.protocol+"//"+url.hostname);'http://violetgene.org'
  set_or_add(details, 'Access-Control-Allow-Origin', 'http://violetgene.org');
  set_or_add(details, 'Access-Control-Allow-Headers', 'authorization, content-type, Token');
  set_or_add(details, 'Access-Control-Allow-Credentials', 'true');
  set_or_add(details, 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS');

     if(details.method == "OPTIONS"){
       details.statusCode = 200;
       details.statusLine= "HTTP/1.1 200 OK"
       bkg.console.log("Added OK to OPTIONS request");
      return { responseHeaders: details.responseHeaders, statusCode : 200 };
     }
      else {
        return { responseHeaders: details.responseHeaders};
      }
}, {
    urls: [targetPage]
}, [
  'blocking',
  'responseHeaders'
]);

function set_or_add(details, header, value){
  contains_header = false;
  for(var i = 0; i < details.responseHeaders.length; ++i)
  {
    if(details.responseHeaders[i].name.toLowerCase() == header.toLowerCase()){
      contains_header = true;
      details.responseHeaders[i].value = value;
    }
  }
  if(!contains_header){
    details.responseHeaders.push({
        name: header,
        value: value
    });
  }
}


function getParameterByName(name, url) {

    if (!url) url = window.location.href;

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var requestFilter = {
    urls: [targetPage]
},

extraInfoSpec = ['requestHeaders', 'blocking'],

handler = function(details) {

var bkg = chrome.extension.getBackgroundPage();
bkg.console.log("onBeforeHeaders sent: ");
bkg.console.log(details);

var isRefererSet = false;
var headers = details.requestHeaders,
    blockingResponse = {};

if(details.method == "POST" || details.method == "GET"){

  // details.responseHeaders.push({
  //     name: 'Access-Control-Allow-Headers',
  //     value: 'authorization, content-type, Token'
  // });
  //
  // details.responseHeaders.push({
  //     name: 'Access-Control-Allow-Credentials',
  //     value: 'true'
  // });
  //
  // details.responseHeaders.push({
  //     name: 'Access-Control-Allow-Methods',
  //     value: 'GET, POST, OPTIONS'
  // });
  //
  // details.responseHeaders.push({
  //     name: 'Access-Control-Allow-Origin',
  //     value: 'http://violetgene.org'
  // });


  for (var i = 0, l = headers.length; i < l; ++i) {
      if (headers[i].name == "Authorization") {
          headers[i].value = "Token: "+getParameterByName('access_token', details.url);
          isRefererSet = true;
          break;
      }
  }

  if (!isRefererSet) {
      headers.push({
          name: "Authorization",
          value: getParameterByName('access_token', details.url)
      });
  }
}

blockingResponse.requestHeaders = headers;
return blockingResponse;
};

chrome.webRequest.onBeforeSendHeaders.addListener(handler, requestFilter, extraInfoSpec);

// [
//     {
//         "URL": "http://192.168.7.2/*",
//         "headers": [
//             {
//                 "name": "Access-Control-Allow-Headers",
//                 "value": "authorization, content-type, Token"
//             },
//             {
//                 "name": "Access-Control-Allow-Credentials",
//                 "value": "true"
//             },
//             {
//                 "name": "Access-Control-Allow-Methods",
//                 "value": "GET, POST, OPTIONS"
//             },
//             {
//                 "name": "Access-Control-Allow-Origin",
//                 "value": "http://violetgene.org"
//             }
//         ]
//     },
//     {
//         "URL": "http://violetgene.org/*",
//         "headers": [
//             {
//                 "name": "Access-Control-Allow-Headers",
//                 "value": "authorization, content-type, Token"
//             },
//             {
//                 "name": "Access-Control-Allow-Credentials",
//                 "value": "true"
//             },
//             {
//                 "name": "Access-Control-Allow-Methods",
//                 "value": "GET, POST, OPTIONS"
//             },
//             {
//                 "name": "Access-Control-Allow-Origin",
//                 "value": "http://website.dev"
//             }
//         ]
//     }
// ]
