<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Jobs\UpdateBioStackExchange;
use App\Models\Online\BioStackExchangeQuestion as BioStack;
use App\Models\Online\BioStackExchangeAnswer as BioStackAnswer;

use App\Models\Online\FlashCardQuestion as Flash;
use App\Models\Online\FlashCardAnswer as FlashAnswer;

class TopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the landing page for non regirstered users
     *
     * @return \Illuminate\Http\Response
     */

     /*
     Biology Stack Exchange handling fuctions
     */
     public function BioStackAnswer($question_id){
       /*This function will look for answers in a database and if not found, will fetch from stack exchange*/
       $question = BioStack::where('question_id', $question_id)->first();
       $answers = $question->answers();

       #If there are no answers in the database, fetch them from the stackexchange
       if(!$answers->exists()){
         $client = new \GuzzleHttp\Client();
         $res = $client->request('GET', "http://api.stackexchange.com/2.2/questions/$question_id/answers?site=biology&pagesize=100&page=1&filter=withbody");
         if($res->getStatusCode() == 200){#Check, that the API returns correct responses
           $search_items = json_decode($res->getBody(), false)->items;
           for ($i=0; $i < sizeof($search_items); $i++) {
             $answer_object = $search_items[$i];
             $answer_array = (array) $answer_object;
             $answer_array['owner'] = json_encode($answer_object->owner);
             $answer = BioStackAnswer::where('answer_id', $answer_array['answer_id']);

             #Check if the answer returned by search does not exist
             if(!$answer->exists()){#We still need this check in case new questions have been posted
               BioStackAnswer::create($answer_array);#Create a new cached question in the database
             }
             #Update the answer, if it has been edited recently
             elseif($answer->first()->last_activity_date < $$answer_array['last_activity_date']){#Check if the question has been updated
               $answer = new BioStackAnswer($answer_array);
               $answer->save();
             }
           }#End of for loop for iterating answers
         }
         else{
           return response()->json(json_encode(array('success' => false, 'html'=>"<p>Error while processing Biology Stack Exchange Request</p>")));
         }
       }

       #Get possibly updated answers collection for passing it to the popup view downstream
       $answers = $question->answers();
       if($answers->exists()){
         $returnHTML = view('user.includes.answers')
                            ->with('topic', $question)
                            ->with('answers', $answers->get()->toArray()
                            )->render();
         return response()->json(array('success' => true,'html'=>$returnHTML));
       }
       else{
         $returnHTML = view('user.includes.answers')
                            ->with('topic', $question)
                            ->with('answers', array(['body'=>"The question has not been answered yet. Be the first on: Biology Stack Exchange."]))
                            ->render();
         return response()->json(array('success' => true, 'html'=>$returnHTML));
       }
     }


     public function fetchTopic(){
       $topic = BioStack::inRandomOrder()->first();
       if(!empty($topic)){
         $returnHTML = view('user.includes.topic')->with('topic', $topic)->render();
         return response()->json(array('success' => true, 'html'=>$returnHTML));
       }
       else{
         return response()->json(array('success' => false));
       }
     }

     public function getBioStackTopics(){
       $client = new \GuzzleHttp\Client();
       $res = $client->request('GET', "http://api.stackexchange.com/2.2/search?site=biology&pagesize=10&page=1&tagged={genetics}&filter=withbody");
       // echo $res->getHeaderLine('content-type');
       // 'application/json; charset=utf8'
       $body = $res->getBody();
       if($res->getStatusCode() == 200){#Check, that the API returns correct responses
         dispatch(new UpdateBioStackExchange());
         return view('user.testing')->with('response', ['status'=>"Starting job"]);
       }
       else{
         return view('user.testing')->with('response', ['status'=>"Error connecting to the Bio Stack Exchange API".strval($res->getStatusCode())]);
       }
    }

    public function refreshBioStackTopics(){
      $client = new \GuzzleHttp\Client();
      $res = $client->request('GET', "http://api.stackexchange.com/2.2/search?site=biology&pagesize=100&page=1&sort=activity&tagged={genetics}&filter=withbody");
      // echo $res->getHeaderLine('content-type');
      // 'application/json; charset=utf8'

      if($res->getStatusCode() == 200){#Check, that the API returns correct responses
        $search_items = json_decode($res->getBody(), false)->items;
        for ($i=0; $i < sizeof($search_items); $i++) {
          $question_object = $search_items[$i];

          $question_array = (array) $question_object;
          $question_array['tags'] = json_encode($question_object->tags);
          $question_array['owner'] = json_encode($question_object->owner);
          $question = BioStack::where('question_id', $question_array['question_id']);
          #Check if the question returned by search does not exist
          if(!$question->exists()){
            BioStack::create($question_array);#Create a new cached question in the database
          }
          elseif($question->first()->last_activity_date < $question_array['last_activity_date']){#Check if the question has been updated
            $question = new BioStack($question_array);
            $quesion->save();
          }
        }

        return view('user.testing')->with('response', $question_array);
      }
      else{
        return view('user.testing')->with('response', ['status'=>"Error connecting to the Bio Stack Exchange API".strval($res->getStatusCode())]);
      }
   }



   /*
   Flash card handling fuctions
   */
   public function Import_flashcards(Request $request){
     // Handle the user upload of avatar
     if($request->hasFile('file_input')){
       $csv_file = $request->file('file_input');
       $handle = fopen($csv_file, 'r');
       $column_headers = array();
       $row_count = 0;
       while (($data = fgetcsv($handle, 10000, ",")) !== FALSE)
       {
         if ($row_count==-1)
         {
           $column_headers = $data;
         }
         else
         {
           $card = Flash::create(array(
                          "level"=>0, "created_by"=>$request->user()->id, "rating"=>0, "answer_count"=>0,
                          "body"=>$data[1], "title"=>$data[0], "answer1"=>$data[2], "answer2"=>$data[3],
                          "correct"=>$data[4]
                        ));
         }
         ++$row_count;
       }
     }
     return view('user/import_flash', ['user' => Auth::user(), 'status'=>"$row_count flash cards imported."]);
   }

   public function Show_import(){
     return view('user/import_flash', ['user' => Auth::user()]);
   }

   /* Grabs a completely random flash card from the list */
   public function fetchFlash(){
     /* User level matching not yet implemented */
     $flash = Flash::inRandomOrder()->first();
     if(!empty($flash)){
       $returnHTML = view('user.includes.flash')->with('flash', $flash)->render();
       return response()->json(array('success' => true, 'html'=>$returnHTML));
     }
     else{
       return response()->json(array('success' => false));
     }
    }

    /* Grabs all cards created by the user and returns a modal window */
    public function getFlashList(){
      /* User level matching not yet implemented */
      $flash = Flash::where('created_by', Auth::user()->id)->get();
      if(!empty($flash)){
        $returnHTML = view('user.includes.flash_list')->with('flash_cards', $flash)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
      }
      else{
        return response()->json(array('success' => false));
      }
     }

     /* Grabs a particular card for editing */
     public function getFlashCard($card_id){
       /* User level matching not yet implemented */
       $user = Auth::user();
       $flash = Flash::where([['created_by', '=', $user->id],['id', '=', $card_id]])->first();
       if($flash->correct==1){
         $flash['correctAns'] = $flash->answer1;
         $flash['incorrectAns'] = $flash->answer2;
       }
       else{
         $flash['correctAns'] = $flash->answer2;
         $flash['incorrectAns'] = $flash->answer1;
       }
         $returnHTML = view('user.includes.flash_edit')->with('flash_card', $flash)->render();
         return view("user.education.flash")->with('user', $user)->with('flash_card', $returnHTML);
      }

    /* Logs a user's answer */
    public function submitFlash(Request $request){
      /* User and flash card score changing not implemented yet */

      $actual = Flash::where('id', $request->flash_id)->first();

      $user = $request->user();
      $outcome = false;
      if($actual->correct == $request->answer){
        $outcome = true;
      }
      $expected_score_user = 1.0 / (1.0 + pow(10.0, ($actual->rating - $user->flash_score)/400.0)); //Use a chess style scoring mechanism
      // $expected_score_card = 1 / (1 + 10^(($user->flash_score - $actual->rating)/400)) //Use a chess style scoring mechanism

      if($outcome){
        $actual->rating -= (1-$expected_score_user) * 32; //Use a chess style scoring mechanism
        $user->flash_score += (1-$expected_score_user) * 32; //Use a chess style scoring mechanism
        $change = (1-$expected_score_user);
      }
      else{
        $actual->rating += $expected_score_user * 32; //Use a chess style scoring mechanism
        $user->flash_score -= $expected_score_user * 32; //Use a chess style scoring mechanism
        $change = ($expected_score_user);
      }

      $actual->answer_count += 1;
      $actual->save();
      $user->save();

      FlashAnswer::Create(array(
                    "flash_id"=>$request->flash_id,
                    "user_id"=>$user->id,
                    "outcome"=>$outcome,
                    "rating_user"=>$user->flash_score,
                    "rating_flash"=>$actual->rating,
                    "rating_change"=>$change * 32));

      // $this->fetchFlash();
      return 1;
     }

  public function editFlashWindow(Request $request){
   $user = $request->user();
   /* Serves the window for flash card editing */
   return view("user.education.flash")->with('user', $user);
  }

  public function createFlash(Request $request){
    $rand_bool = rand(0,1) == 1;
    if($rand_bool){
      $ans1 = $request->correct;
      $ans2 = $request->incorrect;
      $correct = 1;
    }
    else {
      $ans2 = $request->correct;
      $ans1 = $request->incorrect;
      $correct = 2;
    }
    $user = $request->user();
    $card = Flash::create(array(
       "level"=>0, "created_by"=>$user->id, "rating"=>0, "answer_count"=>0,
       "body"=>$request->body, "title"=>"", "answer1"=>$ans1, "answer2"=>$ans2,
       "correct"=>$correct
     ));
    return view("user.education.flash")->with('user', $user)->with('status', 1);
  }

  /*Edit the clash card content and randomise answers again*/
  public function editFlash(Request $request, $flash_id){
    $user = Auth::user();
    $rand_bool = rand(0,1) == 1;
    if($rand_bool){
      $ans1 = $request->correct;
      $ans2 = $request->incorrect;
      $correct = 1;
    }
    else {
      $ans2 = $request->correct;
      $ans1 = $request->incorrect;
      $correct = 2;
    }
    $actual = Flash::where([['created_by', '=', $user->id],['id', '=', $flash_id]])->first();
    $actual->body = $request->body;
    $actual->answer1 = $ans1;
    $actual->answer2 = $ans2;
    $actual->correct = $correct;
    $actual->save();

    if($actual->correct==1){
      $actual['correctAns'] = $actual->answer1;
      $actual['incorrectAns'] = $actual->answer2;
    }
    else{
      $actual['correctAns'] = $actual->answer2;
      $actual['incorrectAns'] = $actual->answer1;
    }

    $returnHTML = view('user.includes.flash_edit')->with('flash_card', $actual)->render();
    return view("user.education.flash")->with('user', $user)
                                      ->with('flash_card', $returnHTML)
                                      ->with('status', 1);

  }

  /* Delete a flash card and return an updated list*/
  public function deleteFlash($flash_id){
    $user = Auth::user();
    $flash = Flash::where('created_by', '=', $user->id)->find($flash_id);
    $flash->delete();

    return view("user.education.flash")->with('user', $user)->with('status', 1);
  }

  /* Show the window for testing purposes*/
  public function test(Request $request){
   $user = $request->user();
   /* Serves the window for flash card editing */
   return view("user.education.index")->with('user', $user);
  }

}
