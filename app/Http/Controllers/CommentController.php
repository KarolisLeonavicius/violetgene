<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Like;
use Auth;

class CommentController extends Controller
{
    public function postTopic(Request $request, $id){
      $user = Auth::user();
      Post::create(array('user_id'=>$user->id, 'comment'=>$request['comment'], 'topic_id'=>$id));
      return response()->json(array('success' => true, 'html'=>""));
    }

    public function likeTopic(Request $request, $id){
      $user = Auth::user();
      Like::create(array('user_id'=>$user->id, 'topic_id'=>$id));
      return response()->json(array('success' => true, 'html'=>""));
    }

    public function postExercise(Request $request, $id){
      $user = Auth::user();
      Post::create(array('user_id'=>$user->id, 'comment'=>$request['comment'], 'exercise_id'=>$id));
      return response()->json(array('success' => true, 'html'=>""));
    }

    public function likeExercise(Request $request, $id){
      $user = Auth::user();
      Like::create(array('user_id'=>$user->id, 'exercise_id'=>$id));
      return response()->json(array('success' => true, 'html'=>""));
    }

    public function postTest(Request $request, $id){
      $user = Auth::user();
      Post::create(array('user_id'=>$user->id, 'comment'=>$request['comment'], 'test_id'=>$id));
      return response()->json(array('success' => true, 'html'=>"", 'id'=>$id));
    }

    public function likeTest(Request $request, $id){
      $user = Auth::user();
      Like::create(array('user_id'=>$user->id, 'test_id'=>$id));
      return response()->json(array('success' => true, 'html'=>"", 'id'=>$id));
    }
}
