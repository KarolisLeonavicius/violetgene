<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Exercise;
use App\Models\Post;
use App\Models\Like;
use App\Models\Topic;

use Auth;
use Response;

class ExerciseController extends Controller
{
  /*
  Exam question handling fuctions
  */

  /* Show the window for creating exam material*/
  public function newExerciseWindow(Request $request){
   $user = $request->user();
   $exercises = Exercise::where('user_id', $user->id)->get();
   $topics = Topic::where('approved', true)->get();

   return view("user.education.exercise", compact('user'))
                ->with('exercises', $exercises)
                ->with('topics', $topics);
  }

  /* Show the window for editing exam material*/
  public function editExerciseWindow(Request $request){
   $user = $request->user();
   $exercises = Exercise::where('user_id', $user->id)->get();
   $topics = Topic::where('approved', true)->get();
   $exercise = Exercise::find($request['id']);

   $content = Storage::disk('local')->get('exercises/'.$exercise->content);//This loads the content from disk
   /* Serves the window for exercise editing */
   return view("user.education.exercise", compact('user'))
                ->with('exercises', $exercises)
                ->with('exercise', $exercise)
                ->with('content', $content)
                ->with('topics', $topics);
  }

  /*Create a new exercise*/
  public function newExercise(Request $request){
   $user = $request->user();
   $exercises = Exercise::where('user_id', $user->id)->get();
   $topics = Topic::where('approved', true)->get();
   if($request['topic'] == "None"){
     $topic_id = null;
   }
   else{
     $topic_id = $request['topic'];
   }
   $exercise = new Exercise();
   $exercise->topic_id = $topic_id;
   $exercise->privacy = $request['privacy'];
   $exercise->title = $request['title'];
   $exercise->content = md5($request['title']).md5($user->id).'.html';//Store file name in the DB
   $exercise->user_id = $user->id;
   $exercise->save();
   Storage::disk('local')->put('exercises/'.md5($request['title']).md5($user->id).'.html', $request['content']);
   $content = Storage::disk('local')->get('exercises/'.$exercise->content);//This stores the content from disk
   /* Serves the window for exercise editing */
   return view("user.education.exercise", compact('user'))
                ->with('exercises', $exercises)
                ->with('exercise', $exercise)
                ->with('content', $content)
                ->with('topics', $topics);
  }

  /*Edit an exercise*/
  public function editExercise(Request $request, $id){
   $user = $request->user();
   $exercises = Exercise::where('user_id', $user->id)->get();
   $topics = Topic::where('approved', true)->get();

   if($request['topic'] == "None"){
     $topic_id = null;
   }
   else{
     $topic_id = $request['topic'];
   }
   $exercise = Exercise::find($id);
   $exercise->topic_id = $topic_id;
   $exercise->privacy = $request['privacy'];
   $exercise->title = $request['title'];
   $exercise->content = md5($request['title']).md5($user->id).'.html';//Store file name in the DB
   $exercise->user_id = $user->id;
   $exercise->save();
   Storage::disk('local')->put('exercises/'.md5($request['title']).md5($user->id).'.html', $request['content']);
   $content = Storage::disk('local')->get('exercises/'.$exercise->content);//This stores the content from disk

   /* Serves the window for exercise editing */
   return view("user.education.exercise", compact('user'))
                ->with('exercises', $exercises)
                ->with('exercise', $exercise)
                ->with('content', $content)
                ->with('topics', $topics);
  }


  /*
  Genetics topics handling fuctions
  */
  /* Show the window for Creating topic material*/
  public function newTopicWindow(Request $request){
   $user = $request->user();
   $topics = Topic::where('user_id', $user->id)->get();
   /* Serves the window for flash card editing */
   return view("user.education.topic")
                ->with('user', $user)
                ->with('topics', $topics);
  }

  /* Show the window for editing topic material*/
  public function editTopicWindow(Request $request){
   $user = $request->user();
   $topics = Topic::where('user_id', $user->id)->get();
   $topic = Topic::find($request['id']);
   /* Serves the window for flash card editing */

   $content = Storage::disk('local')->get('topics/'.$topic->content);//This stores the content from disk
   return view("user.education.topic")
                ->with('user', $user)
                ->with('topics', $topics)
                ->with('topic', $topic)
                ->with('content', $content);
  }


  /*Create a new topic*/
  public function editTopic(Request $request, $id){
   $user = $request->user();
   if($request['parent'] == "None"){
     $parent = null;
   }
   else{
     $parent = $request['parent'];
   }
   $topic = Topic::find($id);
   $topic->privacy = $request['privacy'];
   $topic->category = $request['category'];
   $topic->title = $request['title'];
   $topic->parent = $parent;
   $topic->content = md5($request['title']).md5($user->id).'.html';//Store file name in the DB
   $topic->user_id = $user->id;
   $topic->save();

   Storage::disk('local')->put('topics/'.$topic->content, $request['content']);//This stores the content from disk

   $content = Storage::disk('local')->get('topics/'.$topic->content);//This stores the content from disk
   /* Serves the window for exercise editing */
   $topics = Topic::where('user_id', $user->id)->get();
   return view("user.education.topic")
                ->with('user', $user)
                ->with('topics', $topics)
                ->with('topic', $topic)
                ->with('content', $content);
  }

  /*Create a new topic*/
  public function newTopic(Request $request){
   $user = $request->user();
   if($request['parent'] == "None"){
     $parent = null;
   }
   else{
     $parent = $request['parent'];
   }
   $topic = new Topic();
   $topic->privacy = $request['privacy'];
   $topic->category = $request['category'];
   $topic->title = $request['title'];
   $topic->parent = $parent;
   $topic->content = md5($request['title']).md5($user->id).'.html';//Store file name in the DB
   $topic->user_id = $user->id;
   $topic->save();

   Storage::disk('local')->put('topics/'.$topic->content, $request['content']);//This stores the content from disk

   $content = Storage::disk('local')->get('topics/'.$topic->content);//This stores the content from disk
   /* Serves the window for exercise editing */
   $topics = Topic::where('user_id', $user->id)->get();
   return view("user.education.topic")
                ->with('user', $user)
                ->with('topics', $topics)
                ->with('topic', $topic)
                ->with('content', $content);
  }

  /*Get topic explorer*/
  public function getTopicTree($id){
    $user = Auth::user();

    if($id == 0)
      $FirstTopic = Topic::where('approved', true)->inRandomOrder()->first();
    else
      $FirstTopic = Topic::find($id);

    /*Random first topic....*/
    $children = Topic::where('parent', $FirstTopic->id)->get();
    $parent = Topic::find($FirstTopic->parent);
    $content = Storage::disk('local')->get('topics/'.$FirstTopic->content);//This stores the content from disk
    $comments = Post::where('topic_id', $FirstTopic->id)->get();
    $likes = Like::where('topic_id', $FirstTopic->id)->get();
    $htmlcontent = view('user.includes.topic_container')
          ->with('user', $user)
          ->with('FirstTopic', $FirstTopic)
          ->with('content', $content)
          ->with('children', $children)
          ->with('comments', $comments)
          ->with('likes', $likes)
          ->render();
    $FirstTopic->htmlContent = $htmlcontent;

   $returnHTML = view('user.includes.topic_tree')
                       ->with('user', $user)
                       ->with('children', $children)
                       ->with('topic', $FirstTopic)
                       ->with('parent', $parent)
                       ->render();

   return response()->json(array('success' => true,'id'=>$id,'category' => $FirstTopic->category,'html'=>$returnHTML, 'content' => $htmlcontent));
   }


  /*Get topic html*/
  public function getTopic($id){
    $user = Auth::user();
    if($id == 0)
      $FirstTopic = Topic::where('approved', true)->inRandomOrder()->first();
    else
      $FirstTopic = Topic::find($id);

    $content = Storage::disk('local')->get('topics/'.$FirstTopic->content);//This stores the content from disk
    $comments = Post::where('exercise_id', $FirstTopic->id)->get();
    $likes = Like::where('exercise_id', $FirstTopic->id)->get();
    $returnHTML = view('user.includes.topic_container')
                        ->with('user', $user)
                        ->with('FirstTopic', $FirstTopic)
                        ->with('content', $content)
                        ->with('comments', $comments)
                        ->with('likes', $likes)
                        ->render();
   return response()->json(array('success' => true, 'category' => $FirstTopic->category, 'content'=>$returnHTML));
  }


  /*Get exercise html*/
  public function getExercise($id){
   $user = Auth::user();
   if($id == 0)
     $FirstExercise = Exercise::where('approved', true)->inRandomOrder()->first();
   else
     $FirstExercise = Exercise::find($id);
   if(!empty($FirstExercise)){
     $content = Storage::disk('local')->get('exercises/'.$FirstExercise->content);//This stores the content from disk
     $comments = Post::where('exercise_id', $FirstExercise->id)->get();
     $likes = Like::where('exercise_id', $FirstExercise->id)->get();
     $returnHTML = view('user.includes.exercise_container')
                         ->with('user', $user)
                         ->with('FirstExercise', $FirstExercise)
                         ->with('content', $content)
                         ->with('comments', $comments)
                         ->with('likes', $likes)
                         ->render();
     return response()->json(array('success' => true, 'content'=>$returnHTML));
   }
   else{
     return response()->json(array('success' => false));
   }
  }

}
