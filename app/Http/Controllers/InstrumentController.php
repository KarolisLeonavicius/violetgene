<?php

namespace App\Http\Controllers;

use \MaddHatter\LaravelFullcalendar\Event;
use Illuminate\Http\Request;
use \App\Models\Instrument as instrument;
use \App\Models\Booking as booking;
use \App\Models\Test as test;
use \App\Models\Price as price;
use Auth;
use Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class InstrumentController extends Controller
{

  /* Return Instrument list */
  public function getInstrumentList(){
    $user = Auth::user();
    $instruments = instrument::get();
    if(!empty($instruments)){
      foreach($instruments as $instrument)
      {
        $instrument->address = "None";
        $instrument->distance = "12km";
        $instrument->price = price::where('item', 'booking_day')->first()->price_pounds + 2 * price::where('item', 'booking_transport')->first()->price_pounds;
      }

      $returnHTML = view('user.instrument.instrument_list_buy')->with('instruments', $instruments)->with('user', $user)->render();
      return response()->json(array('success' => true, 'html'=>$returnHTML));
    }
    else{
      return response()->json(array('success' => false));
    }

  }

  public function View($id){
    $user = Auth::user();
    $instrument = instrument::where('id', $id)->get();
    return view('user.instrument.view')->with('instrument', $instrument);
  }

  public function getAvailability($id){
    // $user = Auth::user();
    // $instrument = instrument::find($id);
    $bookings = booking::where('instrument_id', $id)->get();
    $events = [];
    foreach($bookings as $booking){
      if($booking->paid)
        array_push($events, \Calendar::event(
            'Booked', //event title
            true, //full day event?
            \Carbon\Carbon::createFromTimestamp($booking->from)->toDateTimeString(), //start time (you can also use Carbon instead of DateTime)
            \Carbon\Carbon::createFromTimestamp($booking->to)->toDateTimeString(), //end time (you can also use Carbon instead of DateTime)
            1 //optionally, you can specify an event ID
        ));
      else
      array_push($events, \Calendar::event(
          'Reserved', //event title
          true, //full day event?
          \Carbon\Carbon::createFromTimestamp($booking->from)->toDateTimeString(), //start time (you can also use Carbon instead of DateTime)
          \Carbon\Carbon::createFromTimestamp($booking->to)->toDateTimeString(), //end time (you can also use Carbon instead of DateTime)
          0 //optionally, you can specify an event ID
      ));
    }

    $calendar = \Calendar::addEvents($events); //add an array with addEvents
    // $user = Auth::user();
    $returnHTML = view('user.instrument.instrument_calendar')->with('calendar', $calendar)->render();

    // $calendar->script()->options
    $opts = json_decode($calendar->script()->options);
    $evts = $opts->events;
    return response()->json(array('success' => true, 'html'=>$returnHTML, 'events'=>json_encode($evts)));
    // return view('user.instrument.instrument_calendar')->with('calendar', $calendar)->with('user', $user);
  }

  public function getBookings(){
    $user = Auth::user();
    $bookings = booking::where('user_id', $user->id)->get();

    $returnHTML = view('user.experiment.bookings_list')->with('bookings', $bookings)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function connectInstrument(Request $request){
    // if($request->instrument == 0){
    //   $request->instrument = instrument::where('type', 'Simulation')->first()->id;
    // }

    $user = Auth::user();
    $test_instance = test::where([
      'user_id'=>$user->id,
      'id'=>$request->test
    ])->first();
    if(isset($test_instance)){
      $test_instance->instrument_id = $request->instrument;
      $test_instance->save();
    }
    return response()->json(array('success' => true, 'html'=>""));
  }


  public function Simulator(Request $request){
    $user  = Auth::user();
    $test  = test::find($request->test);

    if(!Storage::disk('local')->exists('tests/runs/'.$test->id_form.'.csv')){
      $panel = $test->belongsTo('App\Models\GenePanel', 'panel_id')->first();
      $names = array();
      $CTs = array();

      foreach(json_decode($panel->genes) as $gene){

        array_push($names, $gene->name);
        if($gene->type == "Positive Control") {
          $items = range(18, 22);
          array_push($CTs, $items[array_rand($items)]);
        }
        else {
          if($gene->type == "Negative Control") {
          $items = range(40, 46);
          array_push($CTs, $items[array_rand($items)]);
          }
          else{
            $items = range(18, 40);;
            array_push($CTs, $items[array_rand($items)]);
          }
        }
      }
      $this->create_simulation('tests/runs/'.$test->id_form.'.csv', $names, $CTs);
    }

    $content = Storage::disk('local')->get('tests/runs/'.$test->id_form.'.csv');

    $values = array_map('str_getcsv', str_getcsv($content,"\n"));
    $length = count($values);
    $data = array();
    for($i=1; $i<($length*($request->progress)/100); $i+=1) array_push($data, $values[$i]);

    if(isset($test)){
      if($test->user_id == $user->id){
        $save_data = $data;
        array_unshift($save_data, $values[0]);
        $test->raw_data = json_encode($save_data);
        $results = $this->analyze($save_data);
        $test->result=json_encode($results);
        $test->save();

        $rawHTML = view('user.includes.data_table')
                          ->with('data', $data)
                          ->with('titles', $values[0])
                          ->render();

        $resultHTML = view('user.includes.result_table')
                          ->with('results', $results)
                          ->render();
      }
    }
    else {
      //Test not found
      $rawHTML = "";
      $resultHTML = "";
    }

    event(new \App\Events\DataEvent($request->time, $data, (int)42*($request->progress)/100, 85.4, $rawHTML, $resultHTML));
    return Response::json(["Success"=>true], 200, array('Content-Type' => 'application/javascript'))->setCallback(Input::get('callback'));
  }

  public function Upload(Request $request){
    $user  = Auth::user();
    $fluor_data = json_decode($request->fluor_data);
    $melt_data = json_decode($request->melt_data);

    if(!isset($fluor_data->steps))
    {
      return Response::json(["Success"=>false], 200, array('Content-Type' => 'application/javascript'))->setCallback(Input::get('callback'));
    }
    else{
      $cq_data = $fluor_data->steps[0]->cq;
      $amplification_data = $fluor_data->steps[0]->amplification_data;

      $test  = test::find($request->test_id);
      $panel = $test->belongsTo('App\Models\GenePanel', 'panel_id')->first();
      $names = ["Cycle"];
      $raw_data = [];
      $CQs = [];
      foreach(json_decode($panel->genes) as $gene){
        array_push($names, $gene->name);
      }

      $cycle_count = 0;
      foreach($amplification_data as $row){
        if($cycle_count < $row[2]) $cycle_count = $row[2];
      }

      for($cycle = 0; $cycle<$cycle_count; $cycle+=1){
        $row_tmp = [$cycle];
        $filled = false;
        for($well = 0; $well<count($names); $well+=1){
          foreach($amplification_data as $row){
            if(($row[1] === $well)&&($row[2] === $cycle)){
              array_push($row_tmp, $row[3]);
              $filled = true;
              // break;
            }
          }
        }
        if($filled) array_push($raw_data, $row_tmp);
      }

      if(isset($test)){
        if($test->user_id == $user->id){
          $test->raw_data = json_encode($raw_data);
          $results = $this->analyze2($raw_data, $names, $cq_data);
          $test->result=json_encode($results);
          $test->save();

          $rawHTML = view('user.includes.data_table')
                            ->with('data', $raw_data)
                            ->with('titles', $names)
                            ->render();

          $resultHTML = view('user.includes.result_table')
                            ->with('results', $results)
                            ->render();
        }
      }
      else {
        //Test not found
        $rawHTML = "";
        $resultHTML = "";
        return Response::json(["Success"=>false], 200, array('Content-Type' => 'application/javascript'))->setCallback(Input::get('callback'));
      }

      event(new \App\Events\DataEvent($request->time, $raw_data, (int)42*($request->progress)/100, 85.4, $rawHTML, $resultHTML));
      return Response::json(["Success"=>true], 200, array('Content-Type' => 'application/javascript'))->setCallback(Input::get('callback'));
    }
  }

  private function create_simulation($filepath, $names, $CTs){

    $cycles = range(0, 42);
    $output = "Cycle,T0,T1";
    $items = range(-2, 2);

    //This will ensure, that samples with the same name have the same response
    for($i = 0; $i< count($CTs); $i+=1){
      $found = array_search ($names[$i] , $names);
      if($found) $CTs[$i] = $CTs[$found] + $items[array_rand($items)];
    }

    foreach($names as $name){
      $output = $output.",".$name;
    }
    $output = $output."\n";

    $items = range(-20, 20);

    foreach($cycles as $cycle){
      $output = $output.$cycle.",";
      $output = $output.",";
      foreach($CTs as $CT){
        $value_current = 500 + $items[array_rand($items)] + 1000 / (1 + exp($CT - $cycle));
        $output = $output.",".$value_current;
      }
      $output = $output."\n";
    }

    Storage::disk('local')->put($filepath, $output);
  }


  private function analyze($data){
    $header = $data[0];
    unset($data[0]);
    $results = array();

    for($i = 3; $i<count($header); $i+=1){
      $result = (object)array();

      $result->name = $header[$i];

      $values = array_column($data, $i);
      if(count($values)>0){
        asort($values);
        $range = $values[count($values)-1] - $values[0];
        $thr = $range * 0.1;

        $Ct = 0;
        for($j = 1; $j < count($data); $j+=1){
          if($data[$j][$i] > $thr + $values[0]) break;
          else $Ct = $data[$j][0];
        }

        $result->Ct = $Ct;
        $result->copyNr = (int) (1000000 * pow(1.8, 25 - $Ct));
        $quality = 200 * $range/($values[count($values)-1] + $values[0]);
        if($quality>100) $quality = 100;
        $result->quality = (int)$quality;
        array_push($results, $result);
      }
      else{
        return (object)array();
      }
    }
    return $results;
  }

  private function analyze2($data, $header, $Cq){
    $results = array();

    for($i = 1; $i<count($header); $i+=1){
      $result = (object)array();

      $result->name = $header[$i];

      $values = array_column($data, $i);
      if(count($values)>0){
        asort($values);
        $range = $values[count($values)-1] - $values[0];
        $thr = $range * 0.1;

        $Ct = $Cq[$i][2];
        for($j = 1; $j < count($data); $j+=1){
          if($data[$j][$i] > $thr + $values[0]) break;
          else $Ct = $data[$j][0];
        }

        $result->Ct = $Ct;
        $result->copyNr = (int) (1000000 * pow(1.8, 25 - $Ct));
        $quality = 200 * $range/($values[count($values)-1] + $values[0]);
        if($quality>100) $quality = 100;
        $result->quality = (int)$quality;
        array_push($results, $result);
      }
      else{
        return (object)array();
      }
    }
    return $results;
  }

}
