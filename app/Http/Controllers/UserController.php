<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\School;
use App\Models\Code;
use App\Models\Role;
use App\Models\Topic;
use App\Models\Exercise;
use App\Models\Post;
use App\Models\Like;
use App\Models\Test as test;
use App\Models\Event;
use App\Models\GenePanel as panel;
use App\Models\GeneTarget as gene;
use App\Models\DnaExtraction as extraction;
use App\Models\Experiment as experiment;

use Illuminate\Http\Request;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function Welcome(){
    $user = Auth::user();

    $tests = test::where('user_id', $user->id)->orWhere('privacy', 'public')->orderBy("created_at",'desc')->take(3)->get();

    $totalHtml = "";
    foreach($tests as $test){
      $comments = Post::where('test_id', $test->id)->get();
      $likes = Like::where('test_id', $test->id)->get();
      if(Storage::disk('local')->exists('tests/avatars/'.$test->avatar)){
        $content = Storage::disk('local')->get('tests/avatars/'.$test->avatar);
        $test->imdata = base64_encode($content);
      }
      if((isset($test->panel_id) && isset($test->extraction_id))){
        $panel = panel::find($test->panel_id);
        $panelHTML = view('user.includes.panel_genes_min') //Container for test results
                          ->with('panel', $panel)
                          ->with('genes', json_decode($panel->genes))
                          ->render();
      }
      $returnHTML = view('user.includes.test_container') //Container for the list
                        ->with('test', $test)
                        ->with('comments', $comments)
                        ->with('likes', $likes)
                        ->with('user', $user)
                        ->with('panelHTML', $panelHTML)
                        ->render();
      $totalHtml = $totalHtml.$returnHTML; //Concatenate the test result containers
    }

    $events = event::where('user_id', $user->id)->orderBy('created_at','desc')->get();
    $timelineHTML = view('user.includes.timeline_container')
                      ->with('events', $events)->render();

    $status = (object)array('testCount'=>test::where('user_id', $user->id)->count(), 'experiments'=>gene::count(), 'exercises'=>Exercise::count());

    return view('user/welcome', compact('user'), compact('chart'))
                ->with('tests', $totalHtml)
                ->with('status', $status)
                ->with('timeline', $timelineHTML);
  }

  public function Account($type){
    $user = Auth::user();
    if($type=="basic"){

      $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel

      $answers = $user->FlashAnswers()->get();
      $outTable->addNumberColumn('Answer');
      $outTable->addNumberColumn('Score');
      $counter = 0;
      foreach ($answers as $answer) {
          $counter += 1;
          $outTable->addRow([$counter, $answer->rating_user]);
      }
      $chart = \Lava::LineChart('FlashChart', $outTable, ['height'=>150, 'title' => "Score chart", 'legend' => ['position' => 'top'], 'hAxis'=>['title'=>"Answer count"]]);

      $storagePath = public_path('/uploads/avatars/' . $user->avatar);
      $im = file_get_contents($storagePath);
      $imdata = base64_encode($im);

      $events = event::where('user_id', $user->id)->orderBy('created_at','desc')->get();
      $timelineHTML = view('user.includes.timeline_container')
                        ->with('events', $events)->render();

      $storagePath = public_path('/uploads/avatars/' . $user->avatar);
      $im = file_get_contents($storagePath);
      $imdata = base64_encode($im);
      return view('user/settings', compact('user'), compact('chart'))
                      ->with('imdata', $imdata)
                      ->with('timeline', $timelineHTML);
    }
    elseif($type=="student")
        return view('user/student_reg', ['user' => $user]);
    elseif($type=="admin")
        return view('user/admin_reg', ['user' => $user]);
  }

  public function Password(){
    return view('user/password', ['user' => Auth::user()]);
  }

  public function Education($id){
    $user = Auth::user();

    $FoundationTopics = Topic::where('parent', NULL)->get();

    if($id == 0)
      $FirstTopic = Topic::where('approved', true)->inRandomOrder()->first();
    else
      $FirstTopic = Topic::find($id);


    $htmlcontent = null;
    $starting_topics = null;
    /*Random first topic....*/
    if(!empty($FirstTopic)){
      $children = Topic::where('parent', $FirstTopic->id)->get();
      $content = Storage::disk('local')->get('topics/'.$FirstTopic->content);//This stores the content from disk
      $comments = Post::where('topic_id', $FirstTopic->id)->get();
      $likes = Like::where('topic_id', $FirstTopic->id)->get();
      $htmlcontent = view('user.includes.topic_container')
            ->with('user', $user)
            ->with('FirstTopic', $FirstTopic)
            ->with('content', $content)
            ->with('children', $children)
            ->with('comments', $comments)
            ->with('likes', $likes)
            ->render();
      $FirstTopic->htmlContent = $htmlcontent;

      $starting_topics = view('user/includes/starting_topics')
            ->with('FirstTopic', $FirstTopic)
            ->with('FoundationTopics', $FoundationTopics)
            ->render();
    }

    /*Foundation topics....*/
    foreach ($FoundationTopics as $topic) {
      $content = Storage::disk('local')->get('topics/'.$topic->content);//This stores the content from disk
      $children = Topic::where('parent', $topic->id)->get();
      $comments = Post::where('topic_id', $topic->id)->get();
      $likes = Like::where('topic_id', $topic->id)->get();
      $htmlcontent = view('user.includes.topic_container')
            ->with('user', $user)
            ->with('FirstTopic', $topic)
            ->with('content', $content)
            ->with('children', $children)
            ->with('comments', $comments)
            ->with('likes', $likes)
            ->render();
      $topic->htmlContent = $htmlcontent;
    }

    return view('user/education', ['user' => $user])
                ->with('starting_topics', $starting_topics);
  }

  public function Explorer(){
    $user = Auth::user();

    $tests = test::where('privacy', 'public')->where('result', "!=", null)->orWhere('user_id', $user->id)->get();
    $testsHTML = view('user.includes.table_tests')
                      ->with('tests', $tests)
                      ->render();

    $templates = experiment::where('privacy', 'public')->orWhere('user_id', $user->id)->get();
    $templatesHTML = view('user.includes.table_templates')
                      ->with('templates', $templates)
                      ->render();

    $panels = panel::where('privacy', 'public')->orWhere('user_id', $user->id)->get();
    $panelsHTML = view('user.includes.table_panels')
                      ->with('panels', $panels)
                      ->render();

    $genes = gene::where('privacy', 'public')->orWhere('user_id', $user->id)->get();
    $genesHTML = view('user.includes.table_genes')
                      ->with('genes', $genes)
                      ->render();

    $extractions = extraction::where('privacy', 'public')->orWhere('user_id', $user->id)->get();
    $extractionsHTML = view('user.includes.table_extractions')
                      ->with('extractions', $extractions)
                      ->render();

    $test = test::where('privacy', 'public')->orWhere('user_id', $user->id)->inRandomOrder()->first();
    $testHTML = "";
    if(!empty($test)){
      $comments = Post::where('test_id', $test->id)->get();
      $likes = Like::where('test_id', $test->id)->get();
      $panelHTML = "";

      if(isset($test->experiment_id)){
        $exp = experiment::find($test->experiment_id);
        $panel = panel::find($exp->gene_panel_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->render();
      }

      if((isset($test->panel_id))){
        $panel = panel::find($test->panel_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->with('genes', json_decode($panel->genes))
                          ->render();
      }

      $testHTML = view('user.includes.test_container')
                        ->with('test', $test)
                        ->with('comments', $comments)
                        ->with('likes', $likes)
                        ->with('panelHTML', $panelHTML)
                        ->with('user', $user)->render();
    }

    return view('user/explorer')
                  ->with('user' , $user)
                  ->with('testsHTML' , $testsHTML)
                  ->with('templatesHTML', $templatesHTML)
                  ->with('panelsHTML' , $panelsHTML)
                  ->with('targetsHTML' , $genesHTML)
                  ->with('testHTML', $testHTML)
                  ->with('extractionsHTML' , $extractionsHTML);
  }

  public function Update_profile(Request $request){
    $user = Auth::user();
    $user->email = $request->email;
    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->city = $request->city;
    $user->country = $request->country;
    $user->description = $request->description;
    $user->save();
    $storagePath = public_path('/uploads/avatars/' . Auth::user()->avatar);
    $im = file_get_contents($storagePath);
    $imdata = base64_encode($im);

    $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel

    $answers = $user->FlashAnswers()->get();
    $outTable->addNumberColumn('Answer');
    $outTable->addNumberColumn('Score');
    $counter = 0;
    foreach ($answers as $answer) {
        $counter += 1;
        $outTable->addRow([$counter, $answer->rating_user]);
    }
    $chart = \Lava::LineChart('FlashChart', $outTable, ['height'=>150, 'title' => "Score chart", 'legend' => ['position' => 'top'], 'hAxis'=>['title'=>"Answer count"]]);

    $events = event::where('user_id', $user->id)->orderBy('created_at','desc')->get();
    $timelineHTML = view('user.includes.timeline_container')
                      ->with('events', $events)->render();

    $storagePath = public_path('/uploads/avatars/' . $user->avatar);
    $im = file_get_contents($storagePath);
    $imdata = base64_encode($im);
    return view('user/settings', compact('user'), compact('chart'))
                    ->with('imdata', $imdata)
                    ->with('timeline', $timelineHTML);
  }

  public function Update_avatar(Request $request){
    // Handle the user upload of avatar
    if($request->hasFile('avatar')){
      $avatar = $request->file('avatar');
      $filename = time() . '.' . $avatar->getClientOriginalExtension();
      Image::make($avatar)->resize(128, 128)->save( public_path('/uploads/avatars/' . $filename ) );

      $user = Auth::user();
      $user->avatar = $filename;
      $user->save();
    }
    $storagePath = public_path('/uploads/avatars/' . Auth::user()->avatar);
    $im = file_get_contents($storagePath);
    $imdata = base64_encode($im);

    $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel

    $answers = $user->FlashAnswers()->get();
    $outTable->addNumberColumn('Answer');
    $outTable->addNumberColumn('Score');
    $counter = 0;
    foreach ($answers as $answer) {
        $counter += 1;
        $outTable->addRow([$counter, $answer->rating_user]);
    }
    $chart = \Lava::LineChart('FlashChart', $outTable, ['height'=>150, 'title' => "Score chart", 'legend' => ['position' => 'top'], 'hAxis'=>['title'=>"Answer count"]]);

    $events = event::where('user_id', $user->id)->orderBy('created_at','desc')->get();
    $timelineHTML = view('user.includes.timeline_container')
                      ->with('events', $events)->render();

    $storagePath = public_path('/uploads/avatars/' . $user->avatar);
    $im = file_get_contents($storagePath);
    $imdata = base64_encode($im);
    return view('user/settings', compact('user'), compact('chart'))
                    ->with('imdata', $imdata)
                    ->with('timeline', $timelineHTML);
  }

  public function Get_avatar(){#For accessing user avatar images
    $storagePath = public_path('/uploads/avatars/' . Auth::user()->avatar);
    return Image::make($storagePath)->response();
  }

  public function Update_admin(Request $request){#This is the main script for starting new user approval
    $user=Auth::user();

    #If a user is not an admin, reset the role and approval process, otherwise allow to update the information
    if($user->roles()->first()->id != Role::where('name', 'admin')->first()->id)
    {
      $user->activated=false;
      $user->removeRole($user->roles()->first()->id);
      $user->assignRole(Role::where('name', 'admin')->first()->id);
      $user->save();
    }

    $admin_list = Admin::where('user_id', $user->id);
    if(!$admin_list->exists())#Check if an admin with the same user id exists, create new if it doesn't
    {
      $admin = new Admin;
    }
    else{
      $admin = $admin_list->first();
    }
    #Upadate the admin record with new information
    $admin->user_id=$user->id;
    $admin->run_tests=isset($request->run_tests);
    $admin->design_tests=isset($request->design_tests);
    $admin->edit_material=isset($request->edit_material);
    $admin->extracurricular=isset($request->extracurricular);
    $admin->curricular=isset($request->curricular);
    $admin->school_role=$request->school_role;
    $admin->phone=$request->phone;
    $admin->training=$request->training;
    $admin->save();

    $school_list = School::where('postcode', $request->postcode)->orWhere('name', $request->name);
    if(!$school_list->exists())#If this is a school not in the database and user does not have one assigned
    {
      if($user->hasSchool())
      {
        $user->removeSchool($user->school()->first()->id);
      }
      $school = new School;
      $school->name=$request->name;
      $school->postcode=$request->postcode;
      $school->address=$request->street;
      $school->type=$request->type;
      $school->board=$request->board;
      $school->students=$request->total;
      $school->class_size=$request->class;
      $school->city=$request->city;
      $school->country=$request->country;
      $school->save();
      $user->assignSchool($school_list->first()->id);
    }
    else{
      #Only update the school record, if the user is activated
      if($user->activated){
        $school = $school_list->first();
        $school->name=$request->name;
        $school->postcode=$request->postcode;
        $school->address=$request->street;
        $school->type=$request->type;
        $school->board=$request->board;
        $school->students=$request->total;
        $school->class_size=$request->class;
        $school->city=$request->city;
        $school->country=$request->country;
        $school->save();
      }

      #If user does not have an assigned school, but the one he entered exists,
      #assign the existing school
      if(!$user->hasSchool())
        $user->assignSchool($school_list->first()->id);
    }

    return view('user/admin_reg', ['user' => $user]);
  }

  public function Assign_school($school){#A script that assigns a school to a user (without approval)
    $user=Auth::user();
    $user->activated=false;
    $user->assignSchool($school);
    $user->removeRole($user->roles()->first()->id);
    $user->assignRole(Role::where('name', 'admin')->first()->id);
    $user->save();
    // if(!$user->hasSchool())
    //   $user->assignSchool($school);

    return view('user/admin_reg', ['user' => $user]);
  }

  public function Use_code(Request $request){#A script that assigns a school to a user (without approval)
    $errors = "";
    $user=Auth::user();
    $user->activated=false;
    $user->removeRole($user->roles()->first()->id);
    $user->assignRole(Role::where('name', 'student')->first()->id);
    $code_model = Code::where('code', $request->code)->first();#Check the code exists
    if($user->hasSchool())
      $user->removeSchool($user->school()->first()->id);
    if(sizeof($code_model)>0){#Check, that the code is valid
      $user->activated=true;
      $user->assignSchool($code_model->school_id);
    }
    else
      $errors = "Invalid code";
    $user->save();
    return view('user/student_reg', ['user' => $user, 'errors' => $errors]);
  }

  public function Unregister(){#A script that assigns a school to a user (without approval)
    $user=Auth::user();
    $user->activated=false;
    $user->removeRole($user->roles()->first()->id);
    $user->assignRole(Role::where('name', 'user')->first()->id);
    $user->removeSchool($user->school()->first()->id);
    $user->save();
    return view('user/student_reg', ['user' => $user]);
  }

  public function Postcode_lookup(Request $request){#A script that returns all school names and postcodes, that match first 3 symbols
    if(strlen($request->postcode)>2)
    {
      $school=School::where('postcode', $request->postcode)->get();
      if(sizeof($school)==0)
        $school=School::where('postcode', 'like', '%' . substr($request->postcode, 0, 3) . '%')->get();
    }
    else {
      $school=collect(new School);#Return an empty school model
    }

    $ids = $school->pluck('id');
    $schools = $school->pluck('name');
    $postcodes = $school->pluck('postcode');
    return response()->json([
    'ids' => $ids,
    'schools' => $schools,
    'postcodes' => $postcodes
    ]);
  }
    //
}
