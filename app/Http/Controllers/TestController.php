<?php

namespace App\Http\Controllers;

use \App\Models\Instrument as instrument;
use \App\Models\Experiment as experiment;
use \App\Models\DnaExtraction as extraction;
use \App\Models\GeneTarget as target;
use \App\Models\GenePanel as panel;
use \App\Models\Test as test;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Like;
use App\Models\Price as price;
use Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{

  public function back(Request $request){
    return back();
  }

  public function start(){
    $user = Auth::user();

    $price = price::where('item', 'dna_extraction')->first();
    if(!empty($price)) $extraction_price = $price->price_pounds;
    else $extraction_price = 6.0;

    $price = price::where('item', 'gene_panel')->first();
    if(!empty($price)) $panel_price = $price->price_pounds;
    else $panel_price = 12.0;

    $price = price::where('item', 'booking_transport')->first();
    if(!empty($price)) $transport_price = $price->price_pounds;
    else $transport_price = 20.0;

    $price = price::where('item', 'booking_day')->first();
    if(!empty($price)) $booking_price = $price->price_pounds;
    else $booking_price = 20.0;

    return view('user.experiment.order')
                ->with('user', $user)
                ->with('extraction_price', $extraction_price)
                ->with('panel_price', $panel_price)
                ->with('transport_price', $transport_price)
                ->with('booking_price', $booking_price);
  }
  //
  /*
   DNA extraction routes
  */
  //
  /* Create a new type of DNA extraction protocol */
  public function createExtraction(Request $request){
    $user = Auth::user();

    $extraction= extraction::where([
      'protocol'=>$request->protocol
    ])->first();

    if((count($extraction)<1)){
      $extraction = extraction::create(array(
                                        'type'=>$request->type,
                                        'steps'=>$request->steps,
                                        'duration'=>$request->duration,
                                        'privacy'=>$request->privacy,
                                        'protocol'=>$request->protocol,
                                        'derived_from'=>$request->derived_from,
                                        'additional'=>$request->additional,
                                        'user_id'=>$user->id,
                                        'approved'=>false
                                      ));
      $returnHTML = view('user.experiment.dna_extraction')->with('user', $user)->with('extraction', $extraction)->with('status', "success")->render();
    }
    else{
      $returnHTML = view('user.experiment.dna_extraction')->with('user', $user)->with('extraction', $extraction)->with('status', "DNA extraction protocol already exists.")->render();
    }
    return view('user.experiment.design_extraction')->with('user', $user)->with('method', $returnHTML);
  }

  /* Return DNA extraction list */
  public function getExtractionList(){
    $user = Auth::user();
    $extractions = extraction::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.extraction_list')->with('extractions', $extractions)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getExtractionSelect(){
    $user = Auth::user();
    $extractions = extraction::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.extraction_list_select')->with('extractions', $extractions)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getExtractionMin($id){
    $user = Auth::user();
    $extraction = extraction::find($id);
    $returnHTML = view('user.includes.extraction_container_min')->with('extraction', $extraction)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getExtractionListBuy(){
    $user = Auth::user();
    $extractions = extraction::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.extraction_list_buy')->with('extractions', $extractions)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  /* Return DNA extraction protocol in a minimal window */
  public function getExtractionProtocol($id){
    $extraction = extraction::find($id);
    return view('user.experiment.extraction_protocol')->with('extraction', $extraction);
  }

  public function deleteExtraction($id){
    $user = Auth::user();
    $extraction = extraction::find($id);
    $extraction->delete();
    return view('user.experiment.design_extraction')->with('user', $user);
  }

  /* Display experiment design window */
  public function designExtractionWindow($extraction){
    $user = Auth::user();

    if($extraction == 0){
      return view('user.experiment.design_extraction')->with('user', $user);
    }
    else {
      $extraction = extraction::find($extraction);
      if(($extraction->user_id == $user->id)){
        $returnHTML = view('user.experiment.dna_extraction')->with('user', $user)->with('extraction', $extraction)->render();
        return view('user.experiment.design_extraction')->with('user', $user)->with('method', $returnHTML);
      }
      else{
        return view('user.experiment.design_extraction')->with('user', $user);
      }
    }
  }
  //
  //
  /*
   Gene target routes
  */
  //
  /* Display gene target design window */
  public function designTargetWindow($id){
    $user = Auth::user();

    if($id == 0){
      return view('user.experiment.design_target')->with('user', $user);
    }
    else {
      $target = target::find($id);
      if(($target->user_id == $user->id)){
        $returnHTML = view('user.experiment.target_form')->with('user', $user)->with('target', $target)->render();
        return view('user.experiment.design_target')->with('user', $user)->with('target', $returnHTML);
      }
      else{
        return view('user.experiment.design_target')->with('user', $user);
      }
    }
  }
  /* Create a new gene target */
  public function createTarget(Request $request){
    $user = Auth::user();
    $target= target::where([
      'forward_primer'=>$request->forward_primer,
      'backward_primer'=>$request->backward_primer
    ])->first();

    if((count($target)<1)|($request->forward_primer=="NNNNNNNNNN")){
      $target = target::create(array(
                                'type'=>$request->type,
                                'name'=>$request->name,
                                'gene_id'=>$request->gene_id,
                                'reference'=>$request->reference,
                                'forward_primer'=>$request->forward_primer,
                                'backward_primer'=>$request->backward_primer,
                                'privacy'=>$request->privacy,
                                'derived_from'=>$request->derived_from,
                                'user_id'=>$user->id,
                                'approved'=>false
                              ));
      $returnHTML = view('user.experiment.target_form')->with('user', $user)->with('target', $target)->with('status', "success")->render();
    }
    else{
      $returnHTML = view('user.experiment.target_form')->with('user', $user)->with('target', $target)->with('status', "Primer combination already exists")->render();
    }
    return view('user.experiment.design_target')->with('user', $user)->with('target', $returnHTML);
  }

  /* Edit a gene target */
  public function editTarget(Request $request){
    $user = Auth::user();
    $target = target::find($request->id);
    //Can only edit the test, that belongs to a user or if you are a superuser
    if(($target->user_id == $user->id)|($user->roles()->first()->name == 'superuser')){
      $target->name = $request->name;
      $target->type = $request->type;
      $target->gene_id = $request->gene_id;
      $target->reference = $request->reference;
      $target->forward_primer = $request->forward_primer;
      $target->backward_primer = $request->backward_primer;
      $target->privacy = $request->privacy;
      $target->derived_from = $request->derived_from;
      $target->user_id = $user->id;
      $target->approved = false;
      $target->save();
      $returnHTML = view('user.experiment.target_form')->with('user', $user)->with('target', $target)->with('status', "success")->render();
    }
    else{
      $returnHTML = view('user.experiment.target_form')->with('user', $user)->with('target', $target)->with('status', "This target has not been created by you.")->render();
    }
    return view('user.experiment.design_target')->with('user', $user)->with('target', $returnHTML);
  }

  /* Return target list */
  public function getTargetList(){
    $user = Auth::user();
    $targets = target::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.target_list_add')->with('targets', $targets)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML, 'gene_list'=>json_encode($targets)));
  }
  /* Return DNA extraction list */
  public function getTargetListEdit(){
    $user = Auth::user();
    $targets = target::where('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.target_list_edit')->with('targets', $targets)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function deleteTarget($id){
    $user = Auth::user();
    $target = target::find($id);
    $target->delete();
    return view('user.experiment.design_target')->with('user', $user);
  }

  //
  /*
   Gene panel routes
  */
  //
  /* Display gene panel design information */
  public function ViewPanel($id){
    $panel = panel::find($id);
    $genes = json_decode($panel->genes);
    $gene_list = view('user.experiment.panel_rows')->with('genes', $genes)->render();
    // dd(json_decode($panel->genes));
    return view('user.experiment.panel_view')->with('panel', $panel)->with('gene_list', $gene_list);
  }

  /* Display gene panel design window */
  public function designPanelWindow($id){
    $user = Auth::user();

    if($id == 0){
      return view('user.experiment.design_panel')->with('user', $user);
    }
    else {
      $panel = panel::find($id);
      if(($panel->user_id == $user->id)){
        $returnHTML = view('user.experiment.panel_form')->with('user', $user)->with('panel', $panel)->render();
        return view('user.experiment.design_panel')->with('user', $user)->with('panel', $returnHTML)->with('genes', $panel->genes);
      }
      else{
        return view('user.experiment.design_panel')->with('user', $user);
      }
    }
  }
  /* Return target list */
  public function getPanelRow($id){
    $user = Auth::user();
    $target = target::find($id);
    $returnHTML = view('user.experiment.panel_row')->with('target', $target)->render();
    $target->html=$returnHTML;
    return response()->json(array('success' => true, 'html'=>$returnHTML, 'gene'=>json_encode($target)));
  }
  /* Create a new gene panel */
  public function createPanel(Request $request){
    $user = Auth::user();
    $panel= panel::where([
      'genes'=>$request->genes
    ])->first();

    if((count($panel)<1)){
      $panel = panel::create(array(
                                  'type'=>$request->type,
                                  'name'=>$request->name,
                                  'content'=>$request->content,
                                  'genes'=>$request->genes,
                                  'pos_controls' => $request->pos_controls,
                                  'neg_controls' => $request->neg_controls,
                                  'reactions' => $request->reactions,
                                  'privacy'=>$request->privacy,
                                  'user_id'=>$user->id,
                                  'approved'=>false
                                ));
      $returnHTML = view('user.experiment.panel_form')->with('user', $user)->with('panel', $panel)->with('status', "success")->render();
    }
    else{
      $returnHTML = view('user.experiment.panel_form')->with('user', $user)->with('panel', $panel)->with('status', "A panel already exists")->render();
    }
    return view('user.experiment.design_panel')->with('user', $user)->with('panel', $returnHTML)->with('genes', $panel->genes);
  }

  /* Edit a gene target */
  public function editPanel(Request $request){
    $user = Auth::user();
    $panel = panel::find($request->id);
    if(($panel->user_id == $user->id)|($user->roles()->first()->name == 'superuser')){
      $panel->name = $request->name;
      $panel->type = $request->type;
      $panel->content = $request->content;
      $panel->genes = $request->genes;
      $panel->pos_controls = $request->pos_controls;
      $panel->neg_controls = $request->neg_controls;
      $panel->privacy = $request->privacy;
      $panel->reactions = $request->reactions;
      $panel->user_id = $user->id;
      $panel->approved = false;
      $panel->save();
      $returnHTML = view('user.experiment.panel_form')->with('user', $user)->with('panel', $panel)->with('status', "success")->render();
    }
    else {
      $returnHTML = view('user.experiment.panel_form')->with('user', $user)->with('panel', $panel)->with('status', "This panel has not been created by you.")->render();
    }
    return view('user.experiment.design_panel')->with('user', $user)->with('panel', $returnHTML)->with('genes', $panel->genes);
  }

  public function deletePanel($id){
    $user = Auth::user();
    $panel = panel::find($id);
    $panel->delete();
    return view('user.experiment.design_panel')->with('user', $user);
  }

  /* Return DNA extraction list */
  public function getPanelList(){
    $user = Auth::user();
    $panels = panel::where('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.panel_list')->with('panels', $panels)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getPanelListBuy(){
    $user = Auth::user();
    $panels = panel::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.panel_list_buy')->with('panels', $panels)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getPanelListSelect(){
    $user = Auth::user();
    $panels = panel::where('approved', true)->get();
    $returnHTML = view('user.experiment.panel_list_select')->with('panels', $panels)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  public function getPanelMin($id){
    $user = Auth::user();
    $panel = panel::find($id);
    $genes = json_decode($panel->genes);
    $returnHTML = view('user.includes.panel_container_min')->with('panel', $panel)->with('genes', $genes)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }

  /* Return DNA extraction list */
  public function getPanelListView(){
    $user = Auth::user();
    $panels = panel::where('approved', true)->orWhere('user_id', $user->id)->get();
    $returnHTML = view('user.experiment.panel_list')->with('panels', $panels)->with('user', $user)->render();
    return response()->json(array('success' => true, 'html'=>$returnHTML));
  }


    //
    /*
     Design genetic experiments
    */
    //
    /* Display new test design window */
    public function New($type){
      $user = Auth::user();
      if($type == "custom")
        return view('user.experiment.new_custom')->with('user', $user);
      else
        return view('user.experiment.new')->with('user', $user);
    }

    /* Load existing experiments*/
    public function MyTests($id){
      $user = Auth::user();

      $totalHtml = "";
      $tests = test::where('user_id', $user->id)->get();
      $totalHtml = view('user.experiment.test_list')
                        ->with('tests', $tests)
                        ->with('user', $user)->render();

      if($id != 0)
      {
        $test = test::where(['user_id'=>$user->id, 'id'=>$id])->get()->first();

        $panelHTML = "";
        $extrHTML = "";
        if(isset($test->experiment_id)){
          $exp = experiment::find($test->experiment_id);
          $panel = panel::find($exp->gene_panel_id);
          $extrn = extraction::find($exp->extraction_id);
          $panelHTML = view('user.includes.panel_container_min')
                            ->with('panel', $panel)
                            ->render();
          $extrHTML = view('user.includes.extraction_container_min')
                            ->with('extraction', $extrn)
                            ->render();
        }
        else{
          $panel = panel::find($test->panel_id);
          $extrn = extraction::find($test->extraction_id);
          $panelHTML = view('user.includes.panel_container_min')
                            ->with('panel', $panel)
                            ->render();
          $extrHTML = view('user.includes.extraction_container_min')
                            ->with('extraction', $extrn)
                            ->render();
        }

        if((isset($test->panel_id) && isset($test->extraction_id))){
          $panel = panel::find($test->panel_id);
          $extrn = extraction::find($test->extraction_id);
          $panelHTML = view('user.includes.panel_container_min')
                            ->with('panel', $panel)
                            ->with('genes', json_decode($panel->genes))
                            ->render();
          $extrHTML = view('user.includes.extraction_container_min')
                            ->with('extraction', $extrn)
                            ->render();
        }

        if(Storage::disk('local')->exists('tests/avatars/'.$test->avatar)){
          $content = Storage::disk('local')->get('tests/avatars/'.$test->avatar);
          $imdata = base64_encode($content);
          $test->imdata = $imdata;
        }

        if($test->instrument_id>0) $instrument = instrument::find($test->instrument_id);
        else  $instrument = instrument::where('type', 'Simulation')->first();

        if(isset($test->raw_data)){
          $raw_data = json_decode($test->raw_data, true);
          $titles = $raw_data[0];
          unset($raw_data[0]);
          $rawHTML = view('user.includes.data_table')
                            ->with('titles', $titles)
                            ->with('data', $raw_data)
                            ->render();
        }
        else {
          $rawHTML = "No data";
        }

        if(isset($test->result)){
          $results = json_decode($test->result);
          $resultHTML = view('user.includes.result_table')
                            ->with('results', array($results)[0])
                            ->render();
        }
        else {
          $resultHTML = "No data";
        }

        $comments = Post::where('test_id', $test->id)->get();
        $likes = Like::where('test_id', $test->id)->get();

        return view('user.experiment.my_tests')
                    ->with('test', $test)
                    ->with('tests', $totalHtml)
                    ->with('user', $user)
                    ->with('panelHTML', $panelHTML)
                    ->with('rawHTML', $rawHTML)
                    ->with('resultHTML', $resultHTML)
                    ->with('extrHTML', $extrHTML)
                    ->with('comments', $comments)
                    ->with('likes', $likes);
        }
        else{
          return view('user.experiment.my_tests')
                  ->with('user', $user)
                  ->with('tests', $totalHtml);
        }
    }


    /* Returns test list */
    public function getTestList(){
      $user = Auth::user();
      $tests = test::get();
      $totalHtml = "";
      foreach($tests as $test){
        $comments = Post::where('test_id', $test->id)->get();
        $likes = Like::where('test_id', $test->id)->get();
        $returnHTML = view('user.includes.test_container')
                          ->with('test', $test)
                          ->with('comments', $comments)
                          ->with('likes', $likes)
                          ->with('user', $user)->render();
        $totalHtml = $totalHtml.$returnHTML;
        }
      return response()->json(array('success' => true, 'html'=>$totalHtml));
    }

    /* Returns test list */
    public function getTestList2(){
      $user = Auth::user();
      $tests = test::where('user_id', $user->id)->get();

      $returnHTML = view('user.experiment.test_list')
                        ->with('tests', $tests)
                        ->with('user', $user)->render();
      return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    /* Returns test list */
    public function getTest($id){
      if($id == 0){
        return response()->json(array('success' => true, 'html'=>""));
      }
      else{
        $user = Auth::user();
        $test = test::find($id);

        $comments = Post::where('test_id', $id)->get();
        $likes = Like::where('test_id', $id)->get();
        $panelHTML = "";
        $extrHTML = "";
        if(isset($test->experiment_id)){
          $exp = experiment::find($test->experiment_id);
          $panel = panel::find($exp->gene_panel_id);
          $extrn = extraction::find($exp->extraction_id);
          $panelHTML = view('user.includes.panel_container_min')
                            ->with('panel', $panel)
                            ->render();
          $extrHTML = view('user.includes.extraction_container_min')
                            ->with('extraction', $extrn)
                            ->render();
        }

        if((isset($test->panel_id)&&isset($test->extraction_id))){
          $panel = panel::find($test->panel_id);
          $extrn = extraction::find($test->extraction_id);
          $panelHTML = view('user.includes.panel_container_min')
                            ->with('panel', $panel)
                            ->with('genes', json_decode($panel->genes))
                            ->render();
          $extrHTML = view('user.includes.extraction_container_min')
                            ->with('extraction', $extrn)
                            ->render();
        }

        $returnHTML = view('user.includes.test_container')
                          ->with('test', $test)
                          ->with('comments', $comments)
                          ->with('likes', $likes)
                          ->with('panelHTML', $panelHTML)
                          ->with('extrHTML', $extrHTML)
                          ->with('user', $user)->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
      }
    }

    /* Returns test list */
    public function deleteTest($id){
      $user = Auth::user();
      $test = test::where(['id'=>$id, 'user_id'=>$user->id])->first();
      $test->delete();
      return redirect('/test/my_tests/0');
      $this->MyTests(0);
    }


    /* Saves an existing test */
    public function saveTest(Request $request){
      $user = Auth::user();
      //Save the attached image if exists
      $avatar_changed = false;
      if($request->hasFile('avatar')){
        $filename = "default.png";
        if(!(($request->file('avatar')->getClientOriginalExtension()=='png')|
        ($request->file('avatar')->getClientOriginalExtension()=='jpg')|
        ($request->file('avatar')->getClientOriginalExtension()=='gif')))
        return response()->json(array('success' => false, 'message' => "Image format has to be png, jpg or gif"));

        $avatar = $request->file('avatar');
        $filename = $user->id . time() . "." . $request->file('avatar')->getClientOriginalExtension();
        $filepath = 'tests/avatars/'.$filename;
        $processed = Image::make($avatar)->fit(256)->stream()->__toString();
        Storage::disk('local')->put($filepath, $processed);
        $avatar_changed = true;
        // $content = Storage::disk('local')->get($filename);//This stores the content from disk
      }
      $result = test::where('id_form', $request->id_form)->first();

      if(!isset($result)){
        if(!$avatar_changed) $filename = "default.png";

        $result = test::create([
          'title'=>$request->title,
          'id_form'=>$request->id_form,
          'comments'=>$request->notes,
          'avatar'=>$filename,
          'user_id'=>$user->id,
          'experiment_id'=>$request->experiment_id,
          'extraction_id'=>$request->extraction_id,
          'panel_id'=>$request->panel_id,
          'privacy'=>$request->privacy
        ]);
        return response()->json(array('success' => true, 'message' => "Created"));
      }
      else{
        $result->title = $request->title;
        $result->id_form = $request->id_form;
        $result->comments = $request->notes;
        if($avatar_changed) $result->avatar = $filename;
        $result->user_id = $user->id;
        $result->experiment_id = $request->experiment_id;
        $result->extraction_id = $request->extraction_id;
        $result->panel_id = $request->panel_id;
        $result->privacy = $request->privacy;
        $result->save();
        return response()->json(array('success' => true, 'message' => "Updated"));
      }
      return response()->json(array('success' => false, 'message' => "Could not update test settings"));
    }

    /* Run a new test */
    public function runNewTest(Request $request){
      $user = Auth::user();
      //Save the attached image if exists
      $result = test::where('id_form', $request->id_form)->first();
      $image_updated = false;
      if(!isset($result)){
        $filename = "default.png";
        if($request->hasFile('avatar')){
          if(!(($request->file('avatar')->getClientOriginalExtension()=='png')|
          ($request->file('avatar')->getClientOriginalExtension()=='jpg')|
          ($request->file('avatar')->getClientOriginalExtension()=='gif')))
          return back()->with('message', "Image format has to be png, jpg or gif");

          $avatar = $request->file('avatar');
          $filename = $user->id . time() . "." . $request->file('avatar')->getClientOriginalExtension();
          $filepath = 'tests/avatars/'.$filename;
          $processed = Image::make($avatar)->fit(256)->stream()->__toString();
          Storage::disk('local')->put($filepath, $processed);
          $image_updated = true;
          // $content = Storage::disk('local')->get($filename);//This stores the content from disk
        }
      }

      if(!isset($result))
        $result = test::create([
          'title'=>$request->title,
          'id_form'=>$request->id_form,
          'comments'=>$request->notes,
          'avatar'=>$filename,
          'user_id'=>$user->id,
          'experiment_id'=>$request->experiment_id,
          'extraction_id'=>$request->extraction_id,
          'instrument_id'=>NULL,
          'panel_id'=>$request->panel_id,
          'privacy'=>$request->privacy
        ]);
      else{
        $result->title = $request->title;
        $result->id_form = $request->id_form;
        $result->comments = $request->notes;
        if($image_updated) $result->avatar = $filename;
        $result->user_id = $user->id;
        $result->experiment_id = $request->experiment_id;
        $result->extraction_id = $request->extraction_id;
        $result->panel_id = $request->panel_id;
        $result->privacy = $request->privacy;
        $result->save();
      }

      $panelHTML = "";
      $extrHTML = "";
      if(isset($result->experiment_id)){
        $exp = experiment::find($result->experiment_id);
        $panel = panel::find($exp->gene_panel_id);
        $extrn = extraction::find($exp->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      if((isset($result->panel_id)&&isset($result->extraction_id))){
        $panel = panel::find($result->panel_id);
        $extrn = extraction::find($result->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->with('genes', json_decode($panel->genes))
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      $content = Storage::disk('local')->get('tests/avatars/'.$result->avatar);
      $imdata = base64_encode($content);
      $result->imdata = $imdata;

      if(isset($result->instrument_id)) {
        if($result->instrument_id>0) {
          $instrument = instrument::find($result->instrument_id);
        }
        else{
          $instrument = instrument::first();
          $instrument->type = "Simulation";
        }
      }
      else{
        $instrument = NULL;
      }
      return view('user.experiment.run_test')
                  ->with('test', $result)
                  ->with('user', $user)
                  ->with('instrument', $instrument)
                  ->with('panelHTML', $panelHTML)
                  ->with('extrHTML', $extrHTML);
    }

    /* Run a new test */
    public function runTest($id){
      $user = Auth::user();
      $test = test::find($id);
      $panelHTML = "";
      $extrHTML = "";

      if(isset($test->experiment_id)){
        $exp = experiment::find($test->experiment_id);
        $panel = panel::find($exp->gene_panel_id);
        $extrn = extraction::find($exp->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      if((isset($test->panel_id)&&isset($test->extraction_id))){
        $panel = panel::find($test->panel_id);
        $extrn = extraction::find($test->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->with('genes', json_decode($panel->genes))
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      if(Storage::disk('local')->exists('tests/avatars/'.$test->avatar)){
        $content = Storage::disk('local')->get('tests/avatars/'.$test->avatar);
        $imdata = base64_encode($content);
        $test->imdata = $imdata;
      }

      $instrument = null;
      if(isset($test->instrument_id)){
        if($test->instrument_id>0) $instrument = instrument::find($test->instrument_id);
        else{
          $instrument = instrument::find(1)->first();
          $instrument->type = 'Simulation';
        }
      }

      if(isset($test->raw_data)){
        $raw_data = json_decode($test->raw_data, true);
        $titles = $raw_data[0];
        unset($raw_data[0]);
        $rawHTML = view('user.includes.data_table')
                          ->with('titles', $titles)
                          ->with('data', $raw_data)
                          ->render();
      }
      else {
        $rawHTML = "No data";
      }

      if(isset($test->result)){
        $results = json_decode($test->result);
        $resultHTML = view('user.includes.result_table')
                          ->with('results', array($results)[0])
                          ->render();
      }
      else {
        $resultHTML = "No data";
      }

      return view('user.experiment.run_test')
                  ->with('test', $test)
                  ->with('user', $user)
                  ->with('panelHTML', $panelHTML)
                  ->with('rawHTML', $rawHTML)
                  ->with('resultHTML', $resultHTML)
                  ->with('instrument', $instrument)
                  ->with('extrHTML', $extrHTML);
    }


    /* Experiment controller */
    /* Returns experiment list */
    public function getExperimentList(){
      $user = Auth::user();
      $experiments = experiment::get();
      $totalHtml = "";
      foreach($experiments as $experiment){
        $comments = Post::where('experiment_id', $experiment->id)->get();
        $likes = Like::where('experiment_id', $experiment->id)->get();
        $returnHTML = view('user.includes.experiment_container')
                          ->with('experiment', $experiment)
                          ->with('comments', $comments)
                          ->with('likes', $likes)
                          ->with('user', $user)->render();
        $totalHtml = $totalHtml.$returnHTML;
        }
      return response()->json(array('success' => true, 'html'=>$totalHtml));
    }

    /* Returns experiment */
    public function getExperiment($id){
      $user = Auth::user();
      $experiment = experiment::find($id);

      $comments = Post::where('experiment_id', $id)->get();
      $likes = Like::where('experiment_id', $id)->get();
      $returnHTML = view('user.includes.experiment_container')
                        ->with('experiment', $experiment)
                        ->with('comments', $comments)
                        ->with('likes', $likes)
                        ->with('user', $user)->render();
      return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    /* Returns minimal experiment representation*/
    public function getExperimentMin($id){
      $user = Auth::user();

      $exp = experiment::find($id);
      $panel = panel::find($exp->gene_panel_id);
      $extrn = extraction::find($exp->extraction_id);

      $panelHTML = view('user.includes.panel_container_min')
                        ->with('panel', $panel)
                        ->render();

      $extrHTML = view('user.includes.extraction_container_min')
                        ->with('extraction', $extrn)
                        ->render();

      $returnHTML = $panelHTML."<br/><br/>".$extrHTML;

      return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    /* Display experiment information */
    public function ExperimentView($id){
      $content = view('user.experiment.instrument_instructions')->render();
      return view('user.experiment.experiment_view')->with('content', $content);
    }

    public function viewAnalysis($id){
      $user = Auth::user();
      $test = test::find($id);
      $panelHTML = "";
      $extrHTML = "";

      if(isset($test->experiment_id)){
        $exp = experiment::find($test->experiment_id);
        $panel = panel::find($exp->gene_panel_id);
        $extrn = extraction::find($exp->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      if((isset($test->panel_id)&&isset($test->extraction_id))){
        $panel = panel::find($test->panel_id);
        $extrn = extraction::find($test->extraction_id);
        $panelHTML = view('user.includes.panel_container_min')
                          ->with('panel', $panel)
                          ->with('genes', json_decode($panel->genes))
                          ->render();
        $extrHTML = view('user.includes.extraction_container_min')
                          ->with('extraction', $extrn)
                          ->render();
      }

      if(Storage::disk('local')->exists('tests/avatars/'.$test->avatar)){
        $content = Storage::disk('local')->get('tests/avatars/'.$test->avatar);
        $imdata = base64_encode($content);
        $test->imdata = $imdata;
      }

      if($test->instrument_id>0) $instrument = instrument::find($test->instrument_id);
      else  $instrument = instrument::where('type', 'Simulation')->first();

      $resultHTML = null;

      if(isset($test->result)){
        $results = json_decode($test->result);
        $resultHTML = view('user.includes.result_table')
                          ->with('results', array($results)[0])
                          ->render();
      }

      $comments = Post::where('test_id', $test->id)->get();
      $likes = Like::where('test_id', $test->id)->get();

      return view('user.experiment.view_test')
                  ->with('test_user', $test->user()->first())
                  ->with('test', $test)
                  ->with('likes', $likes)
                  ->with('comments', $comments)
                  ->with('panelHTML', $panelHTML)
                  ->with('extrHTML', $extrHTML)
                  ->with('resultHTML', $resultHTML)
                  ->with('instrument', $instrument);
    }

    /* Display instrument information */
    public function InstrumentView($id){
      $content = view('user.experiment.instrument_instructions')->render();
      return view('user.experiment.instrument_view')->with('content', $content);
    }

}
