<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use Mail;
use App\Hinge;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use \App\Models\Test as test;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the landing page for non regirstered users
     *
     * @return \Illuminate\Http\Response
     */
     public function chrome_extension(){
       return response()->download(storage_path('VioletGeneExtension.crx'));
     }

     public function test_receive(){
       return view('test_receive');
     }

     public function test_emit(){
       return view('test_emit');
     }

     public function fire_event(){
         event(new \App\Events\DataEvent());
         return Response::json(["Success"=>true], 200, array('Content-Type' => 'application/javascript'))->setCallback(Input::get('callback'));
     }

     public function getExperiment(){
       return view('education');
     }

     public function getQpcr(){
       return view('qpcr');
     }

     public function getDatabase(){
       $user = Auth::user();
       $templates = test::where('privacy', 'public')->orWhere('user_id', $user->id)->get();
       return view('tests')->with('templates', $templates);
     }

     public function getLanding(){
       return view('landing');
     }

     public function about(){
       return view('about');
     }

     public function terms(){
       return view('terms');
     }

     public function get_form(Request $request){
       $returnHTML = view('includes.email_form')->render();
       return response()->json(array('success' => true,'html'=>$returnHTML));
     }

     public function send_mail(Request $request){
       Mail::send('emails.contact', ['request' => $request], function ($m) use ($request) {
         $m->from('info@violetgene.org', 'VioletGene');
         $m->to('info@violetgene.org', $request->name)->subject('Email from website');
       });
       return view('about')->with(array('status'=>"Email sent."));
     }

     //------------------------------------------------------------------------------------------------
     //-------------------------------- Hinge production testing --------------------------------------
     //------------------------------------------------------------------------------------------------
     public function HingeAsk(Request $request){

    $file_list = json_decode($request["filelist"], true)["Files"];

    $resp_arr = array();
    foreach($file_list as $file){
        $resp_arr[$file]=$file;
    }
    return response()->json($resp_arr);
    }

    public function HingeUpload(Request $request){
        foreach($request->all() as $log_name => $log)
        {
            $log_data = json_decode($log)->Entries;

            //Store the text file on disc in json_encoded format
            Storage::disk('local')->put('hinges/'.$log_name, json_encode($log_data));

            //Make database entries for the entries in the file

            foreach($log_data as $record){
              $hinges = hinge::where([
                                'MAC' => $record->device,
                                'timestamp' => $record->timestamp,
                              ])->get();
              if(count($hinges)<1)
                        hinge::create([
                                'MAC' => $record->device,
                                'MessageType' => $record->message_type,
                                'message' => json_encode($record->message),
                                'timestamp' => $record->timestamp,
                              ]);
              }

            return response()->json("Stored on the server: ".$log_name);
            // foreach($log_data as $entry){
            //     return [$log_name, "Stored on the server"];
            // }
        }
    }

    public function HingeUpdate(){
      $files = Storage::disk('local')->files('hinges/');
      foreach($files as $file){
        $contents = Storage::disk('local')->get($file);
        $records = json_decode($contents);
        $battery = null;
        $pitch = null;
        $roll = null;
        $initialX = null;
        $finalX = null;
        $initialY = null;
        $finalY = null;
        $initialZ = null;
        $finalZ = null;
        foreach($records as $record){
          $hinges = hinge::where([
                            'MAC' => $record->device,
                            'timestamp' => $record->timestamp,
                          ])->get();
          if(count($hinges)<1)
                    hinge::create([
                            'MAC' => $record->device,
                            'MessageType' => $record->message_type,
                            'message' => json_encode($record->message),
                            'timestamp' => $record->timestamp,
                          ]);
        }
      }
      return redirect('/simon/productionlog/view/0');
    }

    public function HingeView($MAC){
      if(!strcmp($MAC, "0")){
        $hinges = hinge::orderBy('created_at','desc')->get();

        $hinge_display = array();//Device summary array holding average and last set values
        foreach($hinges as $hinge){
          if(strcmp($hinge->MAC, "")!=0){
            if(!isset($hinge_display[$hinge->MAC]))
              $hinge_display[$hinge->MAC] = array();//For each unique MAC create an array of properties

            if(!strcmp($hinge->MessageType, "BTname"))
              $hinge_display[$hinge->MAC]["bt_name"] = json_decode($hinge->message, true)[0];//Last set name

            if(!strcmp($hinge->MessageType, "cal_device")){
              $readings = json_decode($hinge->message, true);//Last set name
              $pitches = $readings[0];
              $rolls = $readings[1];
              $hinge_display[$hinge->MAC]["pitch"] = array_sum($pitches)/count($pitches);//Last set name
              $hinge_display[$hinge->MAC]["roll"] = array_sum($rolls)/count($rolls);;//Last set name
            }

            if(!strcmp($hinge->MessageType, "initial_battery")){
              $battery = json_decode($hinge->message, true);//Last set name
              $hinge_display[$hinge->MAC]["battery"] = array_sum($battery)/count($battery);//Last set name
            }

            if(!strcmp($hinge->MessageType, "initial_IMU")){
              $readings = json_decode($hinge->message, true);//Last set name
              $Xs = $readings[0];
              $Ys = $readings[1];
              $Zs = $readings[2];
              $hinge_display[$hinge->MAC]["X_init"] = array_sum($Xs)/count($Xs);//Last set name
              $hinge_display[$hinge->MAC]["Y_init"] = array_sum($Ys)/count($Ys);//Last set name
              $hinge_display[$hinge->MAC]["Z_init"] = array_sum($Zs)/count($Zs);//Last set name
            }

            if(!strcmp($hinge->MessageType, "final_IMU")){
              $readings = json_decode($hinge->message, true);//Last set name
              $Xs = $readings[0];
              $Ys = $readings[1];
              $Zs = $readings[2];

              $hinge_display[$hinge->MAC]["X_fin"] = array_sum($Xs)/count($Xs);//Last set name
              $hinge_display[$hinge->MAC]["Y_fin"] = array_sum($Ys)/count($Ys);//Last set name
              $hinge_display[$hinge->MAC]["Z_fin"] = array_sum($Zs)/count($Zs);//Last set name
            }
          }
        }

        $devices = array_unique($hinges->pluck('MAC')->all());
        $summary = array(
          'total_entries' => count($hinges),
          'total_devices' => count($devices),
          'last_updated' => $hinges->pluck('created_at')->first()->diffForHumans(),
          );
        return view('hinge/landing', compact('summary'))
                    ->with('hinge_display', $hinge_display);
      }
      else{
        $hinge = hinge::where('MAC', $MAC)->get();

        //Get written bluetooth names
        $BT_names = array_unique($hinge->where('MessageType', "BTname")->pluck("message")->all());

        // Plot battery levels
        $batteries = $hinge->where('MessageType', "initial_battery")->all();
        $battery_charts = array();
        $battery_counter=0;
        foreach ($batteries as $battery) {
          $readings = json_decode($battery->message, true);
          $average = array_sum($readings)/count($readings);
          $lower_lim = 80;
          $upper_lim = 110;
          $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable->addNumberColumn('Reading');
          $outTable->addNumberColumn('Lvl');
          $outTable->addNumberColumn('Avg');
          $outTable->addNumberColumn('Max');
          $outTable->addNumberColumn('Min');
          $counter = 0;
          foreach($readings as $reading){
            $counter += 1;
            $outTable->addRow([$counter, $reading, $average, $upper_lim, $lower_lim]);
          }
          $battery_counter++;
          $name = "Battery".$battery_counter;
          $chart = \Lava::ComboChart($name, $outTable, [
            'title' => $battery->timestamp,
            'vAxis'=> ['title' => 'Battery level (%)'],
            'hAxis' => ['title'=> 'Reading'],
            'seriesType' => 'line',
            'series' => [0 => ['type' => 'scatter']]
          ]);
          $chart_script = \Lava::render('ComboChart', $name, 'chart_div'.$name);
          $battery_charts[$name]=$chart_script;
        }

        // Plot cal IMU levels
        $initial_IMUs = $hinge->where('MessageType', "initial_IMU")->all();
        $initial_IMU_charts = array();
        $initial_IMU_chartsXY = array();
        $initial_IMU_counter=0;
        foreach ($initial_IMUs as $initial_IMU) {
          $readings = json_decode($initial_IMU->message, true);
          $averageZ = array_sum($readings[2])/count($readings[2]);
          $lower_limZ = 16000;
          $upper_limZ = 17000;
          $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable->addNumberColumn('Reading');
          $outTable->addNumberColumn('Lvl');
          $outTable->addNumberColumn('Avg');
          $outTable->addNumberColumn('Max');
          $outTable->addNumberColumn('Min');
          $counter = 0;
          $outTable2 = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable2->addNumberColumn('X axis');
          $outTable2->addNumberColumn('Y axis');
          for($i=0; $i<count($readings[0]); $i++){
            $counter += 1;
            $outTable->addRow([$counter, $readings[2][$i], $averageZ, $upper_limZ, $lower_limZ]);
            $outTable2->addRow([$readings[0][$i], $readings[1][$i]]);
          }
          $initial_IMU_counter++;
          $name = "initial_IMU".$initial_IMU_counter;
          $chart = \Lava::ComboChart($name, $outTable, [
            'title' => $initial_IMU->timestamp,
            'vAxis' => ['title' => 'Z axis raw'],
            'hAxis' => ['title' => 'Reading'],
            'seriesType' => 'line',
            'series' => [0 => ['type' => 'scatter']]
          ]);
          $chart_script = \Lava::render('ComboChart', $name, 'chart_div'.$name);
          $initial_IMU_charts[$name]=$chart_script;

          $name = "initial_IMUXY".$initial_IMU_counter;
          $chart = \Lava::ScatterChart($name, $outTable2, [
            'title' => $initial_IMU->timestamp,
            'vAxis' => ['title' => 'Xaxis'],
            'hAxis' => ['title' => 'Yaxis'],
            'legend' => ['position' => 'none']
          ]);
          $chart_script = \Lava::render('ScatterChart', $name, 'chart_div'.$name);
          $initial_IMU_chartsXY[$name]=$chart_script;
        }

        // Plot final IMU levels
        $final_IMUs = $hinge->where('MessageType', "final_IMU")->all();
        $final_IMU_charts = array();
        $final_IMU_chartsXY = array();
        $final_IMU_counter=0;
        foreach ($final_IMUs as $final_IMU) {
          $readings = json_decode($final_IMU->message, true);
          $averageZ = array_sum($readings[2])/count($readings[2]);
          $lower_limZ = 16000;
          $upper_limZ = 17000;
          $outTable = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable->addNumberColumn('Reading');
          $outTable->addNumberColumn('Lvl');
          $outTable->addNumberColumn('Avg');
          $outTable->addNumberColumn('Max');
          $outTable->addNumberColumn('Min');
          $counter = 0;
          $outTable2 = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable2->addNumberColumn('X axis');
          $outTable2->addNumberColumn('Y axis');
          for($i=0; $i<count($readings[0]); $i++){
            $counter += 1;
            $outTable->addRow([$counter, $readings[2][$i], $averageZ, $upper_limZ, $lower_limZ]);
            $outTable2->addRow([$readings[0][$i], $readings[1][$i]]);
          }
          $final_IMU_counter++;
          $name = "final_IMU".$final_IMU_counter;
          $chart = \Lava::ComboChart($name, $outTable, [
            'title' => $final_IMU->timestamp,
            'vAxis' => ['title' => 'Z axis raw'],
            'hAxis' => ['title' => 'Reading'],
            'seriesType' => 'line',
            'series' => [0 => ['type' => 'scatter']]
          ]);
          $chart_script = \Lava::render('ComboChart', $name, 'chart_div'.$name);
          $final_IMU_charts[$name]=$chart_script;

          $name = "final_IMUXY".$final_IMU_counter;
          $chart = \Lava::ScatterChart($name, $outTable2, [
            'title' => $final_IMU->timestamp,
            'vAxis' => ['title' => 'Xaxis'],
            'hAxis' => ['title' => 'Yaxis'],
            'legend' => ['position' => 'none']
          ]);
          $chart_script = \Lava::render('ScatterChart', $name, 'chart_div'.$name);
          $final_IMU_chartsXY[$name]=$chart_script;
        }

        // Plot calibration readings
        $cal_IMUs = $hinge->where('MessageType', "cal_device")->all();
        $cal_IMU_charts = array();
        $cal_IMU_counter=0;
        foreach ($cal_IMUs as $cal_IMU) {
          $readings = json_decode($cal_IMU->message, true);
          $counter = 0;
          $outTable2 = \Lava::DataTable();  // Lava::DataTable() if using Laravel
          $outTable2->addNumberColumn('Pitch');
          $outTable2->addNumberColumn('Roll');
          for($i=0; $i<count($readings[0]); $i++){
            $counter += 1;
            $outTable2->addRow([$readings[0][$i], $readings[1][$i]]);
          }

          $name = "cal_IMU".$cal_IMU_counter;
          $chart = \Lava::ScatterChart($name, $outTable2, [
            'title' => $cal_IMU->timestamp,
            'vAxis' => ['title' => 'Xaxis'],
            'hAxis' => ['title' => 'Yaxis'],
            'legend' => ['position' => 'none']
          ]);
          $chart_script = \Lava::render('ScatterChart', $name, 'chart_div'.$name);
          $cal_IMU_charts[$name]=$chart_script;
          $cal_IMU_counter++;
        }

        return view('hinge/device', compact('BT_names'), compact('MAC'))
                    ->with('hinge', $hinge)
                    ->with('battery_charts', $battery_charts)
                    ->with('initial_IMU_charts', $initial_IMU_charts)
                    ->with('initial_IMU_chartsXY', $initial_IMU_chartsXY)
                    ->with('final_IMU_charts', $final_IMU_charts)
                    ->with('final_IMU_chartsXY', $final_IMU_chartsXY)
                    ->with('cal_IMU_charts', $cal_IMU_charts);
      }
    }
}
