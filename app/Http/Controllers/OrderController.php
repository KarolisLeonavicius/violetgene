<?php

namespace App\Http\Controllers;
use App;
use Auth;
use App\Models\Price as price;
use App\Models\Quote as quote;
use App\Models\Booking as booking;
use Illuminate\Http\Request;
use Dompdf\Dompdf;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function viewOrder($id){
      $user = Auth::user();
      $quotes = quote::where('user_id', $user->id)->where('id', $id)->first();
      return $quotes->html;
    }

    public function view(Request $request){
      $user = Auth::user();
      $quotes = quote::where('user_id', $user->id)->get()->sortByDesc('created_at');
      return view('order.view', compact('user'), compact('quotes'));
    }

    public function printQuote(Request $request){
      $user = Auth::user();
      $school = $user->school()->first();

      $items = json_decode($request->items);
      $days = json_decode($request->days);
      $items_valid = (object)array();
      $instrument_id = null;
      $panel_id = null;
      $extraction_id = null;
      $experiment_id = null;
      // dd($items);
      $dates = null;
      $panels_created = 0;
      $extractions_created = 0;
      $bookings_created = 0;
      $bookings= 0;
      foreach($items as $key=>$item){
        if($item->name == "booking"){
          $bookings += 1;
          $dates = $item->dates;
          $dates[0] = $dates[0]/1000;
          $dates[1] = $dates[1]/1000;
          $instrument_id = $item->id;

          $clashing1 = booking::where('instrument_id',$instrument_id)->where('from', '<', $dates[1])->where('to', '>', $dates[1])->get();
          $clashing2 = booking::Where('instrument_id',$instrument_id)->where('from', '<', $dates[0])->where('to', '>', $dates[0])->get();

          if((count($clashing1)<1)&&(count($clashing2)<1)){
            booking::create([
              'from' => $dates[1]-86400,
              'to' => $dates[0]+86400,
              'instrument_id' => $instrument_id,
              'user_id' => $user->id
            ]);
            $bookings_created += 1;
            $items_valid->$key = $item;
          }
        }
        if($item->name == "gene_panel"){
          $panels_created += 1;
          $items_valid->$key = $item;
          $panel_id = $item->id;
        }
        if($item->name == "dna_extraction"){
          $extractions_created += 1;
          $items_valid->$key = $item;
          $extraction_id = $item->id;
        }
      }

      if(($bookings>0)&&($bookings_created == 0)) return redirect()->back()->withErrors(["The instrument has already been booked, please choose a different day"]);

      if(isset($dates)){
        $date_to = $dates[0]+86400;
        $date_from = $dates[1]-86400;
      }
      else{
        $date_to = null;
        $date_from = null;
      }
      $quote = quote::create([
              'user_id'=>$user->id,
              'extractions'=>$extractions_created,
              'panels'=>$panels_created,
              'instruments'=>($dates[0]-$dates[1])/86400,
              'cart'=>$request->items,
              'html'=>"",
              'from'=>$date_from,
              'to'=>$date_to
            ]);
      $html = view('order.quote')
              ->with('user', $user)
              ->with('school', $school)
              ->with('items', $items)
              ->with('days', $days)
              ->with('quote', $quote)
              ->render();
      $quote->html = $html;
      $quote->save();
      $prices = price::get();
      return view('order.quote', compact('user'), compact('school'))->with('items', $items_valid)->with('days', $days)->with('quote', $quote)->with('prices', $prices);
    }

    public function printStandard(Request $request, $nr){
      if($nr == 0)
      {
        $instrument_price = price::where('item', 'booking_day')->first()->price_pounds;
        $instrument_transport = price::where('item', 'booking_transport')->first()->price_pounds;
        $panel_price = price::where('item', 'gene_panel')->first()->price_pounds;
        $extraction_price = (100 - $instrument_price - 2*$instrument_transport - 2 * $panel_price) / 12;
        if(isset($request->items))
        {
          $user = Auth::user();
          $school = $user->school()->first();
          $items = array();
          array_push($items, json_decode($request->items)[0]);
          array_push($items, (object)[
            "name"=> "dna_extraction",
            "id"=> 1,
            "quantity"=> 12,
            "price"=> $extraction_price,
            "shipping"=> 0,
            "title"=> "Cooked food",
          ]);
          array_push($items, (object)[
            "name"=> "gene_panel",
            "id"=> 1,
            "quantity"=> 2,
            "price"=> $panel_price,
            "shipping"=> 0,
            "title"=> "Species identification",
          ]);
          $days = json_decode($request->days);
          $dates = json_decode($request->items)[0]->dates;
          $dates[0] = $dates[0]/1000;
          $dates[1] = $dates[1]/1000;
          $instrument_id = json_decode($request->items)[0]->id;

          $clashing1 = booking::where('instrument_id',$instrument_id)->where('from', '<', $dates[1])->where('to', '>', $dates[1])->get();
          $clashing2 = booking::Where('instrument_id',$instrument_id)->where('from', '<', $dates[0])->where('to', '>', $dates[0])->get();

          if((count($clashing1)<1)&&(count($clashing2)<1)){
            booking::create([
              'from' => $dates[1]-86400,
              'to' => $dates[0]+86400,
              'instrument_id' => $instrument_id,
              'user_id' => $user->id
            ]);

            $quote = quote::create([
              'user_id'=>$user->id,
              'extractions'=>12,
              'panels'=>2,
              'instruments'=>($dates[0]-$dates[1])/86400,
              'cart'=>$request->items,
              'html'=>"",
              'from'=>$dates[1]-86400,
              'to'=>$dates[0]+86400
            ]);
            $html = view('order.quote')
                    ->with('user', $user)
                    ->with('school', $school)
                    ->with('items', $items)
                    ->with('days', $days)
                    ->with('quote', $quote)
                    ->render();
            $quote->html = $html;
            $quote->save();

            return view('order.quote', compact('user'), compact('school'))
                          ->with('items', $items)
                          ->with('days', $days)
                          ->with('quote', $quote);
          }
          else {
            return redirect()->back()->withErrors(["The instrument has already been booked, please choose a different day"]);
          }
        }
        else{
          return redirect()->back()->withErrors(["You have to book a qPCR instrument to proceed"]);
        }
      }
      else return redirect()->back()->withErrors(["Unknown error"]);
      // instantiate and use the dompdf class
      // $html = view('order.quote', compact('user'), compact('school'))->with('items', json_decode($request->items))->render();
      // $pdf = App::make('snappy.pdf.wrapper');
      // $pdf->loadHTML($html);
      // return $pdf->inline();
      //
      // $pdf = App::make('dompdf.wrapper');
      // $items = json_decode($request->items);
      // // $pdf->loadHTML(view('order.quote', compact('user'))->with('items', json_decode($request->items)));
      // $pdf->loadView('order.quote', array('user'=>$user, 'school'=>$school, "items"=>$items));
      // $pdf->render();
      // // dd(json_decode($request->items));
      // return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
