<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\LogItem;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
    public function terminate($request, $response)
    {
      $user = $request->user();
      if(is_null($user )) $user_id = 0;
      else $user_id = $user->id;
      LogItem::create(array('user_id'=>$user_id,
      'activity_type'=>$request->method(),
      'activity'=>$request->path(),
      'ip_address'=>$request->ip(),
      'header'=>$request->header('User-Agent')));
        // Store the session data...
    }
}
