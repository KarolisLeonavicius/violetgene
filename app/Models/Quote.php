<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
  protected $fillable = [
      'id', 'user_id', 'extractions', 'panels',
      'instruments', 'cart', 'paid', 'html', 'from', 'to'
  ];
    //
}
