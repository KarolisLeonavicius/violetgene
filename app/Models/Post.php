<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'id', 'parent_post', 'user_id', 'test_id', 'experiment_id', 'extraction_id', 'panel_id', 'instrument_id',
        'topic_id', 'exercise_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
