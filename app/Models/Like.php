<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
  protected $fillable = [
      'id', 'user_id', 'test_id', 'experiment_id', 'extraction_id', 'panel_id', 'instrument_id','topic_id', 'exercise_id'
  ];
}
