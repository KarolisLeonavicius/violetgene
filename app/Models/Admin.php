<?php

namespace App\Models;
use User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }


    public function assignUser($role)
    {
        return $this->roles()->attach($role);
    }

    public function removeUser($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'id', 'school_role', 'run_tests', 'design_tests', 'edit_material', 'curricular', 'extracurricular', 'training', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
