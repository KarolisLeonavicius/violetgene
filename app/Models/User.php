<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function admin()
    {
        return $this->hasOne('App\Models\Admin', 'user_id');
    }

    public function school()
    {
        return $this->belongsToMany('App\Models\School')->withTimestamps();
    }

    public function hasSchool()
    {
      if(sizeof($this->school()->first())>0)
        return true;
      else
        return false;
    }

    public function assignSchool($school)
    {
        return $this->school()->attach($school);
    }

    public function removeSchool($school)
    {
        return $this->school()->detach($school);
    }

    public function social()
    {
        return $this->hasOne('App\Models\Social');
    }

    public function tests()
    {
        return $this->hasMany('App\Models\Test');
    }

    public function FlashAnswers()
    {
        return $this->hasMany('App\Models\Online\FlashCardAnswer');
    }

    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role')->withTimestamps();
    }

    public function hasRole($name)
    {
        foreach($this->roles as $role)
        {
            if($role->name == $name) return true;
        }

        return false;
    }

    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'type', 'provider', 'provider_id', 'token', 'city', 'country', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
