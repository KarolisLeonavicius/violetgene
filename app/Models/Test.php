<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use App\Models\GenePanel as panel;

class Test extends Model
/*Test is an instance of an experiment, which consists of dna_extraction and gene_panel, which consists of genes*/
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'id_form', 'title','comments','image','user_id', 'experiment_id', 'panel_id', 'extraction_id', 'instrument_id','raw_data','result', 'location', 'timestamps', 'privacy', 'avatar'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function genes()
    {
      if(isset($this->panel_id))
      {
        $panel = $this->belongsTo('App\Models\GenePanel', 'panel_id')->first();
        $panelHTML = view('user.includes.panel_genes_min')
                          ->with('panel', $panel)
                          ->render();
        return $panelHTML;
      }
      if(isset($this->experiment_id))
      {
        $experiment = $this->belongsTo('App\Models\Experiment', 'experiment_id')->first();
        $panel = panel::find($experiment->gene_panel_id);
        $panelHTML = view('user.includes.panel_genes_min')
                          ->with('panel', $panel)
                          ->render();
        return $panelHTML;
      }
      return "";
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
