<?php

namespace App\Models\Online;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class FlashCardQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     public function answers()
     {
         /* Has One relationship...
           return $this->hasOne('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'local_key');
           foreign_key identifies Answer in the Questions Object by the primary key, answer_id
           local_key identifies Answer in this model by its primary key answer_id
           There are many answers for one Question
         */

         /* Has One relationship...
           return $this->belongsTo('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'other_key');
           foreign_key identifies the question in this model (Answer)
           other_key identifies the question in its original model, which is question_id
           There is one Question associated with answer
         */
         return $this->hasMany('App\Models\Online\FlashCardAnswer', 'flash_id', 'id');
     }

    protected $fillable = [
        'id', "level", "rating", "answer_count", "body", "title", "answer1", "answer2", "correct", "created_by"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
