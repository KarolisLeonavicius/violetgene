<?php

namespace App\Models\Online;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class FlashCardAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     public function question()
     {
         /* Has One relationship...
           return $this->hasOne('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'local_key');
           foreign_key identifies Answer in the Questions Object by the primary key, answer_id
           local_key identifies Answer in this model by its primary key answer_id
           There are many answers for one Question
         */

         /* Has One relationship...
           return $this->belongsTo('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'other_key');
           foreign_key identifies the question in this model (Answer)
           other_key identifies the question in its original model, which is question_id
           There is one Question associated with answer
         */
         return $this->belongsTo('App\Models\Online\FlashCardQuestion', 'flash_id', 'id');
     }

     public function user()
     {
         /* Has One relationship...
           return $this->hasOne('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'local_key');
           foreign_key identifies Answer in the Questions Object by the primary key, answer_id
           local_key identifies Answer in this model by its primary key answer_id
           There are many answers for one Question
         */

         /* Has One relationship...
           return $this->belongsTo('App\Models\Online\BioStackExchangeQuestion', 'foreign_key', 'other_key');
           foreign_key identifies the question in this model (Answer)
           other_key identifies the question in its original model, which is question_id
           There is one Question associated with answer
         */
         return $this->belongsTo('App\Models\User');
     }

    protected $fillable = [
        'id', "flash_id", "user_id", "outcome", "rating_user", "rating_flash", "rating_change",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
