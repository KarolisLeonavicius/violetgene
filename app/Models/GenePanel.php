<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class GenePanel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'type', 'name', 'content', 'genes', 'pos_controls','neg_controls', 'reactions', 'user_id', 'privacy', 'approved'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function genes()
    {
        $gene_list = json_decode($this->genes);
        return $gene_list;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
