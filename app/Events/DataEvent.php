<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DataEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $data;

    public function __construct($time, $data, $cycles, $temperature, $rawHTML, $resultHTML)
    {
        $this->data = [
            'local_time'=> $time,
            'data'=> $data,
            'cycles'=> $cycles."/42",
            'temp'=> $temperature,
            'chartHTML' => $rawHTML,
            'resultHTML' => $resultHTML,
            'time' => \Carbon\Carbon::now('Europe/London')->toDateTimeString()
            ];
    }

    public function broadcastOn()
    {
        return ['test-channel'];
    }
}
