<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hinge extends Model
{
  protected $fillable = [
      'MAC', 'MessageType', 'timestamp', 'message','battery','pitch', 'roll','initialZ','finalZ','initialX','finalX','initialY','finalY'
  ];
}
