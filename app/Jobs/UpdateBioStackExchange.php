<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Online\BioStackExchangeQuestion as BioStack;

class UpdateBioStackExchange implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $error = 0;
      $page = 1;
      $client = new \GuzzleHttp\Client();
        do{
          $res = $client->request('GET', "http://api.stackexchange.com/2.2/search?site=biology&pagesize=100&page=$page&tagged={genetics}&filter=withbody");
          $body = $res->getBody();
          $page = $page + 1;

          if($res->getStatusCode() == 200){
            $search_items = json_decode($body, false)->items;
            var_dump(sizeof($search_items));
            for ($i=0; $i < sizeof($search_items); $i++) {
              $question_object = $search_items[$i];

              $question_array = (array) $question_object;
              $question_array['tags'] = json_encode($question_object->tags);
              $question_array['owner'] = json_encode($question_object->owner);
              $question = BioStack::where('question_id', $question_array['question_id']);
              #Check if the question returned by search does not exist
              if(!$question->exists()){
                BioStack::create($question_array);#Create a new cached question in the database
              }
              elseif($question->first()->last_activity_date < $question_array['last_activity_date']){#Check if the question has been updated
                $question = new BioStack($question_array);
                $quesion->save();
              }
            }
          }
          else return 0; #If get an error fetching results, return with an the error code
        }while(json_decode($body, false)->has_more);

      return 1; #If job is complete
    }
}
