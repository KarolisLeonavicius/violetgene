<?php

use Illuminate\Database\Seeder;
use App\Models\GeneTarget;

class TargetSeeder extends Seeder{

    public function run(){
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Horse id',
            'gene_id'=>'KT221845.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'CTTAGTGGCAGACTTACTGACACTAACA',
            'backward_primer'=>'AGTTGGCCGATAATTACGTATGG',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Pork id',
            'gene_id'=>'KY110726.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'CAAAGCAACCCTCACACGATT',
            'backward_primer'=>'TGCGAGGGCGGTAATGAT',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Beef id',
            'gene_id'=>'AF446392.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'GGCAGCCCTTGCAAACC',
            'backward_primer'=>'CAGTTCACGGGACTCTGACATTC',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Sheep id',
            'gene_id'=>'MF004246.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'AATATTCCGACCAATCAGTCAATGT',
            'backward_primer'=>'CAACTGGCTGGCCTCCAAT',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Chicken id',
            'gene_id'=>'KX987152.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'TCTCACTTACACTACTTGCCACATCTT',
            'backward_primer'=>'CGTGTGTGTCCTGTTTGGACTAG',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Species identification',
            'name'=>'Turkey id',
            'gene_id'=>'LC088153.1',
            'reference'=>'http://dx.doi.org/10.1080/02652030701584041',
            'forward_primer'=>'CCGTAACCTCCATGCGAATG',
            'backward_primer'=>'TAATATAGGCCGCGTCCAATGT',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Positive Control',
            'name'=>'Pos control',
            'gene_id'=>'',
            'reference'=>'',
            'forward_primer'=>'',
            'backward_primer'=>'',
            'derived_from'=>'[2, 3, 5]',
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
        GeneTarget::create([
            'type'=>'Negative Control',
            'name'=>'Neg control',
            'gene_id'=>'',
            'reference'=>'',
            'forward_primer'=>'',
            'backward_primer'=>'',
            'derived_from'=>null,
            'user_id'=>1,
            'privacy'=>'Public',
            'approved'=>1
        ]);
    }
}
