<?php

use Illuminate\Database\Seeder;
use App\Models\Price;

class PriceSeeder extends Seeder{

    public function run(){
        Price::create([
            'item'=>'booking_day',
            'price_pounds'=>25
        ]);
        Price::create([
            'item'=>'booking_transport',
            'price_pounds'=>15
        ]);
        Price::create([
            'item'=>'dna_extraction',
            'price_pounds'=>3
        ]);
        Price::create([
            'item'=>'gene_panel',
            'price_pounds'=>5
        ]);
    }
}
