<?php

use Illuminate\Database\Seeder;
use App\Models\DnaExtraction;

class ExtractionSeeder extends Seeder{

    public function run(){
        DnaExtraction::create([
            'type'=>'Cooked food',
            'steps'=>3,
            'duration'=>'under 30 mins',
            'protocol'=>'<h3>Simple DNA extraction</h3><ol><li>Take a piece of the sandwich by using the jagged edge of a broken toothpick and place it in the tube containing the DNA extraction solution.<br></li><li>Place the the solution with the sample in thermocycler set at <b>50C for 15min</b>.</li><li>Place the the solution with the sample in thermocycler set at <b>95C for 5min</b>.</li><li>Place a small <b>cotton swab</b> into the tube and gently <b>press it down</b> to keep the precipitate at the bottom of the tube.</li><li>Using a fixed volume glass capillary, add 5uL of extracted DNA to <b>each of the tubes</b> in the 8 well strip.</li></ol>',
            'additional'=>'Timer, hot water or thermocycler',
            'user_id'=>1,
            'derived_from'=>0,
            'privacy'=>'Public',
            'approved'=>1
        ]);
    }
}
