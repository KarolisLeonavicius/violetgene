<?php

use Illuminate\Database\Seeder;
use App\Models\Experiment;

class ExperimentSeeder extends Seeder{

    public function run(){
        Experiment::create([
          'extraction_id'=>1,
          'name'=>"Simple sandwich test",
          'gene_panel_id'=>1,
          'content'=>"",
          'privacy'=>'public',
          'approved'=>1,
          'user_id' => 1
        ]);
    }
}
