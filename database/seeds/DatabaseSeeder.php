<?php

use Illuminate\Database\Seeder;
use \database\seeds\FlashCardQuestionSeeder;
// use \database\seeds\RoleSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(RoleSeeder::class);
      $this->call(FlashCardQuestion::class);
      $this->call(InstrumentSeeder::class);
      $this->call(PriceSeeder::class);
      $this->call(ExtractionSeeder::class);
      $this->call(PanelSeeder::class);
      $this->call(TargetSeeder::class);
      $this->call(ExperimentSeeder::class);
      // $this->call(UserSeeder::class);
    }
}
