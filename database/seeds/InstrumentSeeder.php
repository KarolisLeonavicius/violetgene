<?php

use Illuminate\Database\Seeder;
use App\Models\Instrument;

class InstrumentSeeder extends Seeder{

    public function run(){
        Instrument::create([
          'code'=>'0710712984',
          'ip'=>"192.168.7.2",
          'username'=>"gene@violetgene.org",
          'password'=>"none",
          'configuration'=>null,
          'type'=>'ChaiBio',
          'extraction_id'=>6,
          'run_id'=>7,
          'location'=>null,
          'run_count'=>0,
        ]);
    }
}
