<?php

use Illuminate\Database\Seeder;

define('CSV', base_path('database/seeds/CSV/flash_card_questions.csv'));

class FlashCardQuestion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run() {

         $table_name = "flash_card_questions";
         // delete the existing table
         DB::table($table_name)->delete();

         $wholeCSVData = array_map('str_getcsv', file(CSV));

         $i = "first_value";
         foreach ($wholeCSVData as $single) {
             if ($i == "first_value") {
                 foreach ($single as $key => $heading) {
                     $column_headings[] = $heading;
                 }
                 $i = "not_first_value" ;
                 continue ;
             } // the first row in the csv file contains the headings


             $theVal = [];
             foreach ($single as $key => $row_value) {

                 // uncomment below when you have password in the csv and want to create a hash for it
                 // --useful for users table

                 // when inserting the password create the hash of the password
                 // and store the real password in another row
               /*  if($column_headings[$key] == "password") {
                     $theVal['password'] = Hash::make($row_value);
                     $theVal['password_real'] = $row_value ;
                     continue ;
                 }*/

                 $theVal[$column_headings[$key]] = $row_value;

             } // after this loop we'll have one row prepared

             DB::table($table_name)->insert($theVal);

         }
     }
}
