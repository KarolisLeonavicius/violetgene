<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instruments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('ip');
            $table->string('username');
            $table->string('password');
            $table->text('configuration')->nullable();
            $table->string('type')->default("ChaiBio");
            $table->text('location')->nullable();
            $table->integer('run_count')->default(0);
            $table->integer('extraction_id')->default(0);
            $table->integer('run_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instruments');
    }
}
