<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBioStackExchangeQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_stack_exchange_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->json('tags')->nullable();
            $table->json('owner')->nullable();
            $table->boolean("is_answered")->nullable();
            $table->integer("view_count")->nullable();
            $table->integer("answer_count")->nullable();
            $table->integer("score")->nullable();
            $table->integer("last_activity_date")->nullable();
            $table->integer("creation_date")->nullable();
            $table->integer("last_edit_date")->nullable();
            $table->integer("question_id");
            $table->text("link");
            $table->text("title");
            $table->text("body");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_stack_exchange_questions');
    }
}
