<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBioStackExchangeAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_stack_exchange_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->json("owner");
            $table->boolean("is_accepted");
            $table->integer("score");
            $table->integer("last_activity_date");
            $table->integer("creation_date");
            $table->integer("answer_id");
            $table->integer("question_id");
            $table->text("body");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_stack_exchange_answers');
    }
}
