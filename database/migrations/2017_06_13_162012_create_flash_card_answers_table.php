<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashCardAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_card_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flash_id');
            $table->integer('user_id');
            $table->boolean('outcome');
            $table->float('rating_user');
            $table->float('rating_flash');
            $table->float('rating_change');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_card_answers');
    }
}
