<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('avatar');
            $table->text('id_form');
            $table->string('privacy');
            $table->longtext('comments')->nullable();
            $table->integer('instrument_id')->nullable();
            $table->integer('experiment_id')->nullable();
            $table->integer('extraction_id')->nullable();
            $table->integer('panel_id')->nullable();
            $table->integer('user_id');
            $table->json('raw_data')->nullable();
            $table->text('location')->nullable();
            $table->text('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
