<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperimentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiments', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('extraction_id');
          $table->integer('user_id');
          $table->integer('gene_panel_id');
          $table->longtext('content')->nullable();
          $table->string('privacy');
          $table->boolean('approved');
          $table->timestamps();
          //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('experiments');
    }
}
