<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('first_name');
          $table->string('last_name');
          $table->string('email')->unique()->nullable();
          $table->string('password', 60)->nullable();
          $table->rememberToken();
          $table->boolean('activated')->default(false);
          $table->string('token');
          $table->string('avatar')->default('default.png');
          $table->text('description')->nullable();
          $table->string('city')->default('NA');
          $table->string('country')->default('NA');
          $table->json('timeline')->nullable();
          $table->float('flash_score')->default(0);
          $table->float('exam_score')->default(0);
          $table->integer('stack_answer_views')->default(0);
          $table->integer('stack_question_views')->default(0);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
