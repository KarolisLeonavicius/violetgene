<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDnaExtractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dna_extractions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('derived_from')->default(0);
            $table->integer('user_id');
            $table->text('type');
            $table->integer('steps');
            $table->text('duration');
            $table->text('protocol');
            $table->text('privacy');
            $table->boolean('approved');
            $table->text('additional')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dna_extractions');
    }
}
