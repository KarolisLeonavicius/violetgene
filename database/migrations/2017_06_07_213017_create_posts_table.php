<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_post')->nullable();
            $table->integer('test_id')->nullable();
            $table->integer('user_id');
            $table->integer('experiment_id')->nullable();
            $table->integer('extraction_id')->nullable();
            $table->integer('panel_id')->nullable();
            $table->integer('instrument_id')->nullable();
            $table->integer('topic_id')->nullable();
            $table->integer('exercise_id')->nullable();
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
