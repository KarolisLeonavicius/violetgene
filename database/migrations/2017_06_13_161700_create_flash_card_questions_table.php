<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashCardQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_card_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->default(0);
            $table->float('rating')->default(0.0);
            $table->integer('answer_count')->default(0);
            $table->integer('created_by');
            $table->text('body');
            $table->text('title');
            $table->text('answer1');
            $table->text('answer2');
            $table->integer('correct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_card_questions');
    }
}
