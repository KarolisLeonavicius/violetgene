<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(array('prefix' => ''), function(){
    Route::get('/', 'HomeController@getLanding');
    Route::get('experiment', 'HomeController@getExperiment');
    Route::get('qpcr', 'HomeController@getQpcr');
    Route::get('database', 'HomeController@getDatabase');
    Route::get('terms', 'HomeController@terms');
    Route::get('about', 'HomeController@about');
    Route::get('get_mail', 'HomeController@get_form');
    Route::post('send_mail', 'HomeController@send_mail');
    Route::get('test_receive', 'HomeController@test_receive');
    Route::get('test_emit', 'HomeController@test_emit');
    Route::get('fire', 'HomeController@fire_event');
    Route::get('chrome_extension', 'HomeController@chrome_extension');
    //Hinge test functions
});

Route::group(array('prefix' => 'simon'), function(){
  //Hinge test functions
  Route::post('productionlog/ask', 'HomeController@HingeAsk');
  Route::post('productionlog/upload', 'HomeController@HingeUpload');
  Route::get( 'productionlog/view/{MAC}', 'HomeController@HingeView');
  Route::get( 'productionlog/update', 'HomeController@HingeUpdate');

});

Route::group(array('prefix' => 'social'), function(){
  Route::get('redirect/{provider}',   ['as' => 'social.redirect',   'uses' => 'Auth\SocialController@getSocialRedirect']);
  Route::get('handle/{provider}',     ['as' => 'social.handle',     'uses' => 'Auth\SocialController@getSocialHandle']);
});

Route::group(array('prefix' => 'user', 'middleware' => 'auth'), function(){
    Route::get('home', 'UserController@Welcome');
    Route::get('settings/{type}', 'UserController@Account');
    Route::get('password', 'UserController@Password');
    Route::get('education/{id}', 'UserController@Education');
    Route::get('explorer', 'UserController@Explorer');

    Route::get('get_avatar', 'UserController@Get_avatar');
    Route::get('assign_school/{school}', 'UserController@Assign_school');
    Route::post('update_avatar', 'UserController@Update_avatar');
    Route::post('update_profile', 'UserController@Update_profile');
    Route::post('administrator_register', 'UserController@Update_admin');
    Route::post('use_code', 'UserController@Use_code');
    Route::post('unregister', 'UserController@Unregister');
    Route::post('postcode_lookup', 'UserController@postcode_lookup');
});

Route::group(array('prefix' => 'topic', 'middleware' => 'auth'), function(){
    Route::get('getBioStack', 'TopicController@getBioStackTopics');
    Route::get('answer/BioStack/{question_id}', 'TopicController@BioStackAnswer');
    Route::get('refreshBioStack', 'TopicController@refreshBioStackTopics');
    Route::get('fetchTopic', 'TopicController@fetchTopic');

    Route::post('submitFlash', 'TopicController@submitFlash');
    Route::post('putFlash', 'TopicController@Import_flashcards');
    Route::get('putFlash', 'TopicController@Show_import');

    Route::get('EditFlashWindow', 'TopicController@editFlashWindow');

    Route::get('fetchFlash', 'TopicController@fetchFlash');
    Route::post('getFlash/{card_id}', 'TopicController@getFlashCard');
    Route::get('getFlashList', 'TopicController@getFlashList');
    Route::post('createFlash', 'TopicController@createFlash');
    Route::post('editFlash/{flash_id}', 'TopicController@editFlash');
    Route::post('deleteFlash/{flash_id}', 'TopicController@deleteFlash');

    Route::get('NewExamWindow', 'ExerciseController@newExerciseWindow');
    Route::get('editExam', 'ExerciseController@editExerciseWindow');
    Route::post('editExam/{id}', 'ExerciseController@editExercise');
    Route::post('createExam', 'ExerciseController@newExercise');
    Route::get('getExercise/{id}', 'ExerciseController@getExercise');

    Route::post('createTopic', 'ExerciseController@newTopic');
    Route::get('NewTopicWindow', 'ExerciseController@newTopicWindow');
    Route::post('editTopic/{id}', 'ExerciseController@editTopic');
    Route::get('EditTopicWindow', 'ExerciseController@editTopicWindow');
    Route::get('getTopicTree/{id}', 'ExerciseController@getTopicTree');
    Route::get('getTopic/{id}', 'ExerciseController@getTopic');

    Route::get('test', 'TopicController@test');

});

Route::group(array('prefix' => 'test', 'middleware' => 'auth'), function(){

    Route::get('design_extraction/{extraction}', 'TestController@designExtractionWindow');
    Route::post('createExtraction', 'TestController@createExtraction');
    Route::get('getExtractionList', 'TestController@getExtractionList');
    Route::get('getExtractionSelect', 'TestController@getExtractionSelect');
    Route::get('getExtractionMin/{id}', 'TestController@getExtractionMin');
    Route::get('getExtractionListBuy', 'TestController@getExtractionListBuy');
    Route::get('ExtractionProtocol/{id}', 'TestController@getExtractionProtocol');
    Route::post('deleteExtraction/{id}', 'TestController@deleteExtraction');

    Route::get('design_panel/{id}', 'TestController@designPanelWindow');
    Route::get('getPanelRow/{id}', 'TestController@getPanelRow');
    Route::get('getPanelMin/{id}', 'TestController@getPanelMin');
    Route::get('getPanelList', 'TestController@getPanelList');
    Route::get('getPanelListBuy', 'TestController@getPanelListBuy');
    Route::get('getPanelListSelect', 'TestController@getPanelListSelect');
    Route::post('createPanel', 'TestController@createPanel');
    Route::post('editPanel', 'TestController@editPanel');
    Route::post('deletePanel/{id}', 'TestController@deletePanel');
    Route::get('PanelDescription/{id}', 'TestController@ViewPanel');

    Route::get('design_target/{extraction}', 'TestController@designTargetWindow');
    Route::post('createTarget', 'TestController@createTarget');
    Route::post('editTarget', 'TestController@editTarget');
    Route::get('getTargetList', 'TestController@getTargetList');
    Route::get('getTargetListEdit', 'TestController@getTargetListEdit');
    Route::post('deleteTarget/{id}', 'TestController@deleteTarget');
    Route::post('editTarget/{id}', 'TestController@editTarget');

    Route::get('new/{type}', 'TestController@New');
    Route::get('my_tests/{id}', 'TestController@MyTests');
    Route::post('createRun', 'TestController@runNewTest');
    Route::post('saveTest', 'TestController@saveTest');
    Route::get('getTestList', 'TestController@getTestList');
    Route::get('getTestList2', 'TestController@getTestList2');
    Route::get('getTest/{id}', 'TestController@getTest');
    Route::post('getTest/{id}', 'TestController@getTest');
    Route::get('runTest/{id}', 'TestController@runTest');
    Route::get('fullscreen/{id}', 'TestController@viewAnalysis');
    Route::post('deleteTest/{id}', 'TestController@deleteTest');

    Route::get('getExperimentList', 'TestController@getExperimentList');
    Route::get('getExperiment/{id}', 'TestController@getExperiment');
    Route::get('getExperimentMin/{id}', 'TestController@getExperimentMin');
    Route::get('viewExperiment/{id}', 'TestController@viewExperiment');

    Route::get('view_instrument/{id}', 'TestController@InstrumentView');
});

Route::group(array('prefix' => 'instrument', 'middleware' => 'auth'), function(){
  Route::get('View', 'InstrumentController@View');
  Route::get('getInstrumentList', 'InstrumentController@getInstrumentList');
  Route::get('getAvailability/{id}', 'InstrumentController@getAvailability');
  Route::get('getBookings', 'InstrumentController@getBookings');
  Route::post('attach', 'InstrumentController@connectInstrument');
  Route::post('simulator', 'InstrumentController@Simulator');
  Route::get('simulator', 'InstrumentController@Simulator');
  Route::post('data_up', 'InstrumentController@Upload');
});

Route::group(array('prefix' => 'order', 'middleware' => 'auth'), function(){
  Route::post('quote', 'OrderController@printQuote');
  Route::post('standard/{nr}', 'OrderController@printStandard');
  Route::get('start', 'TestController@start');
  Route::get('view', 'OrderController@view');
  Route::get('view/{id}', 'OrderController@viewOrder');
});

Route::group(array('prefix' => 'comment', 'middleware' => 'auth'), function(){
  Route::post('post/topic/{id}', 'CommentController@postTopic');
  Route::post('like/topic/{id}', 'CommentController@likeTopic');
  Route::post('post/exercise/{id}', 'CommentController@postExercise');
  Route::post('like/exercise/{id}', 'CommentController@likeExercise');
  Route::post('post/test/{id}', 'CommentController@postTest');
  Route::post('like/test/{id}', 'CommentController@likeTest');
});
